package com.example.james.pilot_project;

import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityDeletionOrUpdateAdapter;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.database.Cursor;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unchecked")
public class DaoAccess_Impl implements DaoAccess {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfOperation;

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfOperation;

  private final EntityDeletionOrUpdateAdapter __updateAdapterOfOperation;

  public DaoAccess_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfOperation = new EntityInsertionAdapter<Operation>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `Operation`(`id`,`Description`,`Amount`,`inserted_at`,`modified_at`,`id_user`,`id_operation_category`,`operation_visible`,`id_account_user`,`id_currency`,`operation_synchronized`) VALUES (nullif(?, 0),?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Operation value) {
        stmt.bindLong(1, value.getId());
        if (value.getDescription() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getDescription());
        }
        stmt.bindDouble(3, value.getAmount());
        if (value.getInsertedAt() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getInsertedAt());
        }
        if (value.getModifiedAt() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getModifiedAt());
        }
        if (value.getIdUser() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getIdUser());
        }
        stmt.bindLong(7, value.getIdOperationCategory());
        final int _tmp;
        _tmp = value.isOperationVisible() ? 1 : 0;
        stmt.bindLong(8, _tmp);
        if (value.getIdAccountUser() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.getIdAccountUser());
        }
        stmt.bindLong(10, value.getIdCurrency());
        final int _tmp_1;
        _tmp_1 = value.isOperationSynchronized() ? 1 : 0;
        stmt.bindLong(11, _tmp_1);
      }
    };
    this.__deletionAdapterOfOperation = new EntityDeletionOrUpdateAdapter<Operation>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `Operation` WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Operation value) {
        stmt.bindLong(1, value.getId());
      }
    };
    this.__updateAdapterOfOperation = new EntityDeletionOrUpdateAdapter<Operation>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `Operation` SET `id` = ?,`Description` = ?,`Amount` = ?,`inserted_at` = ?,`modified_at` = ?,`id_user` = ?,`id_operation_category` = ?,`operation_visible` = ?,`id_account_user` = ?,`id_currency` = ?,`operation_synchronized` = ? WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Operation value) {
        stmt.bindLong(1, value.getId());
        if (value.getDescription() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getDescription());
        }
        stmt.bindDouble(3, value.getAmount());
        if (value.getInsertedAt() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getInsertedAt());
        }
        if (value.getModifiedAt() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getModifiedAt());
        }
        if (value.getIdUser() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getIdUser());
        }
        stmt.bindLong(7, value.getIdOperationCategory());
        final int _tmp;
        _tmp = value.isOperationVisible() ? 1 : 0;
        stmt.bindLong(8, _tmp);
        if (value.getIdAccountUser() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.getIdAccountUser());
        }
        stmt.bindLong(10, value.getIdCurrency());
        final int _tmp_1;
        _tmp_1 = value.isOperationSynchronized() ? 1 : 0;
        stmt.bindLong(11, _tmp_1);
        stmt.bindLong(12, value.getId());
      }
    };
  }

  @Override
  public Long insertOperation(Operation operation) {
    __db.beginTransaction();
    try {
      long _result = __insertionAdapterOfOperation.insertAndReturnId(operation);
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteOperation(Operation operation) {
    __db.beginTransaction();
    try {
      __deletionAdapterOfOperation.handle(operation);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void updateOperation(Operation operation) {
    __db.beginTransaction();
    try {
      __updateAdapterOfOperation.handle(operation);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<Operation> fetchAllOperations() {
    final String _sql = "SELECT * FROM Operation ORDER BY inserted_at desc";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
      final int _cursorIndexOfDescription = _cursor.getColumnIndexOrThrow("Description");
      final int _cursorIndexOfAmount = _cursor.getColumnIndexOrThrow("Amount");
      final int _cursorIndexOfInsertedAt = _cursor.getColumnIndexOrThrow("inserted_at");
      final int _cursorIndexOfModifiedAt = _cursor.getColumnIndexOrThrow("modified_at");
      final int _cursorIndexOfIdUser = _cursor.getColumnIndexOrThrow("id_user");
      final int _cursorIndexOfIdOperationCategory = _cursor.getColumnIndexOrThrow("id_operation_category");
      final int _cursorIndexOfOperationVisible = _cursor.getColumnIndexOrThrow("operation_visible");
      final int _cursorIndexOfIdAccountUser = _cursor.getColumnIndexOrThrow("id_account_user");
      final int _cursorIndexOfIdCurrency = _cursor.getColumnIndexOrThrow("id_currency");
      final int _cursorIndexOfOperationSynchronized = _cursor.getColumnIndexOrThrow("operation_synchronized");
      final List<Operation> _result = new ArrayList<Operation>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final Operation _item;
        _item = new Operation();
        final int _tmpId;
        _tmpId = _cursor.getInt(_cursorIndexOfId);
        _item.setId(_tmpId);
        final String _tmpDescription;
        _tmpDescription = _cursor.getString(_cursorIndexOfDescription);
        _item.setDescription(_tmpDescription);
        final float _tmpAmount;
        _tmpAmount = _cursor.getFloat(_cursorIndexOfAmount);
        _item.setAmount(_tmpAmount);
        final String _tmpInsertedAt;
        _tmpInsertedAt = _cursor.getString(_cursorIndexOfInsertedAt);
        _item.setInsertedAt(_tmpInsertedAt);
        final String _tmpModifiedAt;
        _tmpModifiedAt = _cursor.getString(_cursorIndexOfModifiedAt);
        _item.setModifiedAt(_tmpModifiedAt);
        final String _tmpIdUser;
        _tmpIdUser = _cursor.getString(_cursorIndexOfIdUser);
        _item.setIdUser(_tmpIdUser);
        final int _tmpIdOperationCategory;
        _tmpIdOperationCategory = _cursor.getInt(_cursorIndexOfIdOperationCategory);
        _item.setIdOperationCategory(_tmpIdOperationCategory);
        final boolean _tmpOperationVisible;
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfOperationVisible);
        _tmpOperationVisible = _tmp != 0;
        _item.setOperationVisible(_tmpOperationVisible);
        final String _tmpIdAccountUser;
        _tmpIdAccountUser = _cursor.getString(_cursorIndexOfIdAccountUser);
        _item.setIdAccountUser(_tmpIdAccountUser);
        final int _tmpIdCurrency;
        _tmpIdCurrency = _cursor.getInt(_cursorIndexOfIdCurrency);
        _item.setIdCurrency(_tmpIdCurrency);
        final boolean _tmpOperationSynchronized;
        final int _tmp_1;
        _tmp_1 = _cursor.getInt(_cursorIndexOfOperationSynchronized);
        _tmpOperationSynchronized = _tmp_1 != 0;
        _item.setOperationSynchronized(_tmpOperationSynchronized);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<Operation> getOperationsLookingLike(String OpDescription) {
    final String _sql = "SELECT * from Operation WHERE Description like ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (OpDescription == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, OpDescription);
    }
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
      final int _cursorIndexOfDescription = _cursor.getColumnIndexOrThrow("Description");
      final int _cursorIndexOfAmount = _cursor.getColumnIndexOrThrow("Amount");
      final int _cursorIndexOfInsertedAt = _cursor.getColumnIndexOrThrow("inserted_at");
      final int _cursorIndexOfModifiedAt = _cursor.getColumnIndexOrThrow("modified_at");
      final int _cursorIndexOfIdUser = _cursor.getColumnIndexOrThrow("id_user");
      final int _cursorIndexOfIdOperationCategory = _cursor.getColumnIndexOrThrow("id_operation_category");
      final int _cursorIndexOfOperationVisible = _cursor.getColumnIndexOrThrow("operation_visible");
      final int _cursorIndexOfIdAccountUser = _cursor.getColumnIndexOrThrow("id_account_user");
      final int _cursorIndexOfIdCurrency = _cursor.getColumnIndexOrThrow("id_currency");
      final int _cursorIndexOfOperationSynchronized = _cursor.getColumnIndexOrThrow("operation_synchronized");
      final List<Operation> _result = new ArrayList<Operation>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final Operation _item;
        _item = new Operation();
        final int _tmpId;
        _tmpId = _cursor.getInt(_cursorIndexOfId);
        _item.setId(_tmpId);
        final String _tmpDescription;
        _tmpDescription = _cursor.getString(_cursorIndexOfDescription);
        _item.setDescription(_tmpDescription);
        final float _tmpAmount;
        _tmpAmount = _cursor.getFloat(_cursorIndexOfAmount);
        _item.setAmount(_tmpAmount);
        final String _tmpInsertedAt;
        _tmpInsertedAt = _cursor.getString(_cursorIndexOfInsertedAt);
        _item.setInsertedAt(_tmpInsertedAt);
        final String _tmpModifiedAt;
        _tmpModifiedAt = _cursor.getString(_cursorIndexOfModifiedAt);
        _item.setModifiedAt(_tmpModifiedAt);
        final String _tmpIdUser;
        _tmpIdUser = _cursor.getString(_cursorIndexOfIdUser);
        _item.setIdUser(_tmpIdUser);
        final int _tmpIdOperationCategory;
        _tmpIdOperationCategory = _cursor.getInt(_cursorIndexOfIdOperationCategory);
        _item.setIdOperationCategory(_tmpIdOperationCategory);
        final boolean _tmpOperationVisible;
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfOperationVisible);
        _tmpOperationVisible = _tmp != 0;
        _item.setOperationVisible(_tmpOperationVisible);
        final String _tmpIdAccountUser;
        _tmpIdAccountUser = _cursor.getString(_cursorIndexOfIdAccountUser);
        _item.setIdAccountUser(_tmpIdAccountUser);
        final int _tmpIdCurrency;
        _tmpIdCurrency = _cursor.getInt(_cursorIndexOfIdCurrency);
        _item.setIdCurrency(_tmpIdCurrency);
        final boolean _tmpOperationSynchronized;
        final int _tmp_1;
        _tmp_1 = _cursor.getInt(_cursorIndexOfOperationSynchronized);
        _tmpOperationSynchronized = _tmp_1 != 0;
        _item.setOperationSynchronized(_tmpOperationSynchronized);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<Operation> getOperationsInTimeInterval(String opDateStart, String opDateEnd) {
    final String _sql = "SELECT * from Operation WHERE inserted_at BETWEEN ? AND ? ORDER BY inserted_at desc";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 2);
    int _argIndex = 1;
    if (opDateStart == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, opDateStart);
    }
    _argIndex = 2;
    if (opDateEnd == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, opDateEnd);
    }
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
      final int _cursorIndexOfDescription = _cursor.getColumnIndexOrThrow("Description");
      final int _cursorIndexOfAmount = _cursor.getColumnIndexOrThrow("Amount");
      final int _cursorIndexOfInsertedAt = _cursor.getColumnIndexOrThrow("inserted_at");
      final int _cursorIndexOfModifiedAt = _cursor.getColumnIndexOrThrow("modified_at");
      final int _cursorIndexOfIdUser = _cursor.getColumnIndexOrThrow("id_user");
      final int _cursorIndexOfIdOperationCategory = _cursor.getColumnIndexOrThrow("id_operation_category");
      final int _cursorIndexOfOperationVisible = _cursor.getColumnIndexOrThrow("operation_visible");
      final int _cursorIndexOfIdAccountUser = _cursor.getColumnIndexOrThrow("id_account_user");
      final int _cursorIndexOfIdCurrency = _cursor.getColumnIndexOrThrow("id_currency");
      final int _cursorIndexOfOperationSynchronized = _cursor.getColumnIndexOrThrow("operation_synchronized");
      final List<Operation> _result = new ArrayList<Operation>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final Operation _item;
        _item = new Operation();
        final int _tmpId;
        _tmpId = _cursor.getInt(_cursorIndexOfId);
        _item.setId(_tmpId);
        final String _tmpDescription;
        _tmpDescription = _cursor.getString(_cursorIndexOfDescription);
        _item.setDescription(_tmpDescription);
        final float _tmpAmount;
        _tmpAmount = _cursor.getFloat(_cursorIndexOfAmount);
        _item.setAmount(_tmpAmount);
        final String _tmpInsertedAt;
        _tmpInsertedAt = _cursor.getString(_cursorIndexOfInsertedAt);
        _item.setInsertedAt(_tmpInsertedAt);
        final String _tmpModifiedAt;
        _tmpModifiedAt = _cursor.getString(_cursorIndexOfModifiedAt);
        _item.setModifiedAt(_tmpModifiedAt);
        final String _tmpIdUser;
        _tmpIdUser = _cursor.getString(_cursorIndexOfIdUser);
        _item.setIdUser(_tmpIdUser);
        final int _tmpIdOperationCategory;
        _tmpIdOperationCategory = _cursor.getInt(_cursorIndexOfIdOperationCategory);
        _item.setIdOperationCategory(_tmpIdOperationCategory);
        final boolean _tmpOperationVisible;
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfOperationVisible);
        _tmpOperationVisible = _tmp != 0;
        _item.setOperationVisible(_tmpOperationVisible);
        final String _tmpIdAccountUser;
        _tmpIdAccountUser = _cursor.getString(_cursorIndexOfIdAccountUser);
        _item.setIdAccountUser(_tmpIdAccountUser);
        final int _tmpIdCurrency;
        _tmpIdCurrency = _cursor.getInt(_cursorIndexOfIdCurrency);
        _item.setIdCurrency(_tmpIdCurrency);
        final boolean _tmpOperationSynchronized;
        final int _tmp_1;
        _tmp_1 = _cursor.getInt(_cursorIndexOfOperationSynchronized);
        _tmpOperationSynchronized = _tmp_1 != 0;
        _item.setOperationSynchronized(_tmpOperationSynchronized);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public float getBalance() {
    final String _sql = "SELECT SUM(Amount) FROM Operation";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final float _result;
      if(_cursor.moveToFirst()) {
        _result = _cursor.getFloat(0);
      } else {
        _result = 0;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<Operation> getLastThreeOperations() {
    final String _sql = "SELECT * FROM Operation ORDER BY inserted_at desc LIMIT 3";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
      final int _cursorIndexOfDescription = _cursor.getColumnIndexOrThrow("Description");
      final int _cursorIndexOfAmount = _cursor.getColumnIndexOrThrow("Amount");
      final int _cursorIndexOfInsertedAt = _cursor.getColumnIndexOrThrow("inserted_at");
      final int _cursorIndexOfModifiedAt = _cursor.getColumnIndexOrThrow("modified_at");
      final int _cursorIndexOfIdUser = _cursor.getColumnIndexOrThrow("id_user");
      final int _cursorIndexOfIdOperationCategory = _cursor.getColumnIndexOrThrow("id_operation_category");
      final int _cursorIndexOfOperationVisible = _cursor.getColumnIndexOrThrow("operation_visible");
      final int _cursorIndexOfIdAccountUser = _cursor.getColumnIndexOrThrow("id_account_user");
      final int _cursorIndexOfIdCurrency = _cursor.getColumnIndexOrThrow("id_currency");
      final int _cursorIndexOfOperationSynchronized = _cursor.getColumnIndexOrThrow("operation_synchronized");
      final List<Operation> _result = new ArrayList<Operation>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final Operation _item;
        _item = new Operation();
        final int _tmpId;
        _tmpId = _cursor.getInt(_cursorIndexOfId);
        _item.setId(_tmpId);
        final String _tmpDescription;
        _tmpDescription = _cursor.getString(_cursorIndexOfDescription);
        _item.setDescription(_tmpDescription);
        final float _tmpAmount;
        _tmpAmount = _cursor.getFloat(_cursorIndexOfAmount);
        _item.setAmount(_tmpAmount);
        final String _tmpInsertedAt;
        _tmpInsertedAt = _cursor.getString(_cursorIndexOfInsertedAt);
        _item.setInsertedAt(_tmpInsertedAt);
        final String _tmpModifiedAt;
        _tmpModifiedAt = _cursor.getString(_cursorIndexOfModifiedAt);
        _item.setModifiedAt(_tmpModifiedAt);
        final String _tmpIdUser;
        _tmpIdUser = _cursor.getString(_cursorIndexOfIdUser);
        _item.setIdUser(_tmpIdUser);
        final int _tmpIdOperationCategory;
        _tmpIdOperationCategory = _cursor.getInt(_cursorIndexOfIdOperationCategory);
        _item.setIdOperationCategory(_tmpIdOperationCategory);
        final boolean _tmpOperationVisible;
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfOperationVisible);
        _tmpOperationVisible = _tmp != 0;
        _item.setOperationVisible(_tmpOperationVisible);
        final String _tmpIdAccountUser;
        _tmpIdAccountUser = _cursor.getString(_cursorIndexOfIdAccountUser);
        _item.setIdAccountUser(_tmpIdAccountUser);
        final int _tmpIdCurrency;
        _tmpIdCurrency = _cursor.getInt(_cursorIndexOfIdCurrency);
        _item.setIdCurrency(_tmpIdCurrency);
        final boolean _tmpOperationSynchronized;
        final int _tmp_1;
        _tmp_1 = _cursor.getInt(_cursorIndexOfOperationSynchronized);
        _tmpOperationSynchronized = _tmp_1 != 0;
        _item.setOperationSynchronized(_tmpOperationSynchronized);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
