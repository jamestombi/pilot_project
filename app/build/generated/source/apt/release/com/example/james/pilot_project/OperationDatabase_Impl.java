package com.example.james.pilot_project;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Callback;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Configuration;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.RoomOpenHelper;
import android.arch.persistence.room.RoomOpenHelper.Delegate;
import android.arch.persistence.room.util.TableInfo;
import android.arch.persistence.room.util.TableInfo.Column;
import android.arch.persistence.room.util.TableInfo.ForeignKey;
import android.arch.persistence.room.util.TableInfo.Index;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;

@SuppressWarnings("unchecked")
public class OperationDatabase_Impl extends OperationDatabase {
  private volatile DaoAccess _daoAccess;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `Operation` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `Description` TEXT, `Amount` REAL NOT NULL, `inserted_at` TEXT, `modified_at` TEXT, `id_user` TEXT, `id_operation_category` INTEGER NOT NULL, `operation_visible` INTEGER NOT NULL, `id_account_user` TEXT, `id_currency` INTEGER NOT NULL, `operation_synchronized` INTEGER NOT NULL)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"1a4fccacb78b8e63d628ccf9eff951f9\")");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `Operation`");
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsOperation = new HashMap<String, TableInfo.Column>(11);
        _columnsOperation.put("id", new TableInfo.Column("id", "INTEGER", true, 1));
        _columnsOperation.put("Description", new TableInfo.Column("Description", "TEXT", false, 0));
        _columnsOperation.put("Amount", new TableInfo.Column("Amount", "REAL", true, 0));
        _columnsOperation.put("inserted_at", new TableInfo.Column("inserted_at", "TEXT", false, 0));
        _columnsOperation.put("modified_at", new TableInfo.Column("modified_at", "TEXT", false, 0));
        _columnsOperation.put("id_user", new TableInfo.Column("id_user", "TEXT", false, 0));
        _columnsOperation.put("id_operation_category", new TableInfo.Column("id_operation_category", "INTEGER", true, 0));
        _columnsOperation.put("operation_visible", new TableInfo.Column("operation_visible", "INTEGER", true, 0));
        _columnsOperation.put("id_account_user", new TableInfo.Column("id_account_user", "TEXT", false, 0));
        _columnsOperation.put("id_currency", new TableInfo.Column("id_currency", "INTEGER", true, 0));
        _columnsOperation.put("operation_synchronized", new TableInfo.Column("operation_synchronized", "INTEGER", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysOperation = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesOperation = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoOperation = new TableInfo("Operation", _columnsOperation, _foreignKeysOperation, _indicesOperation);
        final TableInfo _existingOperation = TableInfo.read(_db, "Operation");
        if (! _infoOperation.equals(_existingOperation)) {
          throw new IllegalStateException("Migration didn't properly handle Operation(com.example.james.pilot_project.Operation).\n"
                  + " Expected:\n" + _infoOperation + "\n"
                  + " Found:\n" + _existingOperation);
        }
      }
    }, "1a4fccacb78b8e63d628ccf9eff951f9", "ab1d5f04facd4655bbef1c0d2f6371e8");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    return new InvalidationTracker(this, "Operation");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `Operation`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public DaoAccess daoAccess() {
    if (_daoAccess != null) {
      return _daoAccess;
    } else {
      synchronized(this) {
        if(_daoAccess == null) {
          _daoAccess = new DaoAccess_Impl(this);
        }
        return _daoAccess;
      }
    }
  }
}
