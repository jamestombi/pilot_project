package com.katikatech.james.pilot_project;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ApiConnectionHelper extends ContextWrapper {

    public static final int VERSION_GMS_V8_MAX = 10200000;
    private static final String TAG = "ApiConnectionHelper";
    //public static final String RESPONSE_SUCCESS = "success";
    public static final String RESPONSE_PHONE = "phone";
    public static final String RESPONSE_USERID = "userId";
    public static final String RESPONSE_CODE = "code";
    public static final String RESPONSE_SUCCESS = "success";

    private RequestQueue requestQueue;


    public ApiConnectionHelper(Context base) {
        super(base);
        requestQueue = Volley.newRequestQueue(this);
    }

    /**
     *
     * @param phoneNo the phone number detected in the smartphone
     * @param successReceiver the interface implementation when successful server response
     * @param failureReceiver the interface implementation once server failure
     */
    public void request(String phoneNo, final RequestResponse successReceiver,
                        final ApiError failureReceiver) {
        HashMap<String, String> params = new HashMap<>();
        params.put("phone", phoneNo);
        sendRequest(R.string.url_request, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Boolean success = false;
                String phone = "";
                int code = 0;
                try {
                    phone = response.getString(RESPONSE_PHONE);
                    code = response.getInt(RESPONSE_CODE);
                } catch (JSONException e) {
                    Log.e(TAG, "Possible missing response value.", e);
                }
                successReceiver.onResponse(code);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                failureReceiver.onError(error);
            }
        });
    }


    public void checkLastConnection(String phoneNo, String smsMessage, final VerifyResponse successReceiver,
                       final ApiError failureReceiver) {
        HashMap<String, String> params = new HashMap<>();
        params.put("phone", phoneNo);
        params.put("sms_message", smsMessage);

        sendRequest(R.string.url_verify, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                boolean success = false;
                String phoneNo = null;
                try {
                    success = response.getBoolean(RESPONSE_CODE);
                    phoneNo = response.getString(RESPONSE_PHONE);
                } catch (JSONException e) {
                    Log.e(TAG, "Possible missing response value.", e);
                }
                successReceiver.onResponse(success, phoneNo);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                failureReceiver.onError(error);
            }
        });
    }




    public void verify(String phoneNo, String smsMessage, final VerifyResponse successReceiver,
                       final ApiError failureReceiver) {
        HashMap<String, String> params = new HashMap<>();
        params.put("phone", phoneNo);
        params.put("sms_message", smsMessage);

        sendRequest(R.string.url_verify, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                boolean success = false;
                String phoneNo = null;
                try {
                    success = response.getBoolean(RESPONSE_CODE);
                    phoneNo = response.getString(RESPONSE_PHONE);
                } catch (JSONException e) {
                    Log.e(TAG, "Possible missing response value.", e);
                }
                successReceiver.onResponse(success, phoneNo);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                failureReceiver.onError(error);
            }
        });
    }



    public void request_userid(String userIdInitiate, final RequestUserIdResponse successReceiver,
                        final ApiError failureReceiver) {
        HashMap<String, String> params = new HashMap<>();
        params.put("initUserId", userIdInitiate);
        sendRequest(R.string.url_request_userid, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                boolean success = false;
                String userId = "";
                int code = 0;
                try {
                    userId = response.getString(RESPONSE_USERID);
                    success = response.getBoolean(RESPONSE_SUCCESS);
                } catch (JSONException e) {
                    Log.e(TAG, "Possible missing response value.", e);
                }
                successReceiver.onResponse(userId, success);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                failureReceiver.onError(error);
            }
        });
    }

    public void request_update_user(String userId, String userFirstName, String userName, String userPhone, final UpdateResponse successReceiver,
                               final ApiError failureReceiver) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", userId);
        params.put("userFirstName", userFirstName);
        params.put("userName", userName);
        params.put("userPhone", userPhone);
        sendRequest(R.string.url_request_update_user, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                boolean success = false;
                //String userId = "";
                try {
                    //userId = response.getString(RESPONSE_USERID);
                    success = response.getBoolean(RESPONSE_SUCCESS);
                } catch (JSONException e) {
                    Log.e(TAG, "Possible missing response value.", e);
                }
                successReceiver.onResponse(success);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                failureReceiver.onError(error);
            }
        });
    }


    protected void sendRequest(int urlId, HashMap<String, String> params, Response.Listener success,
                               Response.ErrorListener failure) {
        sendRequest(getString(urlId), params, success, failure);
        //sendBloquingRequest(getString(urlId), params, success, failure);
    }

    /**
     *
     * @param url  The address of the server call
     * @param params the hashmap containing all parameters (for instance phone number)
     * @param success The method called once the results are back
     * @param failure The method called once the server call failed
     */
    protected void sendRequest(String url, HashMap<String, String> params, Response.Listener success,
                               Response.ErrorListener failure) {
        String secret = "big_secret";
        if (TextUtils.isEmpty(secret)) {
            final int gmsVersion = getGmsVersion(getApplicationContext());
            Log.d(TAG, "GMS Version: " + gmsVersion);
            secret = getString(R.string.server_client_secret);
            if (gmsVersion  < VERSION_GMS_V8_MAX) {
                secret = getString(R.string.server_client_secret_v8);
            }
        }
        try {
            JSONObject args = new JSONObject();
            args.put("client_secret", secret);
            for (String key: params.keySet()) {
                args.put(key, params.get(key));
            }

            //Classic definition of a JsonObjectRequest
            // We define the method of request (POST, GET)
            //the address of the server call
            //the JSONObject arguments
            // the Response.Listener of successful operation
            // The Response.ErrorListener of failed operation
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url,
                    args, success, failure);

            //Add the request in the queue of operations
            requestQueue.add(request);
        } catch (JSONException e) {
            Log.e(TAG, "Unable to make the request", e);
        }
    }


    protected void sendBloquingRequest(String url, HashMap<String, String> params, Response.Listener success,
                               Response.ErrorListener failure) {
        String secret = "big_secret";
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        //success = RequestFuture.newFuture();

        if (TextUtils.isEmpty(secret)) {
            final int gmsVersion = getGmsVersion(getApplicationContext());
            Log.d(TAG, "GMS Version: " + gmsVersion);
            secret = getString(R.string.server_client_secret);
            if (gmsVersion  < VERSION_GMS_V8_MAX) {
                secret = getString(R.string.server_client_secret_v8);
            }
        }
        try {
            JSONObject args = new JSONObject();
            JSONObject response = new JSONObject();
            args.put("client_secret", secret);
            for (String key: params.keySet()) {
                args.put(key, params.get(key));
            }

            //Classic definition of a JsonObjectRequest
            // We define the method of request (POST, GET)
            //the address of the server call
            //the JSONObject arguments
            // the Response.Listener of successful operation
            // The Response.ErrorListener of failed operation
            //JsonObjectRequest newRequest = new JsonObjectRequest(Request.Method.POST, url,
            //        args, future, future);


            //JsonObjectRequest newRequest = new JsonObjectRequest(url, args, future, failure);
            JsonObjectRequest newRequest = new JsonObjectRequest(Request.Method.POST,
                    url, args, future, failure);

            //Add the request in the queue of operations
            requestQueue.add(newRequest);

            try {
                response = future.get(15, TimeUnit.SECONDS);
                Log.d(TAG, "CODE ==> "+response.getInt(RESPONSE_CODE));
            } catch (InterruptedException e) {
                Log.e(TAG, "INTERRUPTED "+e.getMessage());
                failure.onErrorResponse(new VolleyError(e));
            } catch (ExecutionException e) {
                Log.e(TAG, "EXECUTION EXCEP "+e.getMessage());
                failure.onErrorResponse(new VolleyError(e));
            } catch (TimeoutException e) {
                Log.e(TAG, "TIMEOUT EXCP "+e.getMessage());
                failure.onErrorResponse(new VolleyError(e));
            }

        } catch (JSONException e) {
            Log.e(TAG, "Unable to make the request", e);
        }
    }

    private static int getGmsVersion(Context context) {
        try {
            return context.getPackageManager()
                    .getPackageInfo("com.google.android.gms", 0 ).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return VERSION_GMS_V8_MAX;
    }

    public interface RequestResponse {
        public void onResponse(int code);
    }

    public interface RequestUserIdResponse {
        public void onResponse(String userId, boolean success);
    }

    public interface VerifyResponse {
        public void onResponse(boolean success, String phoneNumber);
    }

    public interface ResetResponse {
        public void onResponse(boolean success);
    }

    public interface UpdateResponse {
        public void onResponse(boolean success);
    }

    public interface ApiError {
        public void onError(VolleyError error);
    }
}
