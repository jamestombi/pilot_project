package com.katikatech.james.pilot_project;

public class Currency {

    private int currencyId;
    private String currencySymbol;
    private String currencyDescription;

    public Currency(int mCurrencyId, String mCurrencySymbol, String mCurrentDescription) {
        this.currencyId = mCurrencyId;
        this.currencySymbol = mCurrencySymbol;
        this.currencyDescription = mCurrentDescription;

    }

    public int getCurrencyId() {
        return currencyId;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public String getCurrencyDescription() {
        return currencyDescription;
    }
}
