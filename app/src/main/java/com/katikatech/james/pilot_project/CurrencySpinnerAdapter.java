package com.katikatech.james.pilot_project;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class CurrencySpinnerAdapter extends ArrayAdapter<Currency> {

    private List<Currency> data;

    public CurrencySpinnerAdapter(Context context, List<Currency> data) {
        super(context, 0, data);
        this.data = data;
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        Currency currency = data.get(position);
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_currency, parent, false);
            convertView.setTag(ViewHolder.createViewHolder(convertView));
        }

        ViewHolder holder = (ViewHolder)convertView.getTag();
        holder.currencySymbol.setText(currency.getCurrencySymbol());
        holder.currencyDescription.setText(currency.getCurrencyDescription());
        return convertView;
    }

    @Override
    public int getCount( ) {
        return data.size();
    }

    private static class ViewHolder {
        public TextView currencySymbol;
        public TextView currencyDescription;

        public static ViewHolder createViewHolder(View view) {
            ViewHolder holder = new ViewHolder();
            holder.currencySymbol = (TextView) view.findViewById(R.id.currency_symbol);
            holder.currencyDescription = (TextView)view.findViewById(R.id.currency_description);
            return holder;
        }
    }
}
