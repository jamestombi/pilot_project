package com.katikatech.james.pilot_project;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface DaoAccess {

    @Insert
    Long insertOperation(Operation operation);

    @Query("SELECT * FROM Operation WHERE operation_visible = 1 ORDER BY inserted_at desc")
    List<Operation> fetchAllOperations();

    @Query("SELECT * from Operation WHERE Description like :OpDescription")
    List<Operation> getOperationsLookingLike(String OpDescription);


    @Query("SELECT * from Operation WHERE operation_visible = 1 AND inserted_at BETWEEN :opDateStart AND :opDateEnd ORDER BY inserted_at desc")
    List<Operation> getOperationsInTimeInterval(String opDateStart, String opDateEnd);

    @Update
    void updateOperation(Operation operation);

    @Delete
    void deleteOperation(Operation operation);

    @Query("DELETE from Operation")
    void deleteAll();

    @Query("SELECT SUM(Amount) FROM Operation WHERE operation_visible = 1")
    float getBalance();

    @Query("SELECT * FROM Operation WHERE operation_visible = 1 ORDER BY inserted_at desc LIMIT 3")
    List<Operation> getLastThreeOperations();

    //@Query("SELECT * FROM Operation WHERE operation_visible = 1 AND Description = :opDescription AND inserted_at = :opDateInsertion")
    //Operation getRequestedOperation(String opDescription, String opDateInsertion);

    @Query("SELECT * FROM Operation WHERE operation_visible = 1 AND remote_key = :opKey")
    Operation getRequestedOperation(String opKey);
}
