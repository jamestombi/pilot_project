package com.katikatech.james.pilot_project;

import android.app.Application;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.recyclerview.DetailedAccountRecyclerView;
import com.katikatech.james.pilot_project.recyclerview.DetailedTransactionRecyclerView;

import java.text.DecimalFormat;
import java.util.List;

public class FragmentTransaction extends Fragment {

    private static final String ARG_FILTER_NUMBER = "filter_number";
    private static DetailedTransactionRecyclerView myRecyclerView;
    private static DetailedAccountRecyclerView myAccountRecyclerView;
    private static RecyclerView recyclerView;
    private static RecyclerView accountRecyclerView;
    static TextView total_amount;
    private static Context context;
    private static Application application = null;
    private List<Operation> operationList;
    private static int argIncomeOrExpense;
    private static int argFilterValue;
    private String currencySymbol = "";

    public FragmentTransaction() {

    }

    public static FragmentTransaction newInstance(int filterItem) {
        FragmentTransaction fragment = new FragmentTransaction();
        Bundle args = new Bundle();
        args.putInt(ARG_FILTER_NUMBER, filterItem);
        fragment.setArguments(args);
        return fragment;
    }

    public static FragmentTransaction newInstance(int filterItem, int incomeOrExpense) {
        FragmentTransaction fragment = new FragmentTransaction();
        Bundle args = new Bundle();
        argFilterValue = filterItem;
        argIncomeOrExpense = incomeOrExpense;
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView;
        //Inflate fragment_transaction layout

        rootView = inflater.inflate(R.layout.fragment_transaction, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview_detailed_tranctions);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            //registerForContextMenu(recyclerView);
        //Useful if you want to add others option in the main menu
        setHasOptionsMenu(true);


        try{
            String query = getArguments().getString("searchItem");
            if(!query.isEmpty()) {
                Log.d("QUERY ", query);
            }
        } catch (NullPointerException excp){
                Log.d("EMPTY QUERY", "nothing to show");
        }

        //These are the lines to select the total amount
        context = getActivity();
        application = ((FragmentActivity) context).getApplication();
        total_amount = (TextView) getActivity().findViewById(R.id.label_total_amount);

        currencySymbol = new KatikaSharedPreferences(rootView.getContext()).getUserCurrencySymbol();

        //Call List of operation from database and prepare action like click action

        /*
         *
         *
         */
        //Getter of list of operations
        //operationList = new OperationRepository(application).getOperationsInLastMonth();
        if(argFilterValue == R.id.id_this_week_filter) {
            operationList = new OperationRepository(application).getOperationsInWeekPrior(0);
        } else if(argFilterValue == R.id.id_this_month_filter) {
            operationList = new OperationRepository(application).getOperationsInMonthPrior(0);
        } else if (argFilterValue == R.id.id_last_month_filter){
            operationList = new OperationRepository(application).getOperationsInMonthPrior(1);
        } else if (argFilterValue == R.id.id_90_days_filter) {
            operationList = new OperationRepository(application).getOperationsInLastNineteenDays();
        } else if (argFilterValue == R.id.id_current_year_filter) {
            operationList = new OperationRepository(application).getOperationsInYearPrior(0);
        }else {
            operationList = new OperationRepository(application).mylistOperations();
        }

        myRecyclerView = new DetailedTransactionRecyclerView(getContext(), application, operationList);
        //myRecyclerView = new DetailedTransactionRecyclerView(getContext(), application, operationList);
        //adapt all the outputs of the list to the design of the new adapter
        recyclerView.setAdapter(myRecyclerView);


        /*switch (argIncomeOrExpense) {
            case 0:
                myRecyclerView = new DetailedTransactionRecyclerView(operationList);
                //adapt all the outputs of the list to the design of the new adapter
                recyclerView.setAdapter(myRecyclerView);
                break;
            case 1: //Obsolete
                myAccountRecyclerView = new DetailedAccountRecyclerView(operationList);
                //adapt all the outputs of the list to the design of the new adapter
                accountRecyclerView.setAdapter(myAccountRecyclerView);
                break;
            default:
                myRecyclerView = new DetailedTransactionRecyclerView(operationList);
                //adapt all the outputs of the list to the design of the new adapter
                recyclerView.setAdapter(myRecyclerView);
                break;
        }*/



        float sumCredit = 0;
        DecimalFormat df2 = new DecimalFormat("#.##");
        // A list of TextView
        for (int item = 0; item < operationList.size(); item++) {
            sumCredit += operationList.get(item).getAmount();
        }

        if (sumCredit >= 0) {
            total_amount.setTextColor(Color.rgb(50, 200, 50)); //GREEN
        } else {
            total_amount.setTextColor(Color.rgb(200,50,50)); //RED
        }
        String my_text = context.getResources().getString(R.string.total_amount) + " " + df2.format(sumCredit)+" "+currencySymbol;
        total_amount.setText(my_text);

        return rootView;
    }

    /*
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_detailed_transactions, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
    */
}
