package com.katikatech.james.pilot_project;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.katikatech.james.pilot_project.services.PhoneNumberVerifierService;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;

import static com.katikatech.james.pilot_project.services.PhoneNumberVerifierService.MAX_TIMEOUT;

public class KatikaSMSReceiver extends BroadcastReceiver {

    private static final String TAG = "KatikaSMSReceiver";

    Handler h = new Handler();
    Runnable r = new Runnable() {
        @Override
        public void run() {
            doTimeout();
        }
    };


    public void setTimeout() {
        h.postDelayed(r, MAX_TIMEOUT);
    }

    public void cancelTimeout() {
        h.removeCallbacks(r);
    }


    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        if (intent == null) {
            return;
        }

        String action = intent.getAction();
        Log.d(TAG, "ACTION = "+ action);
        if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(action)) {
            cancelTimeout();
            //notifyStatus(STATUS_RESPONSE_RECEIVED, null);
            Bundle extras = intent.getExtras();
            Status status = (Status) extras.get(SmsRetriever.EXTRA_STATUS);
            switch(status.getStatusCode()) {
                case CommonStatusCodes.SUCCESS:
                    String smsMessage = (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE);
                    Log.d(TAG, "Retrieved sms code: " + smsMessage);
                    if (smsMessage != null) {
                        PhoneNumberVerifierService.startActionSMSVerification(context, smsMessage);
                    }
                    break;
                case CommonStatusCodes.TIMEOUT:
                    doTimeout();
                    break;
                default:
                    break;
            }
        }
    }

    private void doTimeout() {
        Log.d(TAG, "Waiting for sms timed out.");
        //Toast.makeText(this, getString(R.string.toast_unverified), Toast.LENGTH_LONG).show();
        //Toast.makeText(this, "unverifed number", Toast.LENGTH_LONG).show();
        //stopSelf();
    }
}
