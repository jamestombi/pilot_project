package com.katikatech.james.pilot_project;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.katikatech.james.pilot_project.recyclerview.DetailedAccountRecyclerView;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OperationAccountFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OperationAccountFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OperationAccountFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private static DetailedAccountRecyclerView myAccountRecyclerView;
    private static RecyclerView accountRecyclerView;
    static TextView total_amount;
    private static Context context;
    private static Application application = null;
    private List<Operation> operationList;
    private static int argFilterValue;
    private String currencySymbol = "";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    //private OnFragmentInteractionListener mListener;

    public OperationAccountFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param myFilterValue filter Value.
     * @return A new instance of fragment OperationAccountFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OperationAccountFragment newInstance(int myFilterValue) {
        OperationAccountFragment fragment = new OperationAccountFragment();
        Bundle args = new Bundle();
        argFilterValue = myFilterValue;
        fragment.setArguments(args);
        return fragment;
    }

    /*
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_operation_account, container, false);
        accountRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview_account_transactions);
        accountRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        if(argFilterValue == R.id.id_this_week_filter) {
            operationList = new OperationRepository(application).getOperationsInWeekPrior(0);
        } else if(argFilterValue == R.id.id_this_month_filter) {
            operationList = new OperationRepository(application).getOperationsInMonthPrior(0);
        } else if (argFilterValue == R.id.id_last_month_filter){
            operationList = new OperationRepository(application).getOperationsInMonthPrior(1);
        } else if (argFilterValue == R.id.id_90_days_filter) {
            operationList = new OperationRepository(application).getOperationsInLastNineteenDays();
        } else if (argFilterValue == R.id.id_current_year_filter) {
            operationList = new OperationRepository(application).getOperationsInYearPrior(0);
        }else {
            operationList = new OperationRepository(application).mylistOperations();
        }

        //Attach all datas in recyclerView
        myAccountRecyclerView = new DetailedAccountRecyclerView(operationList);
        //adapt all the outputs of the list to the design of the new adapter
        accountRecyclerView.setAdapter(myAccountRecyclerView);


        return rootView;
    }

    /*
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    */

    /*
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
*/
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    /*
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    */
}
