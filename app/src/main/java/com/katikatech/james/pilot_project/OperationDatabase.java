package com.katikatech.james.pilot_project;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import android.content.Context;

@Database(entities = {Operation.class}, version = 1, exportSchema = false)
public abstract class OperationDatabase extends RoomDatabase {
    private static String DB_NAME = "Operation_database";
    public abstract DaoAccess daoAccess();

    private static volatile OperationDatabase INSTANCE;

    static OperationDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (OperationDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    OperationDatabase.class, DB_NAME).build();
                }
            }
        }
        return INSTANCE;
    }
}
