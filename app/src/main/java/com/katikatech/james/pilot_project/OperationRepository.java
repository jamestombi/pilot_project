package com.katikatech.james.pilot_project;

import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;

import java.text.ParseException;
import java.time.YearMonth;
import java.util.Calendar;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class OperationRepository {

    private DaoAccess daoAccess;

    float myBalance;
    List<Operation> totalOperations;
    Operation myOperation;


    public OperationRepository(Application application){
        OperationDatabase operationDatabase = OperationDatabase.getDatabase(application);
        daoAccess = operationDatabase.daoAccess();
        //operationDatabase = Room.databaseBuilder(context,
        //        OperationDatabase.class, DB_NAME).build();
    }

    public void insertOperation(String description, float amount){
        Operation operation = new Operation();
        operation.setDescription(description);
        operation.setAmount(amount);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        operation.setInsertedAt(dateFormat.format(new Date()));

        insertOperation(operation);
    }

    public void insertOperation(Context context, String description, float amount, String insertedAt, String remoteKey){
        KatikaSharedPreferences preferences = new KatikaSharedPreferences(context);
        Operation operation = new Operation();
        operation.setDescription(description);
        operation.setAmount(amount);
        operation.setInsertedAt(insertedAt);
        operation.setModifiedAt(insertedAt);
        operation.setIdAccountUser("001");
        operation.setIdCurrency(preferences.getUserCurrencyId());
        if(amount >= 0) {
            operation.setIdOperationCategory(1);
        } else {
            operation.setIdOperationCategory(2);
        }
        operation.setIdUser(preferences.getUserId());
        operation.setOperationVisible(true);
        operation.setOperationSynchronized(true);
        operation.setNumberItems(1);
        operation.setRemoteKey(remoteKey);

        insertOperation(operation);
    }

    public void retrieveOperation(Context context, String description, float amount,
                                  String insertedAt, String modifiedAt, int currencyId,
                                  int categoryId, float numberItems, String remoteKey){
        KatikaSharedPreferences preferences = new KatikaSharedPreferences(context);
        Operation operation = new Operation();
        operation.setDescription(description);
        operation.setAmount(amount);
        operation.setInsertedAt(insertedAt);
        operation.setModifiedAt(modifiedAt);
        operation.setIdAccountUser("001");
        operation.setIdCurrency(currencyId);
        operation.setIdOperationCategory(categoryId);
        operation.setIdUser(preferences.getUserId());
        operation.setOperationVisible(true);
        operation.setOperationSynchronized(true);
        operation.setNumberItems(numberItems);
        operation.setRemoteKey(remoteKey);

        insertOperation(operation);
    }

    public void updateOperation(Context context, Operation operation,
                                String description, float amount,
                                int category, float numberItems, boolean remoteSynchro){
        KatikaSharedPreferences preferences = new KatikaSharedPreferences(context);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.FRANCE);
        DateFormat humanDateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.FRANCE);
        operation.setNumberItems(numberItems);
        operation.setModifiedAt(dateFormat.format(new Date()));
        operation.setDescription(description);
        operation.setAmount(amount);
        operation.setIdOperationCategory(category);
        operation.setOperationSynchronized(remoteSynchro);

        //call the background function of updgrade
        updateOperation(operation);

        //Update the last time the data were updated
        try {
            preferences.setLastUpdate(humanDateFormat.format(dateFormat.parse(operation.getModifiedAt())));
        } catch (ParseException e) {
            preferences.setLastUpdate(operation.getModifiedAt());
        }
    }

    public void hideOperation(Context context, Operation operation)
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.FRANCE);
        DateFormat humanDateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.FRANCE);
        KatikaSharedPreferences preferences = new KatikaSharedPreferences(context);
        operation.setOperationVisible(false);
        operation.setModifiedAt(dateFormat.format(new Date()));

        //call the background function of updgrade
        updateOperation(operation);

        //Update the last time the data were updated
        try {
            preferences.setLastUpdate(humanDateFormat.format(dateFormat.parse(operation.getModifiedAt())));
        } catch (ParseException e) {
            preferences.setLastUpdate(operation.getModifiedAt());
        }
    }

    public List<Operation> listOperations() {
        //return operationDatabase.daoAccess().fetchAllOperations();
        return daoAccess.fetchAllOperations();
    }

    public void insertOperation(final Operation operation) {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {

                //operationDatabase.daoAccess().insertOperation(operation);
                daoAccess.insertOperation(operation);
                return null;
            }
        }.execute();
    }


    public void updateOperation(final Operation operation) {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {

                //operationDatabase.daoAccess().insertOperation(operation);
                daoAccess.updateOperation(operation);
                return null;
            }
        }.execute();
    }


    public void clearAllExistingOperations() {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {

                //operationDatabase.daoAccess().insertOperation(operation);
                daoAccess.deleteAll();;
                return null;
            }
        }.execute();
    }


    public void deleteOperation(final Operation operation) {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {

                //operationDatabase.daoAccess().deleteOperation(operation);
                daoAccess.deleteOperation(operation);
                return null;
            }
        }.execute();
    }

    public List<Operation> retrieveOperationsLookingLike(String operationDescription){

        AsyncTask<String, Void, List<Operation>> myTask = new AsyncTask<String, Void, List<Operation>>(){

            @Override
            protected List<Operation> doInBackground(String... strings) {
                String queryHint = strings[0];
                Log.d("JTOXX", ""+queryHint + "  " + strings.length);
                return daoAccess.getOperationsLookingLike(queryHint);
                //return daoAccess.fetchAllOperations();
            }

            @Override
            protected void onPostExecute(List<Operation> allOperations) {
                super.onPostExecute(allOperations);
                Log.d("JTO", ""+allOperations.size());
            }
        }.execute(operationDescription);


        try {
            totalOperations = myTask.get();
        } catch (InterruptedException e) {
            Log.d("JTO",e.getMessage());
        } catch (ExecutionException e) {
            Log.d("JTO",e.getMessage());
        }

        return totalOperations;
    }

    public float getBalance(){


        AsyncTask<Void, Void, Float> myTask = new AsyncTask<Void, Void, Float>(){

            @Override
            protected Float doInBackground(Void... voids) {
                return daoAccess.getBalance();
            }

            @Override
            protected void onPostExecute(Float aFloat) {
                super.onPostExecute(aFloat);
                Log.d("JTO", ""+aFloat);
                myBalance = aFloat;

            }
        }.execute();


        try {
            myBalance = myTask.get();
        } catch (InterruptedException e) {
            Log.d("JTO",e.getMessage());
        } catch (ExecutionException e) {
            Log.d("JTO",e.getMessage());
        }

        return myBalance;
    }



    public List<Operation> mylistOperations() {


        AsyncTask<Void, Void, List<Operation>> allOperations = new AsyncTask<Void, Void, List<Operation>>() {

            @Override
            protected void onPostExecute(final List<Operation> operationList){
                try {
                    if (operationList.size() > 0) {

                    }
                } catch (NullPointerException excp) {
                    Log.d("JTO Log: ", excp.toString());
                }
            }

            @Override
            protected List<Operation> doInBackground(Void... voids) {
                return daoAccess.fetchAllOperations();
            }

        }.execute();

        try {
            totalOperations = allOperations.get();
        } catch (ExecutionException e) {
            Log.d("JTO",e.getMessage());
        } catch (InterruptedException e) {
            Log.d("JTO",e.getMessage());
        }

        return totalOperations;
    }


    public List<Operation> myLastThreeOperations() {


        AsyncTask<Void, Void, List<Operation>> allOperations = new AsyncTask<Void, Void, List<Operation>>() {

            @Override
            protected void onPostExecute(final List<Operation> operationList){
                try {
                    if (operationList.size() > 0) {

                    }
                } catch (NullPointerException excp) {
                    Log.d("JTO Log: ", excp.toString());
                }
            }

            @Override
            protected List<Operation> doInBackground(Void... voids) {
                return daoAccess.getLastThreeOperations();
            }

        }.execute();

        try {
            totalOperations = allOperations.get();
        } catch (ExecutionException e) {
            Log.d("JTO",e.getMessage());
        } catch (InterruptedException e) {
            Log.d("JTO",e.getMessage());
        }

        return totalOperations;
    }

    //public Operation myRequestedOperation(String myDescription, String myAmount, String dateInsertion) {
    public Operation myRequestedOperation(String remoteKey) {


        AsyncTask<String, Void, Operation> allOperations = new AsyncTask<String, Void, Operation>() {

            @Override
            protected void onPostExecute(final Operation myOperation){
                try {
                    Log.d("JTO Log: ", "success");
                } catch (NullPointerException excp) {
                    Log.d("JTO Log: ", excp.toString());
                }
            }

            @Override
            protected Operation doInBackground(String... strings) {
                //We retrieve the parameters we want, given in parameters
                //String myDescription = strings[0];
                String myKey = strings[0];
                //String myAmount = strings[1];
                //String dateInsertion = strings[2];
                //return daoAccess.getRequestedOperation(myDescription, myAmount, dateInsertion);
                return daoAccess.getRequestedOperation(myKey);
                //return daoAccess.getRequestedOperation(myDescription, dateInsertion);
            }

        }.execute(remoteKey);
    //}.execute(myDescription, myAmount, dateInsertion);

        try {
            myOperation = allOperations.get();
        } catch (ExecutionException e) {
            Log.d("JTO",e.getMessage());
        } catch (InterruptedException e) {
            Log.d("JTO",e.getMessage());
        }

        return myOperation;
    }

    List<Operation> getOperationsInYearPrior(int numberOfYearsBack) {


        AsyncTask<Integer, Void, List<Operation>> allOperations = new AsyncTask<Integer, Void, List<Operation>>() {

            @Override
            protected void onPostExecute(final List<Operation> operationList){
                try {
                    if (operationList.size() > 0) {

                    }
                } catch (NullPointerException excp) {
                    Log.d("JTO Log: ", excp.getMessage());
                }
            }

            @Override
            protected List<Operation> doInBackground(Integer... ints) {
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.FRANCE);
                DateFormat currentYearFormat = new SimpleDateFormat("yyyy", Locale.FRANCE);

                //We retrieve the number of months we want to jump back, given in parameters
                int yearsBack = ints[0];

                Date today = null;
                String actualYear = "";
                int daysInMonths = 0;
                int intActualYear = 0;

                try {
                    //Get the current day
                    today = dateFormat.parse(dateFormat.format(new Date()));
                    //Get the current year
                    actualYear = currentYearFormat.format(today);
                    intActualYear = Integer.parseInt(actualYear);

                    /**
                     * We compute the exact year when we go back
                     */

                    intActualYear = intActualYear - yearsBack;


                } catch (ParseException e) {
                    Log.d("JTOAAAAAA", e.getMessage());
                }

                //We append the start and end Dates
                String startDate = actualYear+"/"+"00/01";
                String endDate = actualYear+"/"+"11/31";

                //Execcute request
                return daoAccess.getOperationsInTimeInterval(startDate, endDate);
            }

        }.execute(numberOfYearsBack);

        try {
            totalOperations = allOperations.get();
        } catch (ExecutionException e) {
            Log.d("JTO",e.getMessage());
        } catch (InterruptedException e) {
            Log.d("JTO",e.getMessage());
        }

        return totalOperations;
    }


    List<Operation> getOperationsInLastNineteenDays() {


        AsyncTask<Void, Void, List<Operation>> allOperations = new AsyncTask<Void, Void, List<Operation>>() {

            @Override
            protected void onPostExecute(final List<Operation> operationList){
                try {
                    if (operationList.size() > 0) {

                    }
                } catch (NullPointerException excp) {
                    Log.d("JTO Log: ", excp.toString());
                }
            }

            @Override
            protected List<Operation> doInBackground(Void... voids) {
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.FRANCE);
                Date today = null;

                try {
                    //Get the current day
                    today = dateFormat.parse(dateFormat.format(new Date()));
                } catch (ParseException e) {
                    Log.d("JTOAAAAAA", e.getMessage());
                }

                long diff = today.getTime() - TimeUnit.DAYS.toMillis(90);
                Date lastDate = new Date(diff);

                //Execcute request
                return daoAccess.getOperationsInTimeInterval(dateFormat.format(lastDate), dateFormat.format(today));
            }

        }.execute();

        try {
            totalOperations = allOperations.get();
        } catch (ExecutionException e) {
            Log.d("JTO",e.getMessage());
        } catch (InterruptedException e) {
            Log.d("JTO",e.getMessage());
        }

        return totalOperations;
    }


    List<Operation> getOperationsInMonthPrior(int numberOfMonthsBack) {


        AsyncTask<Integer, Void, List<Operation>> allOperations = new AsyncTask<Integer, Void, List<Operation>>() {

            @Override
            protected void onPostExecute(final List<Operation> operationList){
                try {
                    if (operationList.size() > 0) {

                    }
                } catch (NullPointerException excp) {
                    Log.d("JTO Log: ", excp.getMessage());
                }
            }

            @Override
            protected List<Operation> doInBackground(Integer... ints) {
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.FRANCE);
                DateFormat currentYearFormat = new SimpleDateFormat("yyyy", Locale.FRANCE);
                DateFormat currentMonthFormat = new SimpleDateFormat("MM", Locale.FRANCE);

                //We retrieve the number of months we want to jump back, given in parameters
                int monthsBack = ints[0];

                Date today = null;
                String actualMonth = "";
                String actualYear = "";
                int daysInMonths = 0;
                int intActualMonth = 0;
                int intActualYear = 0;

                try {
                    //Get the current day
                    today = dateFormat.parse(dateFormat.format(new Date()));
                    //Get the current month
                    actualMonth = currentMonthFormat.format(today);
                    intActualMonth = Integer.parseInt(actualMonth);
                    //Get the current year
                    actualYear = currentYearFormat.format(today);
                    intActualYear = Integer.parseInt(actualYear);

                    /**
                     * We compute the exact month when we go back
                     * if we go back in previous year, we substract the number of
                     * year
                     */
                    if((intActualMonth - monthsBack) < 0){
                        intActualMonth = 11 + (intActualMonth - monthsBack);
                        intActualYear -= 1;
                    } else {
                        intActualMonth = intActualMonth - monthsBack;
                        intActualYear -= 0;
                    }

                } catch (ParseException e) {
                    Log.d("JTOAAAAAA", e.getMessage());
                }

                //Retrieve the number of days of the previous  month
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    YearMonth yearMonthObject = YearMonth.of(intActualYear, intActualMonth);
                    daysInMonths = yearMonthObject.lengthOfMonth();
                } else {
                    Calendar myCal = new GregorianCalendar(intActualYear, intActualMonth, 1);
                    daysInMonths = myCal.getActualMaximum(Calendar.DAY_OF_MONTH);
                }
                //long diff = today.getTime() - TimeUnit.DAYS.toMillis(10);
                //Date lastDate = new Date(diff);

                //Parse the string value of the previous month
                String stringActualMonth;
                if (intActualMonth < 10) {
                    stringActualMonth = "0"+intActualMonth;
                } else {
                    stringActualMonth = ""+intActualMonth;
                }

                //We append the start and end Dates
                String startDate = actualYear+"/"+stringActualMonth+"/01";
                String endDate = actualYear+"/"+stringActualMonth+"/"+daysInMonths;

                //Execcute request
                return daoAccess.getOperationsInTimeInterval(startDate, endDate);
            }

        }.execute(numberOfMonthsBack);

        try {
            totalOperations = allOperations.get();
        } catch (ExecutionException e) {
            Log.d("JTO",e.getMessage());
        } catch (InterruptedException e) {
            Log.d("JTO",e.getMessage());
        }

        return totalOperations;
    }


    List<Operation> getOperationsInWeekPrior(int numberOfWeeksBack) {


        AsyncTask<Integer, Void, List<Operation>> allOperations = new AsyncTask<Integer, Void, List<Operation>>() {

            @Override
            protected void onPostExecute(final List<Operation> operationList){
                try {
                    if (operationList.size() > 0) {

                    }
                } catch (NullPointerException excp) {
                    Log.d("JTO Log: ", excp.toString());
                }
            }

            @Override
            protected List<Operation> doInBackground(Integer... ints) {
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.FRANCE);

                //We retrieve the number of months we want to jump back, given in parameters
                int weeksBack = ints[0];

                Calendar cal1 = Calendar.getInstance();
                Calendar cal2 = Calendar.getInstance();
                int week1 = cal1.get(Calendar.WEEK_OF_YEAR) - weeksBack;
                int week2 = cal2.get(Calendar.WEEK_OF_YEAR) - weeksBack + 1;

                cal1.set(Calendar.WEEK_OF_YEAR, week1);
                cal2.set(Calendar.WEEK_OF_YEAR, week2);

                cal1.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                cal2.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);


                String startDate = dateFormat.format(cal1.getTime());
                String endDate = dateFormat.format(cal2.getTime());

                //Execute request
                return daoAccess.getOperationsInTimeInterval(startDate, endDate);
            }

        }.execute(numberOfWeeksBack);

        try {
            totalOperations = allOperations.get();
        } catch (ExecutionException e) {
            Log.d("JTO",e.getMessage());
        } catch (InterruptedException e) {
            Log.d("JTO",e.getMessage());
        }

        return totalOperations;
    }
}
