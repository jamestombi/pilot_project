package com.katikatech.james.pilot_project;

import android.app.SearchManager;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.List;

public class SearchableActivity extends AppCompatActivity {

    public List<Operation> matchingOperations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_layout);

        // Get the intent, verify the action and get the query
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Log.d("JTO", "Query string "+ query);
            doMySearch("%"+query+"%");
        }


    }

    private void doMySearch(String query) {
         matchingOperations = new OperationRepository
                (this.getApplication()).retrieveOperationsLookingLike(query);

    }
}

