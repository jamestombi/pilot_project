package com.katikatech.james.pilot_project.SharedPreferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.katikatech.james.pilot_project.Operation;
import com.google.gson.Gson;

public class CurrentOperationSharedPreferences {
    private static final String OPERATION_PREF = "operation_information";
    private static final String OPERATION_DESCRIPTION = "operation_description";
    private static final String OPERATION_AMOUNT = "operation_amount";
    private static final String OPERATION_CATEGORY = "operation_category";
    private static final String OPERATION_TOTAL_ELEMENT = "operation_total_element";
    private static final String OPERATION_DATE_INSERTION = "operation_date_insertion";
    private static final String OPERATION_FETCHED = "operation_fetched";
    private static final String OPERATION_COMPLETE = "complete_operation";


    private SharedPreferences mSharedPreferences;

    public CurrentOperationSharedPreferences(Context context) {
        this.mSharedPreferences =  context.getSharedPreferences(OPERATION_PREF, Context.MODE_PRIVATE);
    }

    public void setOperationDescription(String operationDescription){
        this.mSharedPreferences.edit().putString(OPERATION_DESCRIPTION, operationDescription).apply();
    }

    public void setOperationAmount(Float operationAmount){
        this.mSharedPreferences.edit().putFloat(OPERATION_AMOUNT, operationAmount).apply();
    }

    public void setOperationTotalElement(Float operationTotalElement){
        this.mSharedPreferences.edit().putFloat(OPERATION_TOTAL_ELEMENT, operationTotalElement).apply();
    }

    public void setOperationDateInsertion(String operationDateInsertion){
        this.mSharedPreferences.edit().putString(OPERATION_DATE_INSERTION, operationDateInsertion).apply();
    }

    public void setOperationCategory(int operationCategory){
        this.mSharedPreferences.edit().putInt(OPERATION_CATEGORY, operationCategory).apply();
    }

    public void setOperationFetched(boolean operationFetched){
        this.mSharedPreferences.edit().putBoolean(OPERATION_FETCHED, operationFetched).apply();
    }

    public void setOperationComplete(Operation operationComplete){
        Gson myGson = new Gson();
        String jsonify = myGson.toJson(operationComplete);
        this.mSharedPreferences.edit().putString(OPERATION_COMPLETE, jsonify).apply();
    }



    public String getOperationDescription(){
        return this.mSharedPreferences.getString(OPERATION_DESCRIPTION, "");
    }

    public float getOperationAmount(){
        return this.mSharedPreferences.getFloat(OPERATION_AMOUNT, 0.0f);
    }

    public float getOperationTotalElement(){
        return this.mSharedPreferences.getFloat(OPERATION_TOTAL_ELEMENT, 0.0f);
    }

    public String getOperationDateInsertion(){
        return this.mSharedPreferences.getString(OPERATION_DATE_INSERTION, "");
    }

    public boolean getOperationFetched(){
        return this.mSharedPreferences.getBoolean(OPERATION_FETCHED, false);
    }

    public int getOperationCategory(){
        return this.mSharedPreferences.getInt(OPERATION_CATEGORY, 0);
    }

    public Operation getOperationComplete(){
        Gson myGson = new Gson();
        String myJson = this.mSharedPreferences.getString(OPERATION_COMPLETE, "");
        return myGson.fromJson(myJson, Operation.class);
    }

}
