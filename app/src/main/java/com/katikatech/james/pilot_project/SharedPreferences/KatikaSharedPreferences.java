package com.katikatech.james.pilot_project.SharedPreferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.katikatech.james.pilot_project.Currency;

import java.util.ArrayList;
import java.util.List;


public class KatikaSharedPreferences {
    private static final String SHARED_PREF = "account_information";
    private static final String FIRST_ACCOUNT_CREATION = "first_account_creation"; /*boolean telling if you are creating your account*/
    private static final String COMPLETE_PRESENTATION = "complete_presentation"; /* boolean telling if the slider is complete */
    private static final String USER_NAME = "user_name";
    private static final String USER_FIRST_NAME = "user_first_name";
    private static final String USER_PHONE = "user_phone";

    private static final String TEMP_NAME = "temp_name";
    private static final String TEMP_FIRST_NAME = "temp_first_name";
    private static final String TEMP_PHONE = "temp_phone";

    private static final String USER_GENDER = "user_gender";
    private static final String LAST_UPDATE = "last_update";
    private static final String ACTION_START_TIMEOUT = "start_timeout";
    private static final String ACTION_STOP_TIMEOUT = "stop_timeout";
    private static final String CURRENCY_ID = "currency_id"; /*Id of your currency */
    private static final String CURRENCY_SYMBOL = "currency_symbol"; /* symbol of your selected currency */
    private static final String ACCOUNT_CONNECTED = "account_connected"; /* boolean telling if your account is connected*/
    private static final String ACTIVITY_ON = "activity_on"; /*boolean telling if you have an activity in the App */
    private static final String USER_ID = "user_id";
    private static final String USER_PASS = "user_pass";
    private static final String TIMEOUT_LOGOFF = "timeout_logoff"; /*Integer (in millis) relating the time before sending a notification of connection*/
    private static final String ACCOUNT_BALANCE = "account_balance";
    private static final String ONLINE_MODE = "online_mode"; /*boolean telling if you online or not */
    private static final String LOCK_BAD_PSWD = "lock_bad_password"; /*boolean telling if the app is lock due to 3 bad passwords */

    public static final float KATIKA_MAX = 1000000000f;
    public static final float KATIKA_MIN = -1000000000f;
    public static final float KATIKA_MAX_ITEMS = 10000f;

    public static final ArrayList<String> KatikaOPerationCategories = new ArrayList<String>(){{
        add("Revenus divers");
        add("Dépenses diverses");
        add("Alimentation");
        add("Transport");
        add("Abonnement");
        add("Loyer");
        add("Salaires");
        add("Impôts et taxes");
    }};

    public static final ArrayList<Currency> KatikalistCurrencies = new ArrayList<Currency>(){{
        add(new Currency(1, "XAF", "Zone Franc (Afrique Centrale)"));
        add(new Currency(2, "XOF", "Zone Franc (Afrique de l'Ouest)"));
        add(new Currency(3, "EUR", "Zone EURO"));
        add(new Currency(4, "JPY", "Japon"));
        add(new Currency(5, "USD", "Dollar Américain"));
    }};

    private SharedPreferences mSharedPreferences;

    public KatikaSharedPreferences(Context context) {
        this.mSharedPreferences =  context.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE);
    }

    public void setFirstAccountCreation(boolean accountCreated){
        this.mSharedPreferences.edit().putBoolean(FIRST_ACCOUNT_CREATION, accountCreated).apply();
    }

    public void setAccountConnected(boolean accountConnected){
        this.mSharedPreferences.edit().putBoolean(ACCOUNT_CONNECTED, accountConnected).apply();
    }

    public void setActivityOn(boolean activityOn){
        this.mSharedPreferences.edit().putBoolean(ACTIVITY_ON, activityOn).apply();
    }

    public void setOnlineMode(boolean onlineMode){
        this.mSharedPreferences.edit().putBoolean(ONLINE_MODE, onlineMode).apply();
    }

    public void setLockBadPswd(int lockBadPswd){
        this.mSharedPreferences.edit().putInt(ONLINE_MODE, lockBadPswd).apply();
    }


    public void setCompletePresentation(boolean completePresentation){
        this.mSharedPreferences.edit().putBoolean(COMPLETE_PRESENTATION, completePresentation).apply();
    }

    public void setUserName(String userName){
        this.mSharedPreferences.edit().putString(USER_NAME, userName).apply();
    }

    public void setUserFirstName(String userFirstname){
        this.mSharedPreferences.edit().putString(USER_FIRST_NAME, userFirstname).apply();
    }

    public void setUserPhone(String userPhone){
        this.mSharedPreferences.edit().putString(USER_PHONE, userPhone).apply();
    }

    public void setTempName(String tempName){
        this.mSharedPreferences.edit().putString(TEMP_NAME, tempName).apply();
    }

    public void setTempFirstName(String tempFirstname){
        this.mSharedPreferences.edit().putString(TEMP_FIRST_NAME, tempFirstname).apply();
    }

    public void setTempPhone(String tempPhone){
        this.mSharedPreferences.edit().putString(TEMP_PHONE, tempPhone).apply();
    }

    public void setLastUpdate(String LastUpdate){
        this.mSharedPreferences.edit().putString(LAST_UPDATE, LastUpdate).apply();
    }

    public void setUserGender(String gender){
        this.mSharedPreferences.edit().putString(USER_GENDER, gender).apply();
    }

    public void setUserCurrencyId(int currencyId){
        this.mSharedPreferences.edit().putInt(CURRENCY_ID, currencyId).apply();
    }

    public void setUserCurrencySymbol(String currencySymbol){
        this.mSharedPreferences.edit().putString(CURRENCY_SYMBOL, currencySymbol).apply();
    }

    public void setUserId(String userId){
        this.mSharedPreferences.edit().putString(USER_ID, userId).apply();
    }

    public void setUserPass(String userPass){
        this.mSharedPreferences.edit().putString(USER_PASS, userPass).apply();
    }

    public void setTimeoutLogoff(int timeoutLogoff){
        this.mSharedPreferences.edit().putInt(TIMEOUT_LOGOFF, timeoutLogoff).apply();
    }

    public void setAccountBalance(float accountBalance){
        this.mSharedPreferences.edit().putFloat(ACCOUNT_BALANCE, accountBalance).apply();
    }




    public boolean getFirstAccountCreation(){
        return this.mSharedPreferences.getBoolean(FIRST_ACCOUNT_CREATION, false);
    }

    public boolean getAccountConnected(){
        return this.mSharedPreferences.getBoolean(ACCOUNT_CONNECTED, false);
    }

    public boolean getActivityOn(){
        return this.mSharedPreferences.getBoolean(ACTIVITY_ON, false);
    }

    public boolean getOnlineMode(){
        return this.mSharedPreferences.getBoolean(ONLINE_MODE, false);
    }

    public int getLockBadPswd(){
        return this.mSharedPreferences.getInt(LOCK_BAD_PSWD, 0);
    }

    public boolean getCompletePresentation(){
        return this.mSharedPreferences.getBoolean(COMPLETE_PRESENTATION, false);
    }

    public String getUserName(){
        return this.mSharedPreferences.getString(USER_NAME, "NAME");
    }

    public String getUserFirstName(){
        return this.mSharedPreferences.getString(USER_FIRST_NAME, "FIRSTNAME");
    }

    public String getUserPhone(){
        return this.mSharedPreferences.getString(USER_PHONE, "00000000");
    }

    public String getTempName(){
        return this.mSharedPreferences.getString(TEMP_NAME, "");
    }

    public String getTempFirstName(){
        return this.mSharedPreferences.getString(TEMP_FIRST_NAME, "");
    }

    public String getTempPhone(){
        return this.mSharedPreferences.getString(TEMP_PHONE, "");
    }

    public String getLastUpdate(){
        return this.mSharedPreferences.getString(LAST_UPDATE, "");
    }

    public String getActionStartTimeout(){
        return this.mSharedPreferences.getString(ACTION_START_TIMEOUT, "com.example.james.pilot_project.action.SET_TIMEOUT");
    }

    public String getActionStopTimeout(){
        return this.mSharedPreferences.getString(ACTION_STOP_TIMEOUT, "com.example.james.pilot_project.action.CANCEL_TIMEOUT");
    }

    public String getUserGender(){
        return this.mSharedPreferences.getString(USER_GENDER, "");
    }

    public int getUserCurrencyId(){
        return this.mSharedPreferences.getInt(CURRENCY_ID, 0);
    }

    public String getUserCurrencySymbol(){
        return this.mSharedPreferences.getString(CURRENCY_SYMBOL, "");
    }

    public int getTimeOutLogoff(){
        return this.mSharedPreferences.getInt(TIMEOUT_LOGOFF, 900000);
    }

    public float getAccountBalance(){
        return this.mSharedPreferences.getFloat(ACCOUNT_BALANCE, 0.f);
    }

    public String getUserId(){
        return this.mSharedPreferences.getString(USER_ID, "");
    }

    public String getUserPass(){
        return this.mSharedPreferences.getString(USER_PASS, "");
    }

}
