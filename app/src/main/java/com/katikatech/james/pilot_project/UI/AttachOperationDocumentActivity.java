package com.katikatech.james.pilot_project.UI;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.services.NotificationService;
import com.katikatech.james.pilot_project.R;

public class AttachOperationDocumentActivity extends AppCompatActivity {

    private KatikaSharedPreferences preferences;
    Toolbar toolbarAttachFile;
    final static String TAG = "AttachDocuments";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attach_operation_document);

        //Orientation portrait programmatically
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        /**
         * Check connection
         */
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(true);
        if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(AttachOperationDocumentActivity.this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        }

        //Toolbar
        toolbarAttachFile = (Toolbar) findViewById(R.id.toolbar_attach_document);
        setSupportActionBar(toolbarAttachFile);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color
            window.setStatusBarColor(ContextCompat.getColor(getApplication(),R.color.colorWhite));

            toolbarAttachFile.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    //we press the back button
    public void onBackPressed() {
        preferences.setAccountConnected(true);
        Intent parentIntent = new Intent(AttachOperationDocumentActivity.this, DetailedOperationActivity.class);
        startActivity(parentIntent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            preferences.setAccountConnected(true);
            Intent detailsIntent = new Intent(AttachOperationDocumentActivity.this, DetailedOperationActivity.class);
            startActivity(detailsIntent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        preferences = new KatikaSharedPreferences(this);
        NotificationService.stopActionTimeOut(AttachOperationDocumentActivity.this);

        /**
         * Check connection
         */
        if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(AttachOperationDocumentActivity.this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        } else {
            preferences.setAccountConnected(false);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(false);
        //preferences.setAccountConnected(true);
        NotificationService.startActionTimeOut(AttachOperationDocumentActivity.this);
        Log.d(TAG, "paused, status connected: "+ preferences.getAccountConnected());
    }

    @Override
    protected void onStop() {
        super.onStop();
        preferences = new KatikaSharedPreferences(this);
        if(!preferences.getActivityOn()) {
            preferences.setAccountConnected(false);
            Log.d(TAG, "stopped, status connected: " + preferences.getAccountConnected());
        } else {
            preferences.setAccountConnected(true);
            Log.d(TAG, "connected, stopped, status connected: " + preferences.getAccountConnected());
        }
    }
}
