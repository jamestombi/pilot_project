package com.katikatech.james.pilot_project.UI;

import android.content.Intent;
import android.os.Build;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.katikatech.james.pilot_project.Category;
import com.katikatech.james.pilot_project.recyclerview.CategorySelectionRecyclerView;
import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.services.NotificationService;
import com.katikatech.james.pilot_project.R;

import java.util.ArrayList;
import java.util.List;

public class CategoryActivity extends AppCompatActivity {

    KatikaSharedPreferences preferences;
    Toolbar toolbarCategory;
    RecyclerView categoryRecyclerView;
    CategorySelectionRecyclerView mSelectionRecyclerView;
    public final static String TAG = "CategoryPage";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        /**
         * Check connection
         */
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(true);
        if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(CategoryActivity.this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        }

        //Toolbar
        toolbarCategory = (Toolbar) findViewById(R.id.toolbar_category_selection);
        setSupportActionBar(toolbarCategory);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color
            window.setStatusBarColor(ContextCompat.getColor(getApplication(),R.color.colorWhite));

            toolbarCategory.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        List<Category> allCategories = new ArrayList<Category>();

        /*
        allCategories.add(new Category(1, "Revenus divers"));
        allCategories.add(new Category(2, "Dépenses diverses"));
        allCategories.add(new Category(3, "Alimentation"));
        allCategories.add(new Category(4, "Transport"));
        allCategories.add(new Category(5, "Abonnement"));
        allCategories.add(new Category(6, "Loyer"));
        allCategories.add(new Category(7, "Salaires"));
        allCategories.add(new Category(8, "Allocations"));
        */

        allCategories.add(new Category(0, "default"));
        for(int i = 0; i < KatikaSharedPreferences.KatikaOPerationCategories.size(); i++){
            allCategories.add(new Category(i+1, KatikaSharedPreferences.KatikaOPerationCategories.get(i)));
        }
        /*
        allCategories.add(new Category(1, KatikaSharedPreferences.KatikaOPerationCategories.get(0)));
        allCategories.add(new Category(2, KatikaSharedPreferences.KatikaOPerationCategories.get(1)));
        allCategories.add(new Category(3, KatikaSharedPreferences.KatikaOPerationCategories.get(2)));
        allCategories.add(new Category(4, KatikaSharedPreferences.KatikaOPerationCategories.get(3)));
        allCategories.add(new Category(5, KatikaSharedPreferences.KatikaOPerationCategories.get(4)));
        allCategories.add(new Category(6, KatikaSharedPreferences.KatikaOPerationCategories.get(5)));
        allCategories.add(new Category(7, KatikaSharedPreferences.KatikaOPerationCategories.get(6)));
        allCategories.add(new Category(8, KatikaSharedPreferences.KatikaOPerationCategories.get(7)));
        */

        //Fill the recyclerView with staticData
        categoryRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_category_selection);
        categoryRecyclerView.setLayoutManager(new LinearLayoutManager(CategoryActivity.this));

        mSelectionRecyclerView = new CategorySelectionRecyclerView(allCategories);
        //adapt all the outputs of the list to the design of the new adapter
        categoryRecyclerView.setAdapter(mSelectionRecyclerView);
    }


    //we press the back button
    public void onBackPressed() {
        preferences.setAccountConnected(true);
        Intent parentIntent = new Intent(CategoryActivity.this, DetailedOperationActivity.class);
        startActivity(parentIntent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            preferences.setAccountConnected(true);
            Intent detailsIntent = new Intent(CategoryActivity.this, DetailedOperationActivity.class);
            startActivity(detailsIntent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        preferences = new KatikaSharedPreferences(this);
        NotificationService.stopActionTimeOut(CategoryActivity.this);

        /**
         * Check connection
         */
        if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(CategoryActivity.this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        } else {
            preferences.setAccountConnected(false);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(false);
        //preferences.setAccountConnected(true);
        NotificationService.startActionTimeOut(CategoryActivity.this);
        Log.d(TAG, "paused, status connected: "+ preferences.getAccountConnected());
    }

    @Override
    protected void onStop() {
        super.onStop();
        preferences = new KatikaSharedPreferences(this);
        if(!preferences.getActivityOn()) {
            preferences.setAccountConnected(false);
            Log.d(TAG, "stopped, status connected: " + preferences.getAccountConnected());
        } else {
            preferences.setAccountConnected(true);
            Log.d(TAG, "connected, stopped, status connected: " + preferences.getAccountConnected());
        }
    }
}
