package com.katikatech.james.pilot_project.UI;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.katikatech.james.pilot_project.Operation;
import com.katikatech.james.pilot_project.OperationRepository;
import com.katikatech.james.pilot_project.R;
import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.services.NotificationService;
import com.katikatech.james.pilot_project.services.PhoneNumberVerifierService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class CheckPhoneActivity extends AppCompatActivity {
    private KatikaSharedPreferences preferences;
    final static String TAG = "Check Phone";
    Boolean recoverData;
    Boolean success;
    //Toolbar toolbarGoodInsertion;
    Button continueButton;
    Button backButton;
    TextView checkPhone;
    ImageView checkOk;
    ImageView checkKO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_phone);

        //Orientation portrait programmatically
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        /**
         * Check connection
         */
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(true);
        if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(CheckPhoneActivity.this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        }

        //Toolbar
        //toolbarGoodInsertion = (Toolbar) findViewById(R.id.toolbar_good_insertion);
        //setSupportActionBar(toolbarGoodInsertion);

        continueButton = (Button) findViewById(R.id.SubmitContinueButton);
        backButton = (Button) findViewById(R.id.SubmitBackButton);
        checkPhone = (TextView) findViewById(R.id.text_print_success);
        checkKO = (ImageView) findViewById(R.id.checkKOImage);
        checkOk = (ImageView) findViewById(R.id.shieldOKImage);



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color
            window.setStatusBarColor(ContextCompat.getColor(getApplication(),R.color.colorWhite));

            //toolbarGoodInsertion.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        //retrieve the extra

        try{
            success = getIntent().getExtras().getBoolean("success");
            Log.d(TAG, "CHECK RESULT " + success);
        } catch (NullPointerException excp){
            success = false;
            Log.d(TAG, "NO CHECK RESULT " + success);
            //Log.d(TAG, "isFromUserBoard FAILED");
        }


        try{
            recoverData = getIntent().getExtras().getBoolean("recovering_data");
            Log.d(TAG, "CHECK RESULT " + recoverData);
        } catch (NullPointerException excp){
            recoverData = false;
            Log.d(TAG, "NO CHECK RESULT " + recoverData);
            //Log.d(TAG, "isFromUserBoard FAILED");
        }

        if (recoverData){
            preferences.setFirstAccountCreation(true);
        }

        final Boolean recover = recoverData;

        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //checkPhone.setText(getString(R.string.text_phone_ok));

        //TODO: prepare the backend function to retrieve all the operations of the user

        if (success) {
            checkPhone.setText(getString(R.string.text_phone_ok));
            backButton.setVisibility(View.INVISIBLE);
            checkOk.setVisibility(View.VISIBLE);
            checkKO.setVisibility(View.INVISIBLE);
            if (recoverData){

                continueButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        OperationRepository OldOperationRepository = new OperationRepository(getApplication());
                        OldOperationRepository.clearAllExistingOperations();
                        preferences.setFirstAccountCreation(true);
                        new RecoverAsyncTask(CheckPhoneActivity.this)
                               .execute(getResources().getString(R.string.url_request_sync_all), preferences.getUserId());
                    }
                });

            } else if (!preferences.getFirstAccountCreation()) {

                continueButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        preferences.setAccountConnected(true);
                        Intent createPasswordIntent = new Intent(CheckPhoneActivity.this, CreatePasswordActivity.class);
                        startActivity(createPasswordIntent);
                        finish();
                    }
                });

            } else {
                continueButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        preferences.setUserFirstName(preferences.getTempFirstName());
                        preferences.setUserName(preferences.getTempName());
                        preferences.setUserPhone(preferences.getTempPhone());
                        PhoneNumberVerifierService.startActionUpdateUser(CheckPhoneActivity.this, preferences.getUserId(), preferences.getUserFirstName(), preferences.getUserName(), preferences.getUserPhone());
                        preferences.setAccountConnected(true);
                        Intent userBoardIntent = new Intent(CheckPhoneActivity.this, UserBoard.class);

                        startActivity(userBoardIntent);
                        finish();
                    }
                });

            }
        } else {
            checkOk.setVisibility(View.INVISIBLE);
            checkKO.setVisibility(View.VISIBLE);
            checkPhone.setText(getString(R.string.toast_unverified));
            continueButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(CheckPhoneActivity.this);
                    builder.setMessage(getResources().getString(R.string.continue_without_confirm))
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent continueIntent;
                                    if(!preferences.getFirstAccountCreation()) {
                                        continueIntent = new Intent(CheckPhoneActivity.this, CreatePasswordActivity.class);
                                    } else {
                                        preferences.setUserFirstName(preferences.getTempFirstName());
                                        preferences.setUserName(preferences.getTempName());
                                        preferences.setUserPhone(preferences.getTempPhone());
                                        if(!recoverData) {
                                            PhoneNumberVerifierService.startActionUpdateUser(CheckPhoneActivity.this, preferences.getUserId(), preferences.getUserFirstName(), preferences.getUserName(), preferences.getUserPhone());
                                        }
                                        preferences.setAccountConnected(true);
                                        continueIntent = new Intent(CheckPhoneActivity.this, UserBoard.class);
                                    }
                                    startActivity(continueIntent);
                                    finish();
                                    dialog.dismiss();
                                }
                            }).setNegativeButton(getResources().getText(R.string.cancel), null);

                    AlertDialog alert = builder.create();
                    alert.show();

                }
            });

            backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    PhoneNumberVerifierService.startActionVerify(CheckPhoneActivity.this, preferences.getTempPhone());
                    preferences.setAccountConnected(true);
                    Intent detailsIntent = new Intent(CheckPhoneActivity.this, PhoneVerifier.class);
                    if(recoverData){
                        detailsIntent.putExtra("recovering_data", 1);
                    }
                    startActivity(detailsIntent);
                    finish();
                }
            });
        }
    }


    //we press the back button
    public void onBackPressed() {

        if(!success) {
            PhoneNumberVerifierService.startActionVerify(CheckPhoneActivity.this, preferences.getTempPhone());
            preferences.setAccountConnected(true);
            Intent detailsIntent = new Intent(CheckPhoneActivity.this, PhoneVerifier.class);
            if (recoverData) {
                detailsIntent.putExtra("recovering_data", 1);
            }
            startActivity(detailsIntent);
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {

            if(!success) {
                PhoneNumberVerifierService.startActionVerify(CheckPhoneActivity.this, preferences.getTempPhone());
                preferences.setAccountConnected(true);
                Intent detailsIntent = new Intent(CheckPhoneActivity.this, PhoneVerifier.class);
                if (recoverData) {
                    detailsIntent.putExtra("recovering_data", 1);
                }
                startActivity(detailsIntent);
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        preferences = new KatikaSharedPreferences(this);
        NotificationService.stopActionTimeOut(CheckPhoneActivity.this);

        /**
         * Check connection
         */
        /*if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(CheckPhoneActivity.this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        } else {
            preferences.setAccountConnected(false);
        } */
    }

    @Override
    protected void onPause() {
        super.onPause();
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(false);
        //preferences.setAccountConnected(true);
        NotificationService.startActionTimeOut(CheckPhoneActivity.this);
        Log.d(TAG, "paused, status connected: "+ preferences.getAccountConnected());
    }

    @Override
    protected void onStop() {
        super.onStop();
        preferences = new KatikaSharedPreferences(this);
        if(!preferences.getActivityOn()) {
            preferences.setAccountConnected(false);
            Log.d(TAG, "stopped, status connected: " + preferences.getAccountConnected());
        } else {
            preferences.setAccountConnected(true);
            Log.d(TAG, "connected, stopped, status connected: " + preferences.getAccountConnected());
        }
    }

    private class RecoverAsyncTask  extends AsyncTask<String, Void, Boolean> {

        private Context mContext;

        public RecoverAsyncTask(Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ConstraintLayout mConstraintLayout = findViewById(R.id.loading_recover_data);
            mConstraintLayout.setVisibility(View.VISIBLE);

        }

        @Override
        protected void onPostExecute(Boolean aBool) {
            super.onPostExecute(aBool);
            ConstraintLayout mConstraintLayout = findViewById(R.id.loading_recover_data);
            mConstraintLayout.setVisibility(View.VISIBLE);
            if (aBool) {
                Log.d(TAG, "onPostExecute: success");
                preferences.setUserGender(""+preferences.getUserId().charAt(4));
                preferences.setAccountConnected(true);
                DateFormat humanDateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.FRANCE);
                preferences.setLastUpdate(humanDateFormat.format(new Date()));
                Intent userBoardIntent = new Intent(CheckPhoneActivity.this, UserBoard.class);
                userBoardIntent.putExtra("recovering_data", true);

                startActivity(userBoardIntent);
                finish();
            } else {
                //ConstraintLayout mConstraintLayout = findViewById(R.id.loading_layout_type_password);
                Log.d(TAG, "onPostExecute: failure");
                /* clear the text zone */


            }
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            String url = strings[0];
            JSONObject args = new JSONObject();
            RequestQueue requestQueue = Volley.newRequestQueue(CheckPhoneActivity.this);
            JSONObject response = new JSONObject();
            try {
                args.put("userid", preferences.getUserId());
                //editPassword.getText().clear();

                final RequestFuture<JSONObject> future = RequestFuture.newFuture();
                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                        url, args, future, future);
                requestQueue.add(request);
                try {
                    response = future.get(10, TimeUnit.SECONDS);
                    JSONArray syncOperations = response.getJSONArray("syncOperation");
                    for (int i = 0; i < syncOperations.length(); i++) {
                        Log.d("value " + i, syncOperations.getJSONObject(i).toString());
                        Operation currentOperation = getOperation(syncOperations.getJSONObject(i).toString());
                        Log.d("MAG  ---> ", " amount : " + currentOperation.getAmount() + " description: " + currentOperation.getDescription());
                        OperationRepository operationRepository = new OperationRepository(getApplication());

                        preferences.setUserCurrencyId(currentOperation.getIdCurrency() - 1);
                        preferences.setUserCurrencySymbol(KatikaSharedPreferences.KatikalistCurrencies
                                .get(currentOperation.getIdCurrency() - 1)
                                .getCurrencySymbol());


                        operationRepository.retrieveOperation(getApplication().
                                        getApplicationContext(),
                                currentOperation.getDescription(),
                                currentOperation.getAmount(),
                                currentOperation.getInsertedAt(), currentOperation.getModifiedAt(),
                                currentOperation.getIdCurrency(),currentOperation.getIdOperationCategory(),
                                currentOperation.getNumberItems(),
                                currentOperation.getRemoteKey());
                    }
                    return response.getBoolean("success");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    Log.e(TAG, "TIME OUT: " + e.getMessage());
                }
            } catch (JSONException excp) {
                Log.d(TAG, excp.getMessage());
            }
            return false;
        }

        private Operation getOperation(String operationJson){
            Gson myGson = new Gson();
            return myGson.fromJson(operationJson, Operation.class);
        }
    }
}
