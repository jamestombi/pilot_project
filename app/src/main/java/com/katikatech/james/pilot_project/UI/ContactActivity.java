package com.katikatech.james.pilot_project.UI;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.katikatech.james.pilot_project.contactMessages.AssistantMessage;
import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.contactMessages.Message;
import com.katikatech.james.pilot_project.recyclerview.MessageRecyclerView;
import com.katikatech.james.pilot_project.services.NotificationService;
import com.katikatech.james.pilot_project.R;
import com.katikatech.james.pilot_project.contactMessages.UserMessage;

import java.util.ArrayList;
import java.util.List;

public class ContactActivity extends AppCompatActivity {

    private KatikaSharedPreferences preferences;
    private Toolbar toolbarContactUs;
    private final static String TAG = "ContactUs";
    EditText messageText;
    public RecyclerView recyclerView;
    public MessageRecyclerView messageRecyclerView;
    List<Message> messages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        //Orientation portrait programmatically
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        /**
         * Check connection
         */
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(true);
        if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        }

        //Toolbar
        toolbarContactUs = (Toolbar) findViewById(R.id.toolbar_contact_us);
        setSupportActionBar(toolbarContactUs);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color
            window.setStatusBarColor(ContextCompat.getColor(getApplication(),R.color.colorLightGray));

            toolbarContactUs.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //We begin with static messages
        messages = new ArrayList<Message>();

        messages.add(new AssistantMessage("Mon assistant",
                "Bienvenu dans notre service de messagerie, vous pourrez communiquer avec votre assistant via cette plateforme"));

        //Fill the recyclerView with staticData
        recyclerView = (RecyclerView) findViewById(R.id.messages_view);
        //set a new linear layout manager and set the layout in the reverse mode
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ContactActivity.this);
        //linearLayoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
                recyclerView.scrollToPosition(messages.size());
            }
        });

        messageRecyclerView = new MessageRecyclerView(messages);
        //adapt all the outputs of the list to the design of the new adapter
        recyclerView.setAdapter(messageRecyclerView);


        messageText = (EditText) findViewById(R.id.editText);
    }

    //we press the back button
    public void onBackPressed() {
        preferences.setAccountConnected(true);
        Intent parentIntent = new Intent(ContactActivity.this, UserBoard.class);
        startActivity(parentIntent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            preferences.setAccountConnected(true);
            Intent parentIntent = new Intent(ContactActivity.this, UserBoard.class);
            startActivity(parentIntent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        preferences = new KatikaSharedPreferences(this);
        NotificationService.stopActionTimeOut(ContactActivity.this);

        /**
         * Check connection
         */
        if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(ContactActivity.this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        } else {
            preferences.setAccountConnected(false);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(false);
        //preferences.setAccountConnected(true);
        NotificationService.startActionTimeOut(ContactActivity.this);
        Log.d(TAG, "paused, status connected: "+ preferences.getAccountConnected());
    }

    @Override
    protected void onStop() {
        super.onStop();
        preferences = new KatikaSharedPreferences(this);
        if(!preferences.getActivityOn()) {
            preferences.setAccountConnected(false);
            Log.d(TAG, "stopped, status connected: " + preferences.getAccountConnected());
        } else {
            preferences.setAccountConnected(true);
            Log.d(TAG, "connected, stopped, status connected: " + preferences.getAccountConnected());
        }
    }

    public void sendMessage(View view) {
        String message = messageText.getText().toString();
        if (message.length() > 0) {
            //scaledrone.publish("observable-room", message);
            messageText.getText().clear();
            Toast.makeText(ContactActivity.this, message, Toast.LENGTH_LONG).show();
            messageRecyclerView.add(new UserMessage(message));
            //messageText.clearFocus();
            //InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        }
    }
}
