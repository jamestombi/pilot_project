package com.katikatech.james.pilot_project.UI;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.services.NotificationService;
import com.katikatech.james.pilot_project.services.PhoneNumberVerifierService;
import com.katikatech.james.pilot_project.R;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.CredentialPickerConfig;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.katikatech.james.pilot_project.utils.StatusBarManager;

public class CreateAccount extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    Toolbar toolbarCreateAccount;
    Button submitAccount;
    RadioGroup genderSelector;
    EditText userName;
    EditText userFirstName;
    EditText userPhone;
    private KatikaSharedPreferences preferences;
    ProgressBar dialog;

    private GoogleApiClient apiClient;

    public static final String TAG = "Account Creation";
    private static final int RESOLVE_HINT = 1000;
    FocusControl phoneFocus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        preferences = new KatikaSharedPreferences(this);
        genderSelector = (RadioGroup) findViewById(R.id.toggle_gender);
        userFirstName = (EditText) findViewById(R.id.firstname_user);
        userName = (EditText) findViewById(R.id.name_user);
        userPhone = (EditText) findViewById(R.id.phone_number_user);
        submitAccount = (Button) findViewById(R.id.submitAccountButton);
        //Orientation portrait programmatically
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //Toolbar
        toolbarCreateAccount = (Toolbar) findViewById(R.id.toolbar_account);
        setSupportActionBar(toolbarCreateAccount);

        /*
         * Check connection
         */
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(true);
        preferences.setAccountConnected(true);
        if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(CreateAccount.this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        }

        /*if(preferences.getFirstAccountCreation()) {
            toolbarCreateAccount.setTitle(R.string.modify_info);

            userFirstName.setText(preferences.getUserFirstName());
            userFirstName.setFocusableInTouchMode(true);
            userName.setText(preferences.getUserName());
            userName.setFocusableInTouchMode(true);
            userPhone.setText(preferences.getUserPhone());
            userPhone.setFocusableInTouchMode(true);
            if(preferences.getUserGender().contentEquals("1")) {
                genderSelector.check(R.id.button_mister);
            } else {
                genderSelector.check(R.id.button_madam);
            }
        } */

        phoneFocus = new FocusControl(userPhone);

        userPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus){
                    userPhone.setEnabled(true);
                    userPhone.requestFocus();
                    if (TextUtils.isEmpty(userPhone.getText().toString())) {
                        requestHint();
                    }
                }
            }
        });

        userPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userPhone.setEnabled(true);
                userPhone.requestFocus();
                if (TextUtils.isEmpty(userPhone.getText().toString())) {
                    requestHint();
                }
            }
        });


        /**
         * STATUS BAR MANAGEMENT
         */
        new StatusBarManager().translucideCustomStatusBar(CreateAccount.this, toolbarCreateAccount, R.color.colorWhite);


        /**
         * Manage Submission
         */
        submitAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean changeActivity = false;
                String userFirstNameString = "";
                String userNameString = "";
                String userPhoneString = "";


                try {
                    int userGender = genderSelector.getCheckedRadioButtonId();
                    if ( userGender == -1){
                        throw new RuntimeException(getResources().getString(R.string.miss_gender_exception));
                    } else {
                        if(userGender == R.id.button_mister){
                            //Toast.makeText(CreateAccount.this, "Monsieur", Toast.LENGTH_SHORT).show();
                            preferences.setUserGender("1");
                        } else if (userGender == R.id.button_madam){
                            //Toast.makeText(CreateAccount.this, "Madame", Toast.LENGTH_SHORT).show();
                            preferences.setUserGender("2");
                        }
                    }

                    userFirstNameString = userFirstName.getText().toString();
                    userNameString = userName.getText().toString();
                    userPhoneString = userPhone.getText().toString();
                    if (userFirstNameString.isEmpty()) {
                        if (userNameString.isEmpty()){
                            throw new RuntimeException(getResources().getString(R.string.miss_username_exception));
                        }
                    }
                    if (userNameString.isEmpty()){
                        throw new RuntimeException(getResources().getString(R.string.miss_username_exception));
                    }
                    if (userPhoneString.isEmpty()){
                        throw new RuntimeException(getResources().getString(R.string.miss_phone_exception));
                    }
                    if(!userPhoneString.contains("+")){
                        throw new RuntimeException(getResources().getString(R.string.miss_ind_phone));
                    }
                    if(userPhoneString.contains("(") || userPhoneString.contains(")") ){
                        throw new RuntimeException(getResources().getString(R.string.miss_ind_phone));
                    }

                    changeActivity = true;

                } catch (RuntimeException excp){
                    Toast.makeText(CreateAccount.this, excp.getMessage(), Toast.LENGTH_SHORT).show();
                }

                if(changeActivity) {

                    if(preferences.getUserPhone().contentEquals(userPhoneString)) {
                        preferences.setUserFirstName(userFirstNameString);
                        preferences.setUserName(userNameString);
                        PhoneNumberVerifierService.startActionUpdateUser(CreateAccount.this, preferences.getUserId(), preferences.getUserFirstName(), preferences.getUserName(), preferences.getUserPhone());
                        preferences.setAccountConnected(true);
                        Intent userBoardIntent = new Intent(CreateAccount.this, UserBoard.class);
                        startActivity(userBoardIntent);
                        finish();
                    } else {

                        //preferences.setUserFirstName(userFirstNameString);
                        preferences.setTempFirstName(userFirstNameString);

                        //preferences.setUserPhone(userPhoneString);
                        preferences.setTempPhone(userPhoneString);

                        //preferences.setUserName(userNameString);
                        preferences.setTempName(userNameString);

                        new AsyncTask<String, Void, Void>(){
                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                                dialog = new ProgressBar(CreateAccount.this);
                                ConstraintLayout mConstraintLayout = findViewById(R.id.loading_layout_create_account);
                                mConstraintLayout.setVisibility(View.VISIBLE);
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);
                                dialog.setProgress(100);
                                preferences.setAccountConnected(true);
                                Intent userBoardIntent = new Intent(CreateAccount.this, PhoneVerifier.class);
                                startActivity(userBoardIntent);
                                finish();
                            }

                            @Override
                            protected Void doInBackground(String... strings) {
                                String phoneString = strings[0];
                                PhoneNumberVerifierService.startActionVerify(CreateAccount.this, phoneString);
                                return null;
                            }
                        }.execute(userPhoneString);

                    }

                }
            }
        });


        /**
         * GoogleApiClient Builder initialised
         */

        apiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .enableAutoManage(this, this)
                .addApi(Auth.CREDENTIALS_API)
                .build();
    }

    /**
     * Backpress button management
     */
    public void onBackPressed() {

        boolean existingAccount = preferences.getFirstAccountCreation();
        if(existingAccount) {
            preferences.setAccountConnected(true);
            Intent userBoardIntent = new Intent(CreateAccount.this, UserBoard.class);
            startActivity(userBoardIntent);
            finish();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        preferences = new KatikaSharedPreferences(this);

        if(preferences.getFirstAccountCreation()) {
            toolbarCreateAccount.setTitle(R.string.modify_info);

            userFirstName.setText(preferences.getUserFirstName());
            userFirstName.setFocusableInTouchMode(true);
            userFirstName.requestFocus();

            userName.setText(preferences.getUserName());
            userName.setFocusableInTouchMode(true);
            userName.requestFocus();

            userPhone.setText(preferences.getUserPhone());
            userPhone.setFocusableInTouchMode(true);
            userPhone.requestFocus();

            if(preferences.getUserGender().contentEquals("1")) {
                genderSelector.check(R.id.button_mister);
            } else {
                genderSelector.check(R.id.button_madam);
            }
        } /*else {
            userFirstName.setText(preferences.getTempFirstName());
            userFirstName.setFocusableInTouchMode(true);
            userFirstName.requestFocus();

            userName.setText(preferences.getTempName());
            userName.setFocusableInTouchMode(true);
            userName.requestFocus();

            userPhone.setText(preferences.getTempPhone());
            userPhone.setFocusableInTouchMode(true);
            userPhone.requestFocus();
        } */

        NotificationService.stopActionTimeOut(CreateAccount.this);

        /**
         * Check connection
         */
        if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(CreateAccount.this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        } else {
            preferences.setAccountConnected(true);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(true);
        //preferences.setAccountConnected(true);
        NotificationService.startActionTimeOut(CreateAccount.this);
        Log.d(TAG, "pause, status connected: "+ preferences.getAccountConnected());
    }

    @Override
    protected void onStop() {
        super.onStop();
        preferences = new KatikaSharedPreferences(this);
        if(!preferences.getActivityOn()) {
            preferences.setAccountConnected(false);
            Log.d(TAG, "stop, activity off, status connected: " + preferences.getAccountConnected());
        } else {
            preferences.setAccountConnected(true);
            Log.d(TAG, "stop, activity on, status connected: " + preferences.getAccountConnected());
        }
    }


    // Construct a request for phone numbers and show the picker
    private void requestHint() {
        phoneFocus.hideKeyboard();
        HintRequest hintRequest = new HintRequest.Builder()
                .setHintPickerConfig(new CredentialPickerConfig.Builder()
                        .setShowCancelButton(true)
                        .build())
                .setPhoneNumberIdentifierSupported(true)
                .build();

        PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(
                apiClient, hintRequest);
        try {
            startIntentSenderForResult(intent.getIntentSender(),
                    RESOLVE_HINT, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    // Obtain the phone number from the result
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESOLVE_HINT) {
            if (resultCode == RESULT_OK) {
                Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);
                userPhone.setText(credential.getId());  //<-- will need to process phone number string
            } else {
                phoneFocus.showKeyboard();
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "Connected");
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.d(TAG, "GoogleApiClient is suspended with cause code: " + cause);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "GoogleApiClient failed to connect: " + connectionResult);
    }


    /**
     * Class FocusControl
     */
    class FocusControl {
        static final int POST_DELAY = 250;
        private Handler handler;
        private InputMethodManager manager;
        private View focus;

        /**
         * Keyboard focus controller
         *
         * Shows and hides the keyboard. Uses a runnable to do the showing as there are race
         * conditions with hiding the keyboard that this solves.
         *
         * @param focus The view element to focus and hide the keyboard from
         */
        public FocusControl(View focus) {
            handler = new Handler();
            manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            this.focus = focus;
        }

        /**
         * Focus the view and show the keyboard.
         */
        public void showKeyboard() {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    focus.requestFocus();
                    manager.showSoftInput(focus, InputMethodManager.SHOW_IMPLICIT);
                }
            }, POST_DELAY);
        }

        /**
         * Hide the keyboard.
         */
        public void hideKeyboard() {
            View currentView = getCurrentFocus();
            if (currentView.equals(focus)) {
                manager.hideSoftInputFromWindow(currentView.getWindowToken(), 0);
            }
        }
    }

}
