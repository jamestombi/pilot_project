package com.katikatech.james.pilot_project.UI;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.R;
import com.katikatech.james.pilot_project.User;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.katikatech.james.pilot_project.utils.StatusBarManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class CreatePasswordActivity extends AppCompatActivity {

    private KatikaSharedPreferences preferences;
    Toolbar toolbarTypePassword;
    EditText createPassword1;
    EditText createPassword2;
    TextView viewInstruction;
    TextView viewBadPassword;
    Button createPasswordAndAccount;
    DatabaseReference userDatabase;

    public final String TAG = "CREATE PASSWORD";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_password);


        //Orientation portrait programmatically
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //Toolbar
        toolbarTypePassword = (Toolbar) findViewById(R.id.toolbar_create_password);
        setSupportActionBar(toolbarTypePassword);

        preferences = new KatikaSharedPreferences(this);

        // FIREBASE INITIALISATION
        userDatabase = FirebaseDatabase.getInstance().getReference("Users");

        createPassword1 = (EditText) findViewById(R.id.user_create_password_1);
        createPassword2 = (EditText) findViewById(R.id.user_create_password_2);
        viewBadPassword = (TextView) findViewById(R.id.bad_password_created);
        viewInstruction = (TextView) findViewById(R.id.text_explane_create_password);
        createPasswordAndAccount = (Button) findViewById(R.id.SubmitPasswordButton);


        new StatusBarManager().translucideCustomStatusBar(CreatePasswordActivity.this,toolbarTypePassword, R.color.colorLightGray);


        createPasswordAndAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String password1 = createPassword1.getText().toString();
                String password2 = createPassword2.getText().toString();
                final int myPass1, myPass2;
                if (password1.isEmpty()) {
                    viewBadPassword.setText(getResources().getString(R.string.empty_password));
                    viewBadPassword.setVisibility(View.VISIBLE);
                } else if (password1.length() != 4) {
                    viewBadPassword.setText(getResources().getString(R.string.pass_size_differs_four));
                    viewBadPassword.setVisibility(View.VISIBLE);
                } else if (password1.contentEquals("0000")) {
                    viewBadPassword.setText(getResources().getString(R.string.four_zeros_bad_pass));
                    viewBadPassword.setVisibility(View.VISIBLE);
                } else if (password1.contentEquals("1234")) {
                    viewBadPassword.setText(getResources().getString(R.string.first_num_bad_pass));
                    viewBadPassword.setVisibility(View.VISIBLE);
                } else {
                    try {
                        myPass1 = Integer.parseInt(password1);
                        myPass2 = Integer.parseInt(password2);
                        if (myPass1 != myPass2){
                            viewBadPassword.setText(getResources().getString(R.string.unconfirmed_pass));
                            viewBadPassword.setVisibility(View.VISIBLE);
                        } else {
                            //Here push User informations to the server
                            preferences.setUserId(generateUserId());
                            preferences.setUserPass(password1);

                            new AsyncTask<String, Void, String>(){
                                @Override
                                protected void onPreExecute() {
                                    super.onPreExecute();
                                    ConstraintLayout mConstraintLayout = findViewById(R.id.loading_layout_create_password);
                                    mConstraintLayout.setVisibility(View.VISIBLE);
                                }

                                @Override
                                protected void onPostExecute(String aString) {
                                    super.onPostExecute(aString);
                                    ConstraintLayout mConstraintLayout = findViewById(R.id.loading_layout_create_password);
                                    mConstraintLayout.setVisibility(View.INVISIBLE);
                                    preferences.setUserId(aString);

                                    preferences.setUserFirstName(preferences.getTempFirstName());
                                    preferences.setUserName(preferences.getTempName());
                                    preferences.setUserPhone(preferences.getTempPhone());

                                    String databaseId = userDatabase.push().getKey();

                                    //Push User values to database
                                    User newUser = new User();
                                    newUser.setFirstName(preferences.getUserFirstName());
                                    newUser.setName(preferences.getUserName());
                                    newUser.setIdUser(preferences.getUserId());
                                    newUser.setPhoneNumber(preferences.getUserPhone());
                                    newUser.setPassWord(password1);

                                    userDatabase.child(databaseId).setValue(newUser);

                                    preferences.setFirstAccountCreation(true);
                                    preferences.setAccountConnected(true);
                                    Intent userIntent = new Intent(CreatePasswordActivity.this, UserBoard.class);
                                    startActivity(userIntent);
                                    finish();
                                }

                                @Override
                                protected String doInBackground(String... strings) {
                                    String url = strings[0];
                                    String userIdInitString = strings[1];
                                    JSONObject args = new JSONObject();
                                    RequestQueue requestQueue = Volley.newRequestQueue(CreatePasswordActivity.this);
                                    JSONObject response = new JSONObject();
                                    try {
                                        args.put("initUserId", userIdInitString);

                                        final RequestFuture<JSONObject> future = RequestFuture.newFuture();
                                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                                                url, args, future, future);
                                        requestQueue.add(request);
                                        try {
                                            response = future.get(10, TimeUnit.SECONDS);
                                            return response.getString("userId");
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        } catch (ExecutionException e) {
                                            e.printStackTrace();
                                        } catch (TimeoutException e) {
                                            Log.e(TAG, "TIME OUT"+e.getMessage());
                                        }
                                    } catch (JSONException excp){
                                        Log.d(TAG, excp.getMessage());
                                    }
                                    return null;
                                }
                            }.execute(getString(R.string.url_request_userid), generateUserId());



                            //PhoneNumberVerifierService.startActionRequestId(CreatePasswordActivity.this, preferences.getUserId());
                            //Intent userBoardIntent = new Intent(CreatePasswordActivity.this, UserBoard.class);
                            //startActivity(userBoardIntent);
                            //finish();
                        }
                    } catch (NumberFormatException excp){
                        viewBadPassword.setText(getResources().getString(R.string.number_pass_exception));
                        viewBadPassword.setVisibility(View.VISIBLE);
                    }

                }
            }
        });

    }

    public String generateUserId(){

        DateFormat dateFormat = new SimpleDateFormat("yyyyMM", Locale.FRANCE);
        String accountString = "0001";
        String dateInsertion = dateFormat.format(new Date());
        preferences = new KatikaSharedPreferences(this);
        accountString += preferences.getUserGender();
        accountString += dateInsertion;
        accountString += "000";

        return accountString;
    }

    /*
   public String getRemoteUserId(String url, String userIdInit){

        String userId="";

        AsyncTask<String, Void, String> myTask = new AsyncTask<String, Void, String>(){
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ConstraintLayout mConstraintLayout = findViewById(R.id.loading_layout_create_password);
                mConstraintLayout.setVisibility(View.VISIBLE);
            }

            @Override
            protected void onPostExecute(String aString) {
                super.onPostExecute(aString);
                ConstraintLayout mConstraintLayout = findViewById(R.id.loading_layout_create_password);
                mConstraintLayout.setVisibility(View.INVISIBLE);
                //preferences.setAccountConnected(true);
                //Intent userIntent = new Intent(CreatePasswordActivity.this, UserBoard.class);
                //startActivity(userIntent);
                //finish();
            }

            @Override
            protected String doInBackground(String... strings) {
                String url = strings[0];
                String userIdInitString = strings[1];
                JSONObject args = new JSONObject();
                RequestQueue requestQueue = Volley.newRequestQueue(CreatePasswordActivity.this);
                JSONObject response = new JSONObject();
                try {
                    args.put("initUserId", userIdInitString);

                    final RequestFuture<JSONObject> future = RequestFuture.newFuture();
                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                            url, args, future, future);
                    requestQueue.add(request);
                    try {
                        response = future.get(10, TimeUnit.SECONDS);
                        return response.getString("userId");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (TimeoutException e) {
                        Log.e(TAG, "TIME OUT"+e.getMessage());
                    }
                } catch (JSONException excp){
                    Log.d(TAG, excp.getMessage());
                }
                return null;
            }
        }.execute(url, userIdInit);


        try {
            userId = myTask.get();
        } catch (InterruptedException e) {
            Log.d("JTO",e.getMessage());
        } catch (ExecutionException e) {
            Log.d("JTO",e.getMessage());
        }

        return userId;
    }
*/
}
