package com.katikatech.james.pilot_project.UI;

import android.content.Intent;
import android.content.pm.ActivityInfo;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.katikatech.james.pilot_project.SharedPreferences.CurrentOperationSharedPreferences;
import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.services.NotificationService;
import com.katikatech.james.pilot_project.R;
import com.katikatech.james.pilot_project.utils.StatusBarManager;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class CutOperationActivity extends AppCompatActivity {

    private KatikaSharedPreferences preferences;
    Toolbar toolbarCutOperation;
    private final static String TAG = "CutOperations";
    private Button addElement;
    private Button subElement;
    private Float totalElements;
    private Float amount;
    EditText editTotalElements;
    EditText totalAmount;
    Button submitCutButton;
    CurrentOperationSharedPreferences operationSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cut_operation);
        operationSharedPreferences = new CurrentOperationSharedPreferences(this);

        //Retrieve the amount from previous activity
        try{
            amount = getIntent().getExtras().getFloat("amount");
        } catch (NullPointerException excp){
            Log.d(TAG, "Unable to retrieve the amount");
            amount = 0.0f;
        }

        //Retrieve the number of items from previous activity
        try{
            totalElements = getIntent().getExtras().getFloat("number_items");
        } catch (NullPointerException excp){
            Log.d(TAG, "Unable to retrieve number of items");
            totalElements = 1.0f;
        }

        //Orientation portrait programmatically
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        /**
         * Check connection
         */
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(true);
        if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(CutOperationActivity.this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        }

        //Toolbar
        toolbarCutOperation = (Toolbar) findViewById(R.id.toolbar_cut_operation);
        setSupportActionBar(toolbarCutOperation);

        new StatusBarManager().translucideCustomStatusBar(CutOperationActivity.this,
                toolbarCutOperation, R.color.colorWhite);

        //Set back button in toolbar
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        addElement = (Button) findViewById(R.id.button_plus);
        subElement = (Button) findViewById(R.id.button_minus);
        editTotalElements = (EditText) findViewById(R.id.edit_quantity);
        totalAmount = (EditText) findViewById(R.id.current_operation_amount);
        submitCutButton = (Button) findViewById(R.id.submit_cut_operation);

        //fill the Text zones
        totalAmount.setText(""+amount);
        editTotalElements.setText(""+totalElements);


        submitCutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DecimalFormat df = new DecimalFormat("#.##");
                df.setRoundingMode(RoundingMode.CEILING);
                DecimalFormat df_items = new DecimalFormat("#.#");
                df_items.setRoundingMode(RoundingMode.CEILING);
                String newAmount = totalAmount.getText().toString();
                String newTotalItems = editTotalElements.getText().toString();
                if (newAmount.trim().isEmpty() || newTotalItems.trim().isEmpty()) {
                    /* Données vides */
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.empty_text_zone), Toast.LENGTH_SHORT).show();
                    return;
                }
                float modifiedAmount = Float.parseFloat(df.format(Float.parseFloat(newAmount)).replace(',','.'));
                float modifiedTotalItems = Float.parseFloat(df_items.format(Float.parseFloat(newTotalItems)).replace(',','.'));
                if(modifiedTotalItems == 0.0 || modifiedAmount == 0.0 ) {
                    /* Données nulles */
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.null_values_exception), Toast.LENGTH_SHORT).show();
                } else if (modifiedAmount > KatikaSharedPreferences.KATIKA_MAX ||
                        modifiedAmount < KatikaSharedPreferences.KATIKA_MIN){
                    /* Données hors des limites */
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.crazy_rich_exception),
                            Toast.LENGTH_SHORT).show();
                } else if (modifiedTotalItems > KatikaSharedPreferences.KATIKA_MAX_ITEMS){
                    /* Données hors des limites */
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.crazy_rich_exception),
                            Toast.LENGTH_SHORT).show();
                } else {
                    operationSharedPreferences.setOperationAmount(modifiedAmount);
                    operationSharedPreferences.setOperationTotalElement(modifiedTotalItems);

                    //operationSharedPreferences.setOperationAmount(Float.parseFloat(newAmount));
                    //operationSharedPreferences.setOperationTotalElement(Float.parseFloat(newTotalItems));
                    preferences.setAccountConnected(true);
                    Intent operationIntent = new Intent(CutOperationActivity.this, DetailedOperationActivity.class);
                    startActivity(operationIntent);
                    finish();
                }
            }
        });



    }

    //we press the back button
    public void onBackPressed() {
        preferences.setAccountConnected(true);
        Intent parentIntent = new Intent(CutOperationActivity.this, DetailedOperationActivity.class);
        startActivity(parentIntent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            preferences.setAccountConnected(true);
            Intent detailsIntent = new Intent(CutOperationActivity.this, DetailedOperationActivity.class);
            startActivity(detailsIntent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        preferences = new KatikaSharedPreferences(this);
        NotificationService.stopActionTimeOut(CutOperationActivity.this);

        /**
         * Check connection
         */
        if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(CutOperationActivity.this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        } else {
            preferences.setAccountConnected(false);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(false);
        //preferences.setAccountConnected(true);
        NotificationService.startActionTimeOut(CutOperationActivity.this);
        Log.d(TAG, "paused, status connected: "+ preferences.getAccountConnected());
    }

    @Override
    protected void onStop() {
        super.onStop();
        preferences = new KatikaSharedPreferences(this);
        if(!preferences.getActivityOn()) {
            preferences.setAccountConnected(false);
            Log.d(TAG, "stopped, status connected: " + preferences.getAccountConnected());
        } else {
            preferences.setAccountConnected(true);
            Log.d(TAG, "connected, stopped, status connected: " + preferences.getAccountConnected());
        }
    }

    public void buttonAddElement(View view){
        totalElements = Float.parseFloat(editTotalElements.getText().toString()) + 1;
        editTotalElements.setText(""+totalElements);
    }

    public void buttonSubElement(View view){
        if (totalElements > 1){
            totalElements = Float.parseFloat(editTotalElements.getText().toString()) - 1;
            editTotalElements.setText(""+totalElements);
        }
    }
}
