package com.katikatech.james.pilot_project.UI;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.katikatech.james.pilot_project.SharedPreferences.CurrentOperationSharedPreferences;
import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.services.NotificationService;
import com.katikatech.james.pilot_project.Operation;
import com.katikatech.james.pilot_project.OperationRepository;
import com.katikatech.james.pilot_project.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DetailedOperationActivity extends AppCompatActivity {

    private KatikaSharedPreferences preferences;
    private CurrentOperationSharedPreferences operationSharedPreferences;
    Toolbar toolbarDetailedOperation;
    TextView DatePrinter;
    TextView AmountPrinter;
    TextView categoryPrinter;
    TextView cutPrinter;
    EditText descriptionEdit;
    RelativeLayout categoryLayout;
    RelativeLayout cutLayout;
    RelativeLayout tagLayout;
    RelativeLayout attachLayout;
    Button updateButton;
    ImageView categoryImage;
    Operation myCurrentOperation;
    boolean isFetched;
    final static String TAG = "DetailsOperations";
    DatabaseReference operationDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_operation);

        operationSharedPreferences = new CurrentOperationSharedPreferences(this);

        // FIREBASE INITIALISATION
        operationDatabase = FirebaseDatabase.getInstance().getReference("Operations");

        String description = "";
        String amount  = ""+0;
        String insertion = "";
        String categorie;
        String remoteKey = "";
        isFetched = false;

        try {
            description = getIntent().getExtras().getString("description");
            Log.d(TAG, " description "+description);
            amount = getIntent().getExtras().get("amount").toString();
            Log.d(TAG, " amount "+ amount);
            insertion = getIntent().getExtras().getString("insertion");
            Log.d(TAG, " insertion "+ insertion);
            remoteKey = getIntent().getExtras().getString("remote_key");
            Log.d(TAG, " remote_key "+ insertion);

        } catch (NullPointerException excp){
            Log.d(TAG, "Unable to retrieve data");

            //If I don't have the three elements, it means I already fetched the operation and fill sharedPreferences
            isFetched = true;
        }

        try{
            categorie = getIntent().getExtras().getString("categorieTitle");
        } catch (NullPointerException excp){
            Log.d(TAG, "Unable to retrieve catgorie data");
            categorie = "catégorie";
        }

        if (!isFetched) {
            //myCurrentOperation = new OperationRepository(this.getApplication()).myRequestedOperation(description, amount, insertion);
            myCurrentOperation = new OperationRepository(this.getApplication()).myRequestedOperation(remoteKey);
            //jsonify the operation object
            operationSharedPreferences.setOperationComplete(myCurrentOperation);
            //retrieve custom attributes
            //operationSharedPreferences.setOperationAmount(0.f);
            Log.d(TAG, " fetched amount "+ myCurrentOperation.getAmount());
            operationSharedPreferences.setOperationAmount(myCurrentOperation.getAmount());
            operationSharedPreferences.setOperationDescription(myCurrentOperation.getDescription());
            operationSharedPreferences.setOperationDateInsertion(myCurrentOperation.getInsertedAt());
            operationSharedPreferences.setOperationCategory(myCurrentOperation.getIdOperationCategory());
            operationSharedPreferences.setOperationTotalElement(myCurrentOperation.getNumberItems());
            Log.d(TAG, "firebase  key: "+ myCurrentOperation.getRemoteKey());
        }

        //Orientation portrait programmatically
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        /**
         * Check connection
         */
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(true);
        if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(DetailedOperationActivity.this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        }

        //Toolbar
        toolbarDetailedOperation = (Toolbar) findViewById(R.id.toolbar_detailed_operation);
        setSupportActionBar(toolbarDetailedOperation);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color
            window.setStatusBarColor(ContextCompat.getColor(getApplication(),R.color.colorWhite));

            toolbarDetailedOperation.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //define the textViews and EditText
        AmountPrinter = (TextView) findViewById(R.id.text_print_amount);
        DatePrinter = (TextView) findViewById(R.id.text_print_date);
        descriptionEdit = (EditText) findViewById(R.id.DescriptionText);
        categoryPrinter = (TextView) findViewById(R.id.categoryTitle);
        cutPrinter = (TextView) findViewById(R.id.cutTitle);
        categoryImage = (ImageView) findViewById(R.id.categoryImage);

        //Print the cut operation entry
        if(operationSharedPreferences.getOperationTotalElement()!=1){
            String numberItemsString = " ("+operationSharedPreferences.getOperationTotalElement()+
                    " " + getResources().getString(R.string.items_string)+ ")";
            cutPrinter.setText(getString(R.string.cut_operation) + numberItemsString);
        }

        categoryLayout = (RelativeLayout) findViewById(R.id.category_selection_layout);
        cutLayout = (RelativeLayout) findViewById(R.id.cut_operation_layout);
        tagLayout = (RelativeLayout) findViewById(R.id.tag_operation_layout);
        attachLayout = (RelativeLayout) findViewById(R.id.attach_file_layout);
        updateButton = (Button) findViewById(R.id.SubmitUpdateButton);

        categoryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferences.setAccountConnected(true);
                Intent categoryIntent = new Intent(DetailedOperationActivity.this, CategoryActivity.class);
                startActivity(categoryIntent);
                finish();
            }
        });

        cutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferences.setAccountConnected(true);
                Intent cutOperationIntent = new Intent(DetailedOperationActivity.this, CutOperationActivity.class);
                cutOperationIntent.putExtra("amount", operationSharedPreferences.getOperationAmount());
                cutOperationIntent.putExtra("number_items", operationSharedPreferences.getOperationTotalElement());
                startActivity(cutOperationIntent);
                finish();
            }
        });

        tagLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferences.setAccountConnected(true);
                Intent tagOperationIntent = new Intent(DetailedOperationActivity.this, TagOperationActivity.class);
                startActivity(tagOperationIntent);
                finish();
            }
        });

        attachLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferences.setAccountConnected(true);
                Intent attachOperationIntent = new Intent(DetailedOperationActivity.this, AttachOperationDocumentActivity.class);
                startActivity(attachOperationIntent);
                finish();
            }
        });

        //AmountPrinter.setText(getString(R.string.text_details_amount, amount));
        AmountPrinter.setText(operationSharedPreferences.getOperationAmount()+" "+preferences.getUserCurrencySymbol());

        String operationMovement;
        if(operationSharedPreferences.getOperationAmount() < 0){
            operationMovement = getResources().getString(R.string.cash_out_date_string) + " ";
        } else {
            operationMovement = getResources().getString(R.string.cash_in_date_string) + " ";
        }
        /* Set Date in human readable format */
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.FRANCE);
        DateFormat formattedDate =
                new SimpleDateFormat("dd MMMM yyyy", Locale.FRANCE);
        try {
            Date currentDate = dateFormat.parse(operationSharedPreferences.getOperationDateInsertion());
            DatePrinter.setText(operationMovement + getString(R.string.text_details_date, formattedDate.format(currentDate)));
        } catch (ParseException e) {
            Log.d(TAG, e.getMessage());
            DatePrinter.setText(getString(R.string.text_details_date, operationSharedPreferences.getOperationDateInsertion()));
        }


        descriptionEdit.setText(operationSharedPreferences.getOperationDescription());
        categoryPrinter.setText(KatikaSharedPreferences.KatikaOPerationCategories.get(operationSharedPreferences.getOperationCategory() - 1));

        int imageResource;

        switch (operationSharedPreferences.getOperationCategory()){
            case 1:
                imageResource = R.drawable.revenus_divers;
                break;
            case 2:
                imageResource = R.drawable.depenses_divers;
                break;
            case 3:
                imageResource = R.drawable.alimentation;
                break;
            case 4:
                imageResource = R.drawable.transport;
                break;
            case 5:
                imageResource = R.drawable.abonnement;
                break;
            case 6:
                imageResource = R.drawable.loyer;
                break;
            case 7:
                imageResource = R.drawable.allocations;
                break;
            case 8:
                imageResource = R.drawable.impots_taxes;
                break;
            default:
                imageResource = R.drawable.dollar_sign;
                break;
        }

        categoryImage.setImageResource(imageResource);



        //The button of validation is pressed
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OperationRepository operationRepository =
                        new OperationRepository(getApplication());

                if (!descriptionEdit.getText().toString().trim().isEmpty()) {

                    /*operationSharedPreferences.setOperationDescription(descriptionEdit.getText().toString());
                    operationSharedPreferences.setOperationAmount(operationSharedPreferences.getOperationAmount());
                    operationSharedPreferences.setOperationCategory(operationSharedPreferences.getOperationCategory());
                    operationSharedPreferences.setOperationTotalElement(operationSharedPreferences.getOperationTotalElement());*/

                    //assign the return of the getOperationComplete method into a Operation instance
                    myCurrentOperation = operationSharedPreferences.getOperationComplete();

                    operationSharedPreferences.setOperationDescription(descriptionEdit.getText().toString());

                    Log.d(TAG, " " + operationSharedPreferences.getOperationDescription() + " "
                            + operationSharedPreferences.getOperationAmount() + " " +
                            operationSharedPreferences.getOperationCategory() + " " +
                            operationSharedPreferences.getOperationTotalElement());

                    operationRepository.updateOperation(DetailedOperationActivity.this,
                            myCurrentOperation,
                            operationSharedPreferences.getOperationDescription(),
                            operationSharedPreferences.getOperationAmount(),
                            operationSharedPreferences.getOperationCategory(),
                            operationSharedPreferences.getOperationTotalElement(),
                            true);

                    String remoteKey = myCurrentOperation.getRemoteKey();
                    //myCurrentOperation.setRemoteKey("");

                    // Update data in remote database
                    if(remoteKey.length() != 0) {
                        operationDatabase.child(remoteKey).setValue(myCurrentOperation)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.d(TAG, "remote update success");
                                    }
                                });
                    } else {
                        remoteKey = preferences.getUserId()+"__"+ myCurrentOperation.getId();
                        operationDatabase.child(remoteKey).setValue(myCurrentOperation)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.d(TAG, "remote update success");
                                    }
                                });
                        operationRepository.updateOperation(DetailedOperationActivity.this,
                                myCurrentOperation,
                                operationSharedPreferences.getOperationDescription(),
                                operationSharedPreferences.getOperationAmount(),
                                operationSharedPreferences.getOperationCategory(),
                                operationSharedPreferences.getOperationTotalElement(),
                                false);
                    }


                    preferences.setAccountConnected(true);
                    Intent parentIntent = new Intent(DetailedOperationActivity.this, detailedTransactions.class);
                    startActivity(parentIntent);
                    finish();
                } else {
                    Toast.makeText(DetailedOperationActivity.this,
                            getResources().getString(R.string.description_operation_exception), Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    //we press the back button
    public void onBackPressed() {
        preferences.setAccountConnected(true);
        Intent parentIntent = new Intent(DetailedOperationActivity.this, detailedTransactions.class);
        startActivity(parentIntent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            preferences.setAccountConnected(true);
            Intent detailsIntent = new Intent(DetailedOperationActivity.this, detailedTransactions.class);
            startActivity(detailsIntent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        preferences = new KatikaSharedPreferences(this);
        NotificationService.stopActionTimeOut(DetailedOperationActivity.this);

        /**
         * Check connection
         */
        if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(DetailedOperationActivity.this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        } else {
            preferences.setAccountConnected(false);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(false);
        //preferences.setAccountConnected(true);
        NotificationService.startActionTimeOut(DetailedOperationActivity.this);
        Log.d(TAG, "paused, status connected: "+ preferences.getAccountConnected());
    }

    @Override
    protected void onStop() {
        super.onStop();
        preferences = new KatikaSharedPreferences(this);
        if(!preferences.getActivityOn()) {
            preferences.setAccountConnected(false);
            Log.d(TAG, "stopped, status connected: " + preferences.getAccountConnected());
        } else {
            preferences.setAccountConnected(true);
            Log.d(TAG, "connected, stopped, status connected: " + preferences.getAccountConnected());
        }
    }
}
