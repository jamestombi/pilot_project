package com.katikatech.james.pilot_project.UI;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Build;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.services.NotificationService;
import com.katikatech.james.pilot_project.R;

public class FilterPage extends AppCompatActivity {

    Button submitButton;
    ProgressDialog dialog;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    Toolbar toolbarFilterPage;
    RadioGroup filter_option;
    int filterItemSelected;
    private KatikaSharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filterpage);
        filterItemSelected = R.id.id_no_filter;
        filter_option = (RadioGroup) findViewById(R.id.toggle_filter);
        submitButton = (Button) findViewById(R.id.SubmitButton_filter);
        //Orientation portrait programmatically
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //Toolbar
        toolbarFilterPage = (Toolbar) findViewById(R.id.toolbar_filterpage);
        setSupportActionBar(toolbarFilterPage);

        /**
         * Check connection
         */
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(true);
        if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(FilterPage.this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color
            window.setStatusBarColor(ContextCompat.getColor(getApplication(),R.color.colorWhite));

            toolbarFilterPage.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        //New selected RadioButton listener
        filter_option.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                Integer [] listOfRadioButton = new Integer[]{R.id.id_no_filter,
                        R.id.id_this_week_filter,
                        R.id.id_this_month_filter,
                        R.id.id_last_month_filter,
                        R.id.id_90_days_filter,
                        R.id.id_current_year_filter};

                RadioButton selected_button = (RadioButton) findViewById(i);
                selected_button.setTypeface(null, Typeface.BOLD);
                filterItemSelected = i;

                for (int item = 0; item < listOfRadioButton.length; item++){
                    if(listOfRadioButton[item]!=i){
                        RadioButton unselected_button = (RadioButton)findViewById(listOfRadioButton[item]);
                        unselected_button.setTypeface(null, Typeface.NORMAL);
                    }
                }
            }

        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent filteredIntent = new Intent(FilterPage.this, detailedTransactions.class);
                filteredIntent.putExtra("filterItemSelected", filterItemSelected);
                startActivity(filteredIntent);
                finish();
            }
        });

    }

    public void onBackPressed() {


        //Intent respo_serv = new Intent(response_server.this, InsertOperationActivity.class);
        AlertDialog.Builder builder = new AlertDialog.Builder(FilterPage.this);
        builder.setMessage("Annuler le filtre?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent respo_serv = new Intent(FilterPage.this, detailedTransactions.class);
                        respo_serv.putExtra("age_gamer", 1);
                        respo_serv.putExtra("gamer", "Not");
                        startActivity(respo_serv);
                        finish();
                        dialog.dismiss();
                        Log.d("JTO", "sortie");
                        //super.onBackPressed();
                    }
                }).setNegativeButton("Annuler", null);

        AlertDialog alert = builder.create();
        alert.show();

        //startActivity(respo_serv);

        //finish();
    }

    //Function used to back in previous activity via the app bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            Intent transactionIntent = new Intent(FilterPage.this, detailedTransactions.class);
            transactionIntent.putExtra("age_gamer", 1);
            transactionIntent.putExtra("gamer", "Not");
            startActivity(transactionIntent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        preferences = new KatikaSharedPreferences(this);

        NotificationService.stopActionTimeOut(FilterPage.this);
        /**
         * Check connection
         */
        if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(FilterPage.this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        } else {
            preferences.setAccountConnected(false);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(false);
        //preferences.setAccountConnected(true);
        NotificationService.startActionTimeOut(FilterPage.this);
        Log.d("FILTER PAUSED", "status connected: "+ preferences.getAccountConnected());
    }

    @Override
    protected void onStop() {
        super.onStop();
        preferences = new KatikaSharedPreferences(this);
        if(!preferences.getActivityOn()) {
            preferences.setAccountConnected(false);
            Log.d("FILTER STOPPED", "status connected: " + preferences.getAccountConnected());
        } else {
            preferences.setAccountConnected(true);
            Log.d("FIFILTER STOPPED", "status connected: " + preferences.getAccountConnected());
        }
    }

}
