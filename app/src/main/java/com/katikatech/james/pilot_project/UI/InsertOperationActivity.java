package com.katikatech.james.pilot_project.UI;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.services.NotificationService;
import com.katikatech.james.pilot_project.Operation;
import com.katikatech.james.pilot_project.OperationRepository;
import com.katikatech.james.pilot_project.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class InsertOperationActivity extends AppCompatActivity {

    public static final String LAST_UPDATE = "last update";
    private final String CHANNEL_ID = "Platform";
    private final int NOTIFICATION_ID = 001;
    private final static String TAG = "New transaction";
    private KatikaSharedPreferences preferences;
    Button submitButton;
    EditText myDescription;
    ProgressDialog dialog;
    EditText myAmount;
    EditText myDate;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    Toolbar toolbarFirstPage;
    RadioGroup moneyTransactionSwitch;
    RadioButton cash_in;
    RadioButton cash_out;
    int isFromUserBoard;

    DatabaseReference operationDatabase;

    //SharedPreferences mSharedPreferences;

    /*Create a channel for notifications in ANDROID version above 8.0.0 */
    private void createNotificationChannel() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Project Base";
            String description = "A test of notifications";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationChannel.setDescription(description);
            notificationChannel.enableLights(true);
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            notificationManager.createNotificationChannel(notificationChannel);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firstpage);

        //Orientation portrait programmatically
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        /**
         * Check connection
         */
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(true);
        if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(InsertOperationActivity.this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        }

        //retrieve the extra
        try{
            isFromUserBoard = getIntent().getExtras().getInt("userBoardOrigin");
            Log.d(TAG, "isFromUserBoard " + isFromUserBoard);
        } catch (NullPointerException excp){
            isFromUserBoard = 0;
            Log.d(TAG, "isFromUserBoard FAILED");
        }

        // FIREBASE INITIALISATION
        operationDatabase = FirebaseDatabase.getInstance().getReference("Operations");


        submitButton = (Button) findViewById(R.id.SubmitButton);
        myDescription = (EditText) findViewById(R.id.NameText);
        myAmount = (EditText) findViewById(R.id.AmountText);
        myDate = (EditText) findViewById(R.id.DateText);
        moneyTransactionSwitch = (RadioGroup) findViewById(R.id.toggle);

        //Toolbar
        toolbarFirstPage = (Toolbar) findViewById(R.id.toolbar_firstpage);
        setSupportActionBar(toolbarFirstPage);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color
            window.setStatusBarColor(ContextCompat.getColor(getApplication(),R.color.colorWhite));

            toolbarFirstPage.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //spendingSwitch = (Switch) findViewById(R.id.spendSwitch);
        cash_in = (RadioButton) findViewById(R.id.cash_in);
        cash_out = (RadioButton) findViewById(R.id.cash_out);


        //Progress Dialog
        dialog = new ProgressDialog(this);
        //DateDialog for date picking
        myDate.setOnFocusChangeListener(dateDialog);
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                int mymonth = month+1;
                String myDayOfMonth = ""+dayOfMonth;
                if (dayOfMonth < 10) {
                    myDayOfMonth = "0" + dayOfMonth;
                }

                String actualDate = "";
                if (mymonth < 10) {
                     actualDate = "" + year + "/" + 0+mymonth + "/" + myDayOfMonth;
                } else {
                    actualDate = "" + year + "/" + mymonth + "/" + myDayOfMonth;
                }

                myDate.setText(actualDate);
            }
        };
        submitButton.setOnClickListener(SubmissionListener);

        moneyTransactionSwitch.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(i == R.id.cash_in){
                    toolbarFirstPage.setTitle(R.string.cash_in_title);
                } else if (i == R.id.cash_out){
                    toolbarFirstPage.setTitle(R.string.cash_out_title);
                }
            }
        });
    }

    public void onBackPressed() {
        
        AlertDialog.Builder builder = new AlertDialog.Builder(InsertOperationActivity.this);
        builder.setMessage(getResources().getText(R.string.cancel_insertion_question))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(isFromUserBoard == 0) {
                            Intent detailsIntent = new Intent(InsertOperationActivity.this, detailedTransactions.class);
                            startActivity(detailsIntent);
                            finish();
                        } else if (isFromUserBoard == 1){
                            Intent userBoardIntent = new Intent(InsertOperationActivity.this, UserBoard.class);
                            startActivity(userBoardIntent);
                            finish();
                        }
                        dialog.dismiss();
                        Log.d("JTO", "sortie");
                        //super.onBackPressed();
                    }
                }).setNegativeButton(getResources().getString(R.string.cancel), null);

        AlertDialog alert = builder.create();
        alert.show();
        
    }

    //Listener in case of taking focus in the date entry
    EditText.OnFocusChangeListener dateDialog = new EditText.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {

            if(hasFocus) {
                //Create an instance of calendar
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                //datePicker dialog box with date set listener in date built
                DatePickerDialog dialog = new DatePickerDialog(InsertOperationActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        }

    };



    Button.OnClickListener  SubmissionListener = new Button.OnClickListener() {


            @Override
            public void onClick(View view) {

                /*Access Database*/
                boolean changeActivity;
                float newAmount = -1.0f;
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.FRANCE);
                DateFormat humanDateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.FRANCE);

                try {
                    OperationRepository operationRepository =
                            new OperationRepository(getApplication());

                    newAmount = Float.parseFloat(myAmount.getText().toString());

                    DecimalFormat df = new DecimalFormat("#.##");
                    df.setRoundingMode(RoundingMode.CEILING);
                    String newAmountStr = myAmount.getText().toString();
                    newAmount = Float.parseFloat(df.format(Float.parseFloat(newAmountStr)).replace(',','.'));

                    Log.d(TAG, "onClick: newAmount "+newAmount);

                    if(newAmount > KatikaSharedPreferences.KATIKA_MAX){
                        throw new RuntimeException(getResources().getString(R.string.crazy_rich_exception));
                    }

                    if (!cash_out.isChecked() && !cash_in.isChecked()){
                        throw new RuntimeException(getResources().getString(R.string.set_operation_move));
                    }

                    if (cash_out.isChecked()){
                        newAmount = -newAmount;
                    }

                    String newDescription = myDescription.getText().toString();
                    if (newDescription.trim().isEmpty()) {
                        throw new RuntimeException(getResources().getString(R.string.description_operation_exception));
                    }

                    String dateInsertion = myDate.getText().toString();

                    if (dateInsertion.isEmpty()) {
                         dateInsertion = dateFormat.format(new Date());
                    } else {
                        try {
                            Date enteredDate = dateFormat.parse(dateInsertion);
                            Date today = dateFormat.parse(dateFormat.format(new Date()));
                            long diff = TimeUnit.MILLISECONDS.toDays(today.getTime() - enteredDate.getTime());
                            if (diff > 90 || diff < 0) {
                                throw new RuntimeException(getResources().getString(R.string.ninety_days_exception));
                            }
                        } catch (ParseException e) {
                            throw new RuntimeException(getResources().getString(R.string.bad_date_exception));
                        }
                    }

                    /**
                     * FIREBASE DISTANT DATABASE AND SQLITE LOCAL DATABASE INSERTION
                     */

                    //define the Key of the operation
                    String databaseId = preferences.getUserId()+"__"+operationDatabase.push().getKey();

                    /*
                    DateFormat dateHashFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.FRANCE);
                    String longDateString = dateHashFormat.format(new Date());

                    MessageDigest digest = null;
                    try {
                        digest = MessageDigest.getInstance("SHA-256");
                        byte[] encodedhash = digest.digest(
                                originalString.getBytes(StandardCharsets.UTF_8));
                    } catch (NoSuchAlgorithmException e) {
                        Log.d(TAG, "onClick: "+e.getMessage());
                    }
                    String hashDateString =  digest.sha256(longDateString);

                    */

                    //Insertion of a new operation in the local database
                    operationRepository.insertOperation(InsertOperationActivity.this, newDescription, newAmount, dateInsertion, databaseId);


                    Operation newOperation = new Operation();
                    newOperation.setDescription(newDescription);
                    newOperation.setAmount(newAmount);
                    newOperation.setInsertedAt(dateInsertion);
                    newOperation.setModifiedAt(dateInsertion);
                    newOperation.setIdAccountUser("001");
                    newOperation.setIdCurrency(preferences.getUserCurrencyId());
                    if(newAmount >= 0) {
                        newOperation.setIdOperationCategory(1);
                    } else {
                        newOperation.setIdOperationCategory(2);
                    }
                    newOperation.setIdUser(preferences.getUserId());
                    newOperation.setOperationVisible(true);
                    newOperation.setOperationSynchronized(true);
                    newOperation.setNumberItems(1);
                    newOperation.setRemoteKey(databaseId);

                    DateFormat dateHashFormat = new SimpleDateFormat("yy_MM_dd-HH_mm_ss", Locale.FRANCE);
                    String longDateString = dateHashFormat.format(new Date());


                    if(!databaseId.isEmpty()) {
                        operationDatabase.child(databaseId).setValue(newOperation)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.d(TAG, "remote insertion success");
                                    }
                                });
                    } else {
                        String newKey = preferences.getUserId() + "__" + longDateString;
                        newOperation.setRemoteKey(newKey);

                        operationDatabase.child(newKey).setValue(newOperation)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.d(TAG, "remote insertion success with new remote key");
                                    }
                                });
                    }

                    changeActivity = true;

                } catch (RuntimeException e ) {

                    Context context = getApplicationContext();
                    CharSequence text;
                    if (newAmount != -1) {
                        text = e.getMessage();
                    } else {
                        text = getResources().getString(R.string.no_amount_exeception);
                    }
                    int duration = Toast.LENGTH_LONG;
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                    changeActivity = false;
                }

                if (changeActivity) {

                    preferences.setLastUpdate(humanDateFormat.format(new Date()));

                    preferences.setAccountConnected(true);

                    /*New Intent with responses from JSON request */
                    Intent detailsIntent = new Intent(InsertOperationActivity.this, GoodInsertionActivity.class);

                    /*start new activity*/
                    startActivity(detailsIntent);

                    finish();

                }
                //A decommenter pour acces serveur
                //sendDataToServer();


            }
        };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            preferences.setAccountConnected(true);
            if(isFromUserBoard == 0) {
                Intent detailsIntent = new Intent(InsertOperationActivity.this, detailedTransactions.class);
                startActivity(detailsIntent);
                finish();
            } else if (isFromUserBoard == 1){
                Intent userBoardIntent = new Intent(InsertOperationActivity.this, UserBoard.class);
                startActivity(userBoardIntent);
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    /*  ***************PAS DE CODE MORT MAIS CODE ACCES SERVER VIA JSON************************
    private String formatDataAsJSON(){
        final JSONObject root = new JSONObject();
        try{
            Editable newDescription = myDescription.getText();
            Editable newAmount = myAmount.getText();
            Boolean spending = spendingSwitch.isChecked();
            root.put("description", newDescription.toString().trim());
            root.put("amount", Float.parseFloat(newAmount.toString()));
            root.put("spending", spending);
            Log.d("JTO--->", root.toString());
            return root.toString();
        } catch (JSONException e){

            Log.d("JTO", e.toString());
        }

        return null;
    }
    */


    /*
    private void sendDataToServer(){

        final String json = formatDataAsJSON();


        new AsyncTask<Void, Void, String>(){

            @Override
            protected void onPostExecute(String s) {
                dialog.dismiss();
                TextView myTextView = (TextView) findViewById(R.id.ResponseView);
                JSONObject myjsonResponse = null;
                //The request has failed, no reply
                if (s == null) {

                    float compute_diff = Float.parseFloat(myAmount.getText().toString()) - 500;
                    myTextView.setTextColor(Color.rgb(200,50,50)); //RED
                    myTextView.setText("error "+ compute_diff);

                    //Create and show a toast for No result
                    Context context = getApplicationContext();
                    CharSequence text = "Server unavailable";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                    return;
                }
                //Reply sent, Parse into Json object the reply
                try {
                    myjsonResponse = new JSONObject(s);
                } catch (JSONException e) {
                    Log.d("JTO ===>", e.toString());
                }
                try {
                    //Put Extra values in the new intent
                    Log.d("Pretty value : ", String.valueOf(myjsonResponse.getInt("pretty")));
                    Log.d("Wow value :", myjsonResponse.getString("wow"));

                    //finish();

                } catch (JSONException e) {
                    Log.d("JTO ===0", e.toString());
                }
            }

            @Override
            protected String doInBackground(Void... voids) {
                return getServerResponse(json);
            }

            @Override
            protected void onPreExecute(){
                // a progress dialog
                dialog = ProgressDialog.show(InsertOperationActivity.this, "",
                        "Loading. Please wait...", true);
                return;
            }


        }.execute();



    }
    */

    /*
    private String getServerResponse(String json) {

        URL url = null;
        HttpURLConnection client = null;
        String data = "";

        //OPEN CONNECTION
        try {
            url = new URL("http://192.168.1.42:3000/sms");
        } catch (MalformedURLException e) {
            Log.d("JTO", e.toString());
        }

        try {
            client = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            Log.d("JTO", e.toString());
        }

        //DEFINE REQUEST METHOD
        try {
            client.setRequestMethod("POST");
        } catch (ProtocolException e) {
            Log.d("JTO POST Exception: ", e.toString());
        }

        client.setRequestProperty("content-type", "application/json");

        //SEND REQUEST

        //client.setRequestProperty("Key","Value");
        client.setDoOutput(true);

        //OutputStream outputPost = null;
        DataOutputStream outputPost = null;

        try {
            //outputPost = new BufferedOutputStream(client.getOutputStream());
            outputPost = new DataOutputStream(client.getOutputStream());
            outputPost.writeBytes(json);
            Log.d("JTO json write: ", json);
            outputPost.flush();
            outputPost.close();
        } catch (IOException e)  {
            Log.d("JTO json exc: ", e.toString());
        }


        //RESPONSE FROM SERVER

        InputStream in = null;
        try {
            in = client.getInputStream();

            InputStreamReader inputStreamReader = new InputStreamReader(in);

            int inputStreamData = inputStreamReader.read();
            while (inputStreamData != -1) {
                char current = (char) inputStreamData;
                inputStreamData = inputStreamReader.read();
                data += current;
            }

            return data;

        } catch (IOException e) {
            Log.d("JTO", e.toString());
        }

        if(client != null) // Make sure the connection is not null.
            client.disconnect();

        return null;
    }

    */

    @Override
    protected void onResume() {
        super.onResume();
        preferences = new KatikaSharedPreferences(this);
        NotificationService.stopActionTimeOut(InsertOperationActivity.this);

        /**
         * Check connection
         */
        if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(InsertOperationActivity.this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        } else {
            preferences.setAccountConnected(false);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(false);
        //preferences.setAccountConnected(true);
        NotificationService.startActionTimeOut(InsertOperationActivity.this);
        Log.d(TAG, "paused, status connected: "+ preferences.getAccountConnected());
    }

    @Override
    protected void onStop() {
        super.onStop();
        preferences = new KatikaSharedPreferences(this);
        if(!preferences.getActivityOn()) {
            preferences.setAccountConnected(false);
            Log.d(TAG, "stopped false, status connected: " + preferences.getAccountConnected());
        } else {
            preferences.setAccountConnected(true);
            Log.d(TAG, "stopped true, status connected: " + preferences.getAccountConnected());
        }
    }

}
