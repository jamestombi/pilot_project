package com.katikatech.james.pilot_project.UI;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.katikatech.james.pilot_project.R;
import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.services.PhoneNumberVerifierService;
import com.katikatech.james.pilot_project.utils.StatusBarManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class LoginActivity extends AppCompatActivity {

    KatikaSharedPreferences preferences;
    Toolbar toolbarRecoverData;
    EditText typeClientNumber;
    EditText typePassword;
    TextView viewBadPassword;
    TextView viewInstruction;
    DatabaseReference userDatabase;
    Button recoverDataButton;

    public final String TAG = "RECOVER DATA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Orientation portrait programmatically
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //Toolbar
        toolbarRecoverData = (Toolbar) findViewById(R.id.toolbar_recover_data);
        setSupportActionBar(toolbarRecoverData);

        preferences = new KatikaSharedPreferences(this);

        // FIREBASE INITIALISATION
        userDatabase = FirebaseDatabase.getInstance().getReference("Users");

        typeClientNumber = (EditText) findViewById(R.id.user_type_clinum);
        typePassword = (EditText) findViewById(R.id.user_type_password);
        viewBadPassword = (TextView) findViewById(R.id.bad_password_typed);
        viewInstruction = (TextView) findViewById(R.id.text_explane_type_clinum_pass);
        recoverDataButton = (Button) findViewById(R.id.SubmitConnectParamButton);

        //Update status bar
        new StatusBarManager().translucideCustomStatusBar(LoginActivity.this,toolbarRecoverData, R.color.colorLightGray);

        recoverDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String userClientNum = typeClientNumber.getText().toString();
                String userPass = typePassword.getText().toString();
                final int myPass;
                //TODO: put correct strings
                if (userClientNum.isEmpty()) {
                    viewBadPassword.setText(getResources().getString(R.string.empty_text_zone));
                    viewBadPassword.setVisibility(View.VISIBLE);
                } else if (userClientNum.length() != 14) {
                    viewBadPassword.setText(getResources().getString(R.string.clinum_size_differs_fourteen));
                    viewBadPassword.setVisibility(View.VISIBLE);
                } else if (userPass.isEmpty()) {
                    viewBadPassword.setText(getResources().getString(R.string.empty_password));
                    viewBadPassword.setVisibility(View.VISIBLE);
                } else if (userPass.length() != 4) {
                    viewBadPassword.setText(getResources().getString(R.string.pass_size_differs_four));
                    viewBadPassword.setVisibility(View.VISIBLE);
                } else {
                    try {
                        //TODO: create the back end function
                        myPass = Integer.parseInt(userPass);
                        new RecoverDataAsync(LoginActivity.this).execute(getString(R.string.url_request_recover_user), userClientNum, userPass);
                    } catch (NumberFormatException excp) {
                        viewBadPassword.setText(getResources().getString(R.string.number_pass_exception));
                        viewBadPassword.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    private class RecoverDataAsync extends AsyncTask<String, Void, String>{

        Context aContext;
        public RecoverDataAsync(Context mContext){
            aContext = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ConstraintLayout mConstraintLayout = findViewById(R.id.loading_layout_recover_data);
            mConstraintLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String aString) {
            super.onPostExecute(aString);
            preferences = new KatikaSharedPreferences(aContext);

            Log.d(TAG, "onPostExecute: complet string = " + aString);
            //convert Json string into object
            userExists nUserExists = getUserExists(aString);

            if(nUserExists.getuExists()){

                Log.d(TAG, "onPostExecute: first name = "+ nUserExists.uFirstName);
                Log.d(TAG, "onPostExecute: name  = "+ nUserExists.uName);
                Log.d(TAG, "onPostExecute: phone = "+ nUserExists.uPhone);
                Log.d(TAG, "onPostExecute: uid = "+ nUserExists.uId);
                ConstraintLayout mConstraintLayout = findViewById(R.id.loading_layout_recover_data);
                mConstraintLayout.setVisibility(View.INVISIBLE);


                preferences.setUserFirstName(nUserExists.uFirstName);
                preferences.setUserName(nUserExists.uName);
                preferences.setUserPhone(nUserExists.uPhone);

                preferences.setTempFirstName(nUserExists.uFirstName);
                preferences.setTempName(nUserExists.uName);
                //preferences.setUserId(nUserExists.uId);
                preferences.setTempPhone(nUserExists.uPhone);

                preferences.setAccountConnected(true);
                //TODO: create activity confirm phone Number or redirect to phone verifier with Bundle variables
                //Send a SMS to user's phone number
                PhoneNumberVerifierService.startActionVerify(LoginActivity.this, preferences.getUserPhone());
                //Goto phone Verifier
                Intent userIntent = new Intent(LoginActivity.this, PhoneVerifier.class);
                userIntent.putExtra("recovering_data", 1);
                startActivity(userIntent);
                finish();
            } else {
                ConstraintLayout mConstraintLayout = findViewById(R.id.loading_layout_recover_data);
                mConstraintLayout.setVisibility(View.INVISIBLE);
                viewBadPassword.setText(getResources().getString(R.string.bad_password));
                viewBadPassword.setVisibility(View.VISIBLE);
            }

        }

        @Override
        protected String doInBackground(String... strings) {
            String url = strings[0];
            String userId = strings[1];
            String userPass = strings[2];
            preferences = new KatikaSharedPreferences(aContext);

            //Reset the user id for the future
            preferences.setUserId(userId);

            JSONObject args = new JSONObject();
            RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
            JSONObject response = new JSONObject();
            try {
                args.put("userId", userId);
                args.put("userPass", userPass);

                final RequestFuture<JSONObject> future = RequestFuture.newFuture();
                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                        url, args, future, future);
                requestQueue.add(request);
                try {
                    response = future.get(10, TimeUnit.SECONDS);
                    //return response.getString("userExists");
                    return response.toString();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    Log.e(TAG, "TIME OUT"+e.getMessage());
                }
            } catch (JSONException excp){
                Log.d(TAG, excp.getMessage());
            }
            return null;
        }

        private userExists getUserExists(String userExistsJson){
            Gson myGson = new Gson();
            return myGson.fromJson(userExistsJson, userExists.class);
        }
    }

    private class userExists{
        private String uName;
        private String uFirstName;
        private String uPhone;
        private String uId;
        private Boolean uExists;



        public String getuName() {
            return uName;
        }

        public void setuName(String uName) {
            this.uName = uName;
        }

        public String getuFirstName() {
            return uFirstName;
        }

        public void setuFirstName(String uFirstName) {
            this.uFirstName = uFirstName;
        }

        public String getuPhone() {
            return uPhone;
        }

        public void setuPhone(String uPhone) {
            this.uPhone = uPhone;
        }

        public Boolean getuExists() {
            return uExists;
        }

        public void setuExists(Boolean uExists) {
            this.uExists = uExists;
        }
    }


}
