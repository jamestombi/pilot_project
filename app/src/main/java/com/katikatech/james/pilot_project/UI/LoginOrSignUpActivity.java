package com.katikatech.james.pilot_project.UI;

import android.content.Intent;
import android.content.pm.ActivityInfo;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.katikatech.james.pilot_project.R;
import com.katikatech.james.pilot_project.utils.StatusBarManager;

public class LoginOrSignUpActivity extends AppCompatActivity {

    Toolbar toolbarLoginSignup;
    Button LoginButton;
    Button SignUpButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_or_sign_up);

        //Orientation portrait programmatically
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //Toolbar
        toolbarLoginSignup= (Toolbar) findViewById(R.id.toolbar_login_signup_page);
        setSupportActionBar(toolbarLoginSignup);

        //Buttons
        LoginButton = (Button) findViewById(R.id.SubmitLoginButton);
        SignUpButton = (Button) findViewById(R.id.SubmitSignUpButton);

        //Update status bar
        new StatusBarManager().translucideCustomStatusBar(LoginOrSignUpActivity.this, toolbarLoginSignup, R.color.colorPrimary);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_close);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent accountIntent = new Intent(LoginOrSignUpActivity.this, CreateAccount.class);
                startActivity(accountIntent);
                finish();
            }
        });

        LoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginIntent = new Intent(LoginOrSignUpActivity.this, LoginActivity.class);
                startActivity(loginIntent);
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    //we press the back button
    public void onBackPressed() {
        finish();
    }


}
