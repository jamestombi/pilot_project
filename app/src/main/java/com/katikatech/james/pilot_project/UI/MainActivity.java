package com.katikatech.james.pilot_project.UI;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.TransitionDrawable;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;

import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.R;

public class MainActivity extends AppCompatActivity {
    private static int ST_TIME_OUT = 2000;
    private KatikaSharedPreferences preferences;
    ImageView splashImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //retirer de cette page, toutes les notifications du haut du smartphone
        //batterie, reseau, etc.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        //Dans cette ligne, maintenir en mode Portrait
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        preferences = new KatikaSharedPreferences(this);
        splashImage = (ImageView) findViewById(R.id.splashImage);

        ((TransitionDrawable) splashImage.getDrawable()).startTransition(1500);

        //final SharedPreferences sharedPreferences =
        //        PreferenceManager.getDefaultSharedPreferences(this);

        // ajouter un handler qui fera une action après un délai,
        // l'action sera de lancer une nouvelle activité
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {

                //Ne lancer cette activité qu'après l'installation et éventuellement en mode d'emploi
                if (!preferences.getCompletePresentation()) {
                    startActivity(new Intent(MainActivity.this, slidePage.class));
                    finish();
                } else if (!preferences.getFirstAccountCreation()) {
                    startActivity(new Intent(MainActivity.this, LoginOrSignUpActivity.class));
                    finish();
                }else {
                    //Intent homeIntent = new Intent(MainActivity.this, UserBoard.class);
                    Intent homeIntent = new Intent(MainActivity.this, PasswordActivity.class);
                    startActivity(homeIntent);
                    finish();
                }

            }

        }, ST_TIME_OUT);
    }
}
