package com.katikatech.james.pilot_project.UI;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.R;
import com.katikatech.james.pilot_project.services.LoadBalanceIntentService;
import com.katikatech.james.pilot_project.utils.StatusBarManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class PasswordActivity extends AppCompatActivity {

    private KatikaSharedPreferences preferences;
    Toolbar toolbarTypePassword;
    EditText editPassword;
    TextView viewUserFirstName;
    TextView viewBadPassword;
    TextView changeUser;
    ConstraintLayout lockConstraint;

    public final String TAG = "PASSWORD ACTIVITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);

        //Orientation portrait programmatically
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //Toolbar
        toolbarTypePassword = (Toolbar) findViewById(R.id.toolbar_type_password);
        setSupportActionBar(toolbarTypePassword);

        preferences = new KatikaSharedPreferences(this);



        editPassword = (EditText) findViewById(R.id.user_password);
        viewUserFirstName = (TextView) findViewById(R.id.user_first_name_name);
        viewBadPassword = (TextView) findViewById(R.id.bad_password);
        String completeName = (preferences.getUserFirstName() + " " + preferences.getUserName()).toLowerCase();
        viewUserFirstName.setText(completeName);
        lockConstraint = (ConstraintLayout) findViewById(R.id.locker_layout_password);
        changeUser = (TextView) findViewById(R.id.change_user);

        Log.d(TAG, "PASWD  " + preferences.getLockBadPswd());
        if (preferences.getLockBadPswd() >= 3){
            Log.d(TAG, "LOCK PASWD  " + preferences.getLockBadPswd());
            lockConstraint.setVisibility(View.VISIBLE);
        }

        new StatusBarManager().translucideCustomStatusBar(PasswordActivity.this,toolbarTypePassword, R.color.colorLightGray);

        editPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
                Log.d("TEXT_BEFORE_TEXT", charSequence.toString());
                if(count == 4) {
                    Intent userIntent = new Intent(PasswordActivity.this, UserBoard.class);
                    startActivity(userIntent);
                    finish();
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                Log.d("TEXT_ON_TEXT", charSequence.toString());
                if(charSequence.length() == 4) {
                    if(!charSequence.toString().equals("0000")) {

                        new ConnectAsyncTask(PasswordActivity.this)
                                .execute(getString(R.string.url_request_connection), charSequence.toString());
                        //.execute(getString(R.string.url_request_balance), charSequence.toString());

                    } else {
                        viewBadPassword.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.d("TEXT_AFTER_TEXT", editable.toString());
                //if (editable.toString().length() == 4) {
                    //editPassword.setText("");
                //}
            }
        });

        changeUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent userIntent = new Intent(PasswordActivity.this, LoginActivity.class);
                startActivity(userIntent);
                finish();
            }
        });
    }

    private class ConnectAsyncTask  extends AsyncTask<String, Void, Boolean>{

        private Context mContext;
        public ConnectAsyncTask(Context context){
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ConstraintLayout mConstraintLayout = findViewById(R.id.loading_layout_type_password);
            mConstraintLayout.setVisibility(View.VISIBLE);

        }

        @Override
        protected void onPostExecute(Boolean aBool) {
            super.onPostExecute(aBool);
            if(aBool) {
                preferences.setLockBadPswd(0);
                LoadBalanceIntentService.startActionBaz(PasswordActivity.this, getString(R.string.url_request_sync), "");
                preferences.setAccountConnected(true);
                Intent userIntent = new Intent(PasswordActivity.this, UserBoard.class);
                startActivity(userIntent);
                finish();
            } else {
                viewBadPassword.setVisibility(View.VISIBLE);
                ConstraintLayout mConstraintLayout = findViewById(R.id.loading_layout_type_password);
                mConstraintLayout.setVisibility(View.INVISIBLE);
                int Lock = preferences.getLockBadPswd() + 1;
                preferences.setLockBadPswd(Lock);
                Log.d(TAG, "PASS INCORRECT: "+ Lock);
                if (Lock >= 3){
                    ConstraintLayout lockConstraintLayout = findViewById(R.id.locker_layout_password);
                    lockConstraintLayout.setVisibility(View.VISIBLE);
                    //start a new service that will last 15 minutes and set it back to invisible
                }
                /* clear the text zone */


            }
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            String url = strings[0];
            String passString = strings[1];
            JSONObject args = new JSONObject();
            RequestQueue requestQueue = Volley.newRequestQueue(PasswordActivity.this);
            JSONObject response = new JSONObject();
            try {
                args.put("userid", preferences.getUserId());
                args.put("pass", passString);
                //editPassword.getText().clear();

                final RequestFuture<JSONObject> future = RequestFuture.newFuture();
                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                        url, args, future, future);
                requestQueue.add(request);
                try {
                    response = future.get(10, TimeUnit.SECONDS);
                    //Log.d("TAG LOG ---> ", ""+response.getDouble("balance"));
                    return response.getBoolean("success");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    Log.e(TAG, "TIME OUT: "+e.getMessage());
                }
            } catch (JSONException excp){
                Log.d(TAG, excp.getMessage());
            }
            return false;
        }

    }
}
