package com.katikatech.james.pilot_project.UI;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.katikatech.james.pilot_project.Currency;
import com.katikatech.james.pilot_project.CurrencySpinnerAdapter;
import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.services.NotificationService;
import com.katikatech.james.pilot_project.R;
import com.katikatech.james.pilot_project.utils.StatusBarManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class PhoneVerifier extends AppCompatActivity {

    Toolbar toolbarVerifyPhone;
    Spinner currencySpinner;
    Button submitAccount;
    Button submitSendSMS;
    EditText smsCodeEdit;
    TextView textExplanation;
    TextView textExplaneCurrency;
    int loginMode = 0;
    private KatikaSharedPreferences preferences;
    private static final String TAG = ".UI.PhoneVerifier";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_verifier);
        preferences = new KatikaSharedPreferences(this);
        currencySpinner = (Spinner) findViewById(R.id.currency_spinner);
        submitAccount = (Button) findViewById(R.id.submitVerifyButton);
        submitSendSMS = (Button) findViewById(R.id.SubmitSendSMSButton);
        smsCodeEdit = (EditText) findViewById(R.id.sms_code);
        textExplanation = (TextView) findViewById(R.id.text_explane_phone_verification);
        textExplaneCurrency = (TextView) findViewById(R.id.text_exp_currency);

        try{
            loginMode = getIntent().getExtras().getInt("recovering_data");
        } catch (NullPointerException excp){
            Log.d(TAG, "Not recovering data, just new signup or changing something");
            loginMode = 0;
        }

        //Orientation portrait programmatically
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        toolbarVerifyPhone = (Toolbar) findViewById(R.id.toolbar_verify_phone);
        setSupportActionBar(toolbarVerifyPhone);

        /*
         * Check connection
         */
        //preferences = new KatikaSharedPreferences(this);
        //preferences.setActivityOn(true);
        //if(!preferences.getAccountConnected()){
        //    Intent connectionIntent = new Intent(PhoneVerifier.this, PasswordActivity.class);
        //    startActivity(connectionIntent);
        //    finish();
        //}

        //Update status bar
        new StatusBarManager().translucideCustomStatusBar(PhoneVerifier.this, toolbarVerifyPhone, R.color.colorWhite);

        if(loginMode == 0) {
            textExplanation.setText(getResources().getString(R.string.phone_verify_explane_verification));
            submitSendSMS.setVisibility(View.INVISIBLE);

            //final List<Currency> listCurrencies = new ArrayList<Currency>();

            //listCurrencies.add(new Currency(1, "XAF", "Zone Franc (Afrique Centrale)"));
            //listCurrencies.add(new Currency(2, "XOF", "Zone Franc (Afrique de l'Ouest)"));
            //listCurrencies.add(new Currency(3, "EUR", "Zone EURO"));
            //listCurrencies.add(new Currency(4, "JPY", "Japon"));
            //listCurrencies.add(new Currency(5, "USD", "Dollar Américain"));

            currencySpinner.setAdapter(
                    new CurrencySpinnerAdapter(this,
                            KatikaSharedPreferences.KatikalistCurrencies));

            currencySpinner.setSelection(preferences.getUserCurrencyId() - 1);

            currencySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    // do something after selected item here
                    Currency currency = KatikaSharedPreferences.KatikalistCurrencies.get(position);
                    preferences.setUserCurrencyId(currency.getCurrencyId());
                    preferences.setUserCurrencySymbol(currency.getCurrencySymbol());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } else {
            textExplaneCurrency.setVisibility(View.INVISIBLE);
            currencySpinner.setVisibility(View.INVISIBLE);
            String recoveringCheckString = getResources().getString(R.string.phone_verify_future_sending_sms) +
                    maskPartial(preferences.getUserPhone());
            textExplanation.setText(recoveringCheckString);
            submitSendSMS.setVisibility(View.VISIBLE);
            smsCodeEdit.setVisibility(View.INVISIBLE);
            submitAccount.setVisibility(View.INVISIBLE);

            submitSendSMS.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    smsCodeEdit.setVisibility(View.VISIBLE);
                    submitAccount.setVisibility(View.VISIBLE);
                    submitSendSMS.setVisibility(View.INVISIBLE);
                }
            });
        }



        submitAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new AsyncTask<String, Void, Boolean>(){
                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        //dialog = new ProgressBar(PhoneVerifier.this);
                        //ConstraintLayout mConstraintLayout = findViewById(R.id.loading_layout_create_account);
                        //mConstraintLayout.setVisibility(View.VISIBLE);
                    }

                    @Override
                    protected void onPostExecute(Boolean aBoolean) {
                        super.onPostExecute(aBoolean);
                        //dialog.setProgress(100);
                        preferences.setAccountConnected(true);
                        Intent checkPhoneIntent = new Intent(PhoneVerifier.this, CheckPhoneActivity.class);
                        checkPhoneIntent.putExtra("success", aBoolean);
                        if(loginMode == 1){
                            checkPhoneIntent.putExtra("recovering_data", true);
                        }
                        startActivity(checkPhoneIntent);;
                        finish();
                    }

                    @Override
                    protected Boolean doInBackground(String... strings) {
                        //String smsCodeString = strings[0];
                        String smsMessage = strings[2];
                        if(!smsMessage.isEmpty()) {
                            String url = strings[0];
                            String phoneNo = strings[1];
                            JSONObject args = new JSONObject();
                            RequestQueue requestQueue = Volley.newRequestQueue(PhoneVerifier.this);
                            JSONObject response = new JSONObject();
                            try {
                                args.put("phone", phoneNo);
                                args.put("sms_message", smsMessage);

                                final RequestFuture<JSONObject> future = RequestFuture.newFuture();
                                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                                        url, args, future, future);
                                requestQueue.add(request);
                                try {
                                    response = future.get(10, TimeUnit.SECONDS);
                                    Log.e(TAG, "RESPONSE GIVEN "+response.getBoolean("code"));
                                    return response.getBoolean("code");
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                } catch (ExecutionException e) {
                                    e.printStackTrace();
                                } catch (TimeoutException e) {
                                    Log.e(TAG, "TIME OUT"+e.getMessage());
                                }
                            } catch (JSONException excp){
                                Log.d(TAG, excp.getMessage());
                            }
                            return false;
                        }
                        return false;
                    }
                }.execute(getString(R.string.url_verify), preferences.getTempPhone(), smsCodeEdit.getText().toString());
                
                /*if(!preferences.getFirstAccountCreation()) {

                    preferences.setFirstAccountCreation(true);
                    if(!smsCodeEdit.getText().toString().isEmpty()) {
                        PhoneNumberVerifierService.startActionSMSVerification(PhoneVerifier.this, smsCodeEdit.getText().toString());
                    }
                    Intent createPasswordIntent = new Intent(PhoneVerifier.this, CreatePasswordActivity.class);
                    startActivity(createPasswordIntent);
                    finish();
                } else {

                    preferences.setAccountConnected(true);
                    if(!smsCodeEdit.getText().toString().isEmpty()) {
                        PhoneNumberVerifierService.startActionSMSVerification(PhoneVerifier.this, smsCodeEdit.getText().toString());
                    }
                    Intent userBoardIntent = new Intent(PhoneVerifier.this, UserBoard.class);
                    startActivity(userBoardIntent);
                    finish();
                }*/
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(loginMode == 0) {
            preferences.setAccountConnected(true);
            if (item.getItemId() == android.R.id.home) {
                Intent createAccountIntent = new Intent(PhoneVerifier.this, CreateAccount.class);
                startActivity(createAccountIntent);
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    //we press the back button
    public void onBackPressed() {
        if(loginMode == 0) {
            preferences.setAccountConnected(true);
            Intent createAccountIntent = new Intent(PhoneVerifier.this, CreateAccount.class);
            startActivity(createAccountIntent);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        preferences = new KatikaSharedPreferences(this);

        NotificationService.stopActionTimeOut(PhoneVerifier.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(true);
        //preferences.setAccountConnected(true);
        NotificationService.startActionTimeOut(PhoneVerifier.this);
        Log.d("Pho PAUSED", "status connected: "+ preferences.getAccountConnected());
    }

    @Override
    protected void onStop() {
        super.onStop();
        preferences = new KatikaSharedPreferences(this);
        if(!preferences.getActivityOn()) {
            preferences.setAccountConnected(false);
            Log.d("Pho STOPPED", "status connected: " + preferences.getAccountConnected());
        } else {
            preferences.setAccountConnected(true);
            Log.d("PhoBIBOARD STOPPED", "status connected: " + preferences.getAccountConnected());
        }
    }

    protected String maskPartial(String phoneNumber){
        String text;

        int textLength = phoneNumber.length();
        text = phoneNumber.substring(0, 4) +
                phoneNumber.substring(4, textLength - 3).replaceAll("[0-9]", "*") +
                phoneNumber.substring(textLength - 3);

        return text;
    }
}
