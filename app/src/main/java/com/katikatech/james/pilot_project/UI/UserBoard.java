package com.katikatech.james.pilot_project.UI;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.katikatech.james.pilot_project.services.LoadBalanceIntentService;
import com.katikatech.james.pilot_project.userCardItem.AccountCard;
import com.katikatech.james.pilot_project.userCardItem.BalanceCard;
import com.katikatech.james.pilot_project.userCardItem.CardItem;
import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.userCardItem.LastOperationsCard;
import com.katikatech.james.pilot_project.services.NotificationService;
import com.katikatech.james.pilot_project.Operation;
import com.katikatech.james.pilot_project.OperationRepository;
import com.katikatech.james.pilot_project.R;
import com.katikatech.james.pilot_project.recyclerview.UserBoardRecyclerView;
import com.katikatech.james.pilot_project.utils.StatusBarManager;


import java.util.ArrayList;
import java.util.List;

public class UserBoard extends AppCompatActivity {

    public RecyclerView recyclerView;
    public UserBoardRecyclerView myRecyclerView;
    public String accountName;
    public String accountFirstName;
    public String accountPhone;
    public String lastUpdate;
    KatikaSharedPreferences preferences;
    Boolean recoverData = false;

    public class MyReceiver extends BroadcastReceiver {
        public static final String ACTION_RESP ="com.myapp.intent.action.TEXT_TO_DISPLAY";
        private float distBalance;

        @Override
        public void onReceive(Context context, Intent intent) {
            float balance = intent.getFloatExtra(LoadBalanceIntentService.SOURCE_URL, 0);
            if(balance > 0){
                //Intent resetIntent = new Intent(UserBoard.this, UserBoard.class);
                //finish();
                //overridePendingTransition(0, 0);
                //startActivity(getIntent());
                //overridePendingTransition(0, 0);
            }
            // send text to display
            Log.d("RESULT INTENT", ""+ balance);
            //Toast.makeText(context, "Balance = "+balance, Toast.LENGTH_LONG).show();
            setDistBalance(balance);
            preferences.setAccountBalance(balance);

        }

        public void setDistBalance(float balance){
            distBalance = balance;
        }

        public float getDistBalance(){
            return distBalance;
        }

    }

    private MyReceiver receiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_board);

        /*
         * Check connection
         */
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(true);
        if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(UserBoard.this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        }

        //Orientation portrait programmatically
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Toolbar toolbar = findViewById(R.id.toolbar_user_board);
        setSupportActionBar(toolbar);

        accountName = "";
        accountFirstName = "";
        accountPhone = "";
        lastUpdate = "";

        accountName = preferences.getUserName();
        accountFirstName = preferences.getUserFirstName();
        accountPhone = preferences.getUserPhone();
        lastUpdate = preferences.getLastUpdate();


        try{
            recoverData = getIntent().getExtras().getBoolean("recovering_data");
            Log.d("TAG", "CHECK RESULT " + recoverData);
        } catch (NullPointerException excp){
            recoverData = false;
            Log.d("TAG", "NO CHECK RESULT " + recoverData);
            //Log.d(TAG, "isFromUserBoard FAILED");
        }

        //Update status bar
        new StatusBarManager().translucideCustomStatusBar(UserBoard.this, toolbar, R.color.colorLightGray);



        /***
         *
         * Static DATA set
         *
         * */
        //Le broadcastReceiver déclaré
        receiver = new MyReceiver();
        // on lance l'intent service
        //LoadBalanceIntentService.startActionFoo(UserBoard.this, getString(R.string.url_request_balance), "");
        //LoadBalanceIntentService.startActionBaz(UserBoard.this, getString(R.string.url_request_sync), "");

        preferences.setAccountBalance(new OperationRepository(this.getApplication()).getBalance());

        // data that will be used for the UserBoard
        List<CardItem> staticData = new ArrayList<CardItem>();

        //Data used to display the balance of the account
        BalanceCard myBalance = new BalanceCard(preferences.getAccountBalance(), 0, lastUpdate);

        // Data used to display informations about the user
        AccountCard myAccount;
        if (accountName.isEmpty()) {
            myAccount = new AccountCard("FIRSTNAME NAME", "01 23 34 56 78", preferences.getUserId());
        } else {
            String completeName = (accountFirstName + " " + accountName).toUpperCase();
            myAccount = new AccountCard(completeName, accountPhone, preferences.getUserId());
        }

        //Retrieve the last three operations in Date
        List<Operation> myLastOperations = new OperationRepository(
                this.getApplication()).myLastThreeOperations();

        int i;
        List<LastOperationsCard> mLastOperations = new ArrayList<LastOperationsCard>();
        for(i = 0; i < myLastOperations.size(); i++){
            mLastOperations.add(new LastOperationsCard(myLastOperations.get(i).getInsertedAt(),
                    myLastOperations.get(i).getDescription(), ""+myLastOperations.get(i).getAmount(),
                    myLastOperations.get(i).getRemoteKey()));
        }
        //List<Float> incomesValues = new ArrayList<Float>();
        //List<Float> expensesValues = new ArrayList<Float>();

        //incomesValues.add((float)25.0);
        //incomesValues.add((float)15.0);
        //incomesValues.add((float)40.0);
        //incomesValues.add((float)34.0);

        //expensesValues.add((float)75.0);
        //expensesValues.add((float)23.0);
        //expensesValues.add((float)50.0);
        //expensesValues.add((float)20.0);

        //TreeMap<String, List<Float>> balanceTreeMap = new TreeMap<String, List<Float>>();
        //balanceTreeMap.put("Incomes", incomesValues);
        //balanceTreeMap.put("Expenses", expensesValues);
        //GraphCard  myGraph = new GraphCard(balanceTreeMap, "");

        //Add all informations to the List of Items
        staticData.add(myAccount);
        staticData.add(myBalance);
        for(i = 0; i < mLastOperations.size(); i ++) {
            staticData.add(mLastOperations.get(i));
        }
        /**
         * End of STATIC DATA SET
         *
         */

        //Fill the recyclerView with staticData
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview_user_board);
        recyclerView.setLayoutManager(new LinearLayoutManager(UserBoard.this));

        myRecyclerView = new UserBoardRecyclerView(staticData, UserBoard.this);
        //adapt all the outputs of the list to the design of the new adapter
        recyclerView.setAdapter(myRecyclerView);



        FloatingActionButton fab = findViewById(R.id.fab_new_transaction);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            fab.setTooltipText("Ajouter une nouvelle operation");
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferences.setAccountConnected(true);
                Intent newTransactionIntent = new Intent(UserBoard.this, InsertOperationActivity.class);
                //Set extra "userBoardOrigin" to 1
                newTransactionIntent.putExtra("userBoardOrigin", 1);
                startActivity(newTransactionIntent);
                finish();
            }
        });

/*
        FloatingActionButton fab = findViewById(R.id.fab_new_transaction);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoadBalanceIntentService.startActionFoo(UserBoard.this, getString(R.string.url_request_balance), "");
                IntentFilter filter = new IntentFilter(MyReceiver.ACTION_RESP);
                filter.addCategory(Intent.CATEGORY_DEFAULT);
                registerReceiver(receiver, filter);
            }
        });
*/
    }

    //we press the back button
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(UserBoard.this);
        builder.setMessage(getResources().getString(R.string.disconnect_question))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        dialog.dismiss();
                    }
                }).setNegativeButton(getResources().getString(R.string.cancel), null);

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_board, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        preferences.setAccountConnected(true);

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_parameters) {
            Intent SettingsIntent = new Intent(UserBoard.this, SettingsActivity.class);
            startActivity(SettingsIntent);
            finish();
        }

        if (id == R.id.action_about) {
            Intent AboutIntent = new Intent(UserBoard.this, AboutActivity.class);
            startActivity(AboutIntent);
            finish();
        }

        if (id == R.id.action_help) {
            Intent HelpIntent = new Intent(UserBoard.this, slidePage.class);
            startActivity(HelpIntent);
            finish();
        }

        if (id == R.id.action_contact_us) {
            Intent ContactIntent = new Intent(UserBoard.this, ContactActivity.class);
            startActivity(ContactIntent);
            finish();
        }

        if (id == R.id.action_profile) {
            Intent ProfileIntent = new Intent(UserBoard.this, CreateAccount.class);
            startActivity(ProfileIntent);
            finish();
        }

        if (id == R.id.action_disconnect) {
            AlertDialog.Builder builder = new AlertDialog.Builder(UserBoard.this);
            builder.setMessage(getResources().getString(R.string.disconnect_question))
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            dialog.dismiss();
                        }
                    }).setNegativeButton(getResources().getString(R.string.cancel), null);

            AlertDialog alert = builder.create();
            alert.show();
        }

       /* if (id == R.id.action_refresh) {
            myRecyclerView.notifyDataSetChanged();
        } */

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();
        preferences = new KatikaSharedPreferences(this);

        NotificationService.stopActionTimeOut(UserBoard.this);

        //Filter the reception of the broadcast
        IntentFilter filter = new IntentFilter(MyReceiver.ACTION_RESP);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(receiver, filter);

        if(recoverData) {
            Log.d("tag", "onResume: recovering");
        }

        /**
         * Check connection
         */
        if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(UserBoard.this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        } else {
            preferences.setAccountConnected(false);
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(false);
        //preferences.setAccountConnected(true);
        NotificationService.startActionTimeOut(UserBoard.this);

        unregisterReceiver(receiver);

        Log.d("BOARD PAUSED", "status connected: "+ preferences.getAccountConnected());
    }

    @Override
    protected void onStop() {
        super.onStop();
        preferences = new KatikaSharedPreferences(this);
        if(!preferences.getActivityOn()) {
            preferences.setAccountConnected(false);
            Log.d("BOARD STOPPED", "status connected: " + preferences.getAccountConnected());
        } else {
            preferences.setAccountConnected(true);
            Log.d("BIBOARD STOPPED", "status connected: " + preferences.getAccountConnected());
        }
    }

    /*public float getDistBalance()
    {

        float myBalance = 0.f;

        AsyncTask<String, Void, Float> myTask  = new AsyncTask<String, Void, Float>(){
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ConstraintLayout mConstraintLayout = findViewById(R.id.loading_layout_user_board);
                mConstraintLayout.setVisibility(View.VISIBLE);
            }

            @Override
            protected void onPostExecute(Float aFloat) {
                super.onPostExecute(aFloat);
                //viewBadPassword.setVisibility(View.VISIBLE);
                ConstraintLayout mConstraintLayout = findViewById(R.id.loading_layout_user_board);
                mConstraintLayout.setVisibility(View.INVISIBLE);
                *//* clear the text zone *//*
                //editPassword.getText().clear()
            }

            @Override
            protected Float doInBackground(String... strings) {
                String url = strings[0];
                JSONObject args = new JSONObject();
                RequestQueue requestQueue = Volley.newRequestQueue(UserBoard.this);
                JSONObject response = new JSONObject();

                try {
                    args.put("userid", preferences.getUserId());
                    final RequestFuture<JSONObject> future = RequestFuture.newFuture();
                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                            url, args, future, future);
                    requestQueue.add(request);
                    Log.d("TAG OUT --> ", response.toString());
                    try {
                        Log.d("TAG ON --> ", response.toString());
                        response = future.get(10, TimeUnit.SECONDS);
                        Log.d("TAG --> ", response.toString());
                        //Log.d("jazz: ", ""+ BigDecimal.valueOf(response.getDouble("balance")).floatValue());
                        return 2.f;
                        //return BigDecimal.valueOf(response.getDouble("balance")).floatValue();

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (TimeoutException e) {
                        Log.e("toto", "TIME OUT: "+e.getMessage());
                    }
                } catch (JSONException excp){
                    Log.d("toto --> ", excp.getMessage());
                }

                return 0.0f;
            }
        }.execute(getString(R.string.url_request_balance));

        try {
            myBalance = myTask.get();
        } catch (InterruptedException e) {
            Log.d("JTO",e.getMessage());
        } catch (ExecutionException e) {
            Log.d("JTO",e.getMessage());
        }

        return myBalance;
    }*/


}
