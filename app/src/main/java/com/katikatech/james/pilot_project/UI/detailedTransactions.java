package com.katikatech.james.pilot_project.UI;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;

import androidx.annotation.NonNull;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.katikatech.james.pilot_project.FragmentTransaction;
import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.services.NotificationService;
import com.katikatech.james.pilot_project.OperationAccountFragment;
import com.katikatech.james.pilot_project.R;
import com.katikatech.james.pilot_project.utils.StatusBarManager;

import java.util.ArrayList;
import java.util.List;

public class detailedTransactions extends AppCompatActivity {

    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    int filterItem;
    KatikaSharedPreferences preferences;
    public final static String TAG = "detailed Transactions";
    private ActionMode mActionMode;


    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_transactions);
        //Orientation portrait programmatically
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);



        /**
         * Check Connection
         */
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(true);
        if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(detailedTransactions.this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        }
        //Retrieve extras infos from the activity
        try {
            filterItem = getIntent().getExtras().getInt("filterItemSelected");
        } catch (NullPointerException excp) {
            filterItem =  R.id.id_no_filter;
        }

        // Get the intent, verify the action and get the query
        Intent intent = getIntent();

        //Get instance of the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_transactions);
        setSupportActionBar(toolbar);

        new StatusBarManager().translucideCustomStatusBar(detailedTransactions.this, toolbar, R.color.colorWhite);

        Bundle filterBundle = new Bundle();
        filterBundle.putInt("filterItem", filterItem);

        //Manage the search here
        //the activity is the home of the search
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Log.d(TAG, "Query string "+ query);
            filterBundle.putString("searchItem", query);
        }


        //Set toolbar title according to the filter
        switch (filterItem){
            case R.id.id_no_filter:
                toolbar.setTitle("Transactions");
                break;
            case R.id.id_this_week_filter:
                toolbar.setTitle("Historique");
                toolbar.setSubtitle("Cette semaine");
                break;
            case R.id.id_this_month_filter:
                toolbar.setTitle("Historique");
                toolbar.setSubtitle("Ce mois");
                break;
            case R.id.id_last_month_filter:
                toolbar.setTitle("Historique");
                toolbar.setSubtitle("Dernier mois");
                //Add filterItem in bundle
                filterBundle.putInt("filterItem", filterItem);
                break;
            case R.id.id_90_days_filter:
                toolbar.setTitle("Historique");
                toolbar.setSubtitle("3 derniers mois");
                break;
            case R.id.id_current_year_filter:
                toolbar.setTitle("Historique");
                toolbar.setSubtitle("Année en cours");
                break;
            default:
                toolbar.setTitle("Transactions");
                break;
        }

        //Set the back button on action Bar
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        //Add instance Fragments
        if(!preferences.getLastUpdate().isEmpty()) {
            mSectionsPagerAdapter.addFragment(FragmentTransaction.newInstance(filterItem, 0));
            mSectionsPagerAdapter.addFragment(OperationAccountFragment.newInstance(filterItem));
        } else {
            mSectionsPagerAdapter.addFragment(PlaceholderFragment.newInstance(filterItem));
            mSectionsPagerAdapter.addFragment(PlaceholderFragment.newInstance(filterItem));
        }
        //mSectionsPagerAdapter.addFragment(PlaceholderFragment.newInstance(filterItem));
        //mSectionsPagerAdapter.addFragment(PlaceholderFragment.newInstance(filterItem));

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        final FloatingActionButton fabnew = (FloatingActionButton) findViewById(R.id.floatingActionButton);

        //Listeners to Tab changing
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 0){
                    fab.hide();
                    fabnew.show();
                } else {
                    fab.hide();
                    fabnew.show();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });




        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        fabnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferences.setAccountConnected(true);
                Intent homeIntent = new Intent(getApplicationContext(), InsertOperationActivity.class);
                startActivity(homeIntent);
                finish();
            }
        });

    }




    //we press the back button
    public void onBackPressed() {
        preferences.setAccountConnected(true);
        Intent respo_serv = new Intent(detailedTransactions.this, UserBoard.class);
        startActivity(respo_serv);
        finish();
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detailed_transactions, menu);
        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Log.d(TAG, s);
                Bundle filterBundle = new Bundle();
                filterBundle.putString("searchItem", s);
                mSectionsPagerAdapter.getItem(0).setArguments(filterBundle);
                mViewPager.getAdapter().notifyDataSetChanged();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Log.d(TAG, s);
                return false;
            }
        });

        return true;
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        preferences.setAccountConnected(true);
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_filter) {
            Intent filterIntent = new Intent(detailedTransactions.this, FilterPage.class);
            startActivity(filterIntent);
            finish();
            //Toast.makeText(getApplicationContext(),"Filter", Toast.LENGTH_SHORT).show();
            return true;
        }

        if (id == android.R.id.home){
            Intent UserBoardIntent = new Intent(detailedTransactions.this, UserBoard.class);
            startActivity(UserBoardIntent);
            finish();
            //Toast.makeText(getApplicationContext(),"Filter", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            //The fragment here will be used to put my_listview
            View rootView = inflater.inflate(R.layout.fragment_detailed_transactions, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format));
            return rootView;
        }
    }













    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public final List<Fragment> fragmentList = new ArrayList<>();
        //List<String> FragmentName = new ArrayList<>();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return fragmentList.get(position);

        }


        @Override
        public int getCount() {
            // Show 3 total pages.
            return fragmentList.size();
            //return 3;
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return super.getItemPosition(object);
        }

        public void addFragment(Fragment fragment){
            fragmentList.add(fragment);
            return;
        }


    }

   @Override
    protected void onResume() {
        super.onResume();
        preferences = new KatikaSharedPreferences(this);

       NotificationService.stopActionTimeOut(detailedTransactions.this);
        /**
         * Check connection
         */
        if(!preferences.getAccountConnected()){
            Intent connectionIntent = new Intent(detailedTransactions.this, PasswordActivity.class);
            startActivity(connectionIntent);
            finish();
        } else {
            preferences.setAccountConnected(false);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        preferences = new KatikaSharedPreferences(this);
        preferences.setActivityOn(false);
        //preferences.setAccountConnected(true);
        NotificationService.startActionTimeOut(detailedTransactions.this);
        Log.d(TAG, "paused, status connected: "+ preferences.getAccountConnected());
    }

    @Override
    protected void onStop() {
        super.onStop();
        preferences = new KatikaSharedPreferences(this);
        if (!preferences.getActivityOn()) {
            preferences.setAccountConnected(false);
            Log.d(TAG, "stopped, status connected: " + preferences.getAccountConnected());
        } else {
            preferences.setAccountConnected(true);
            Log.d(TAG, "stopped true, status connected: " + preferences.getAccountConnected());
        }
    }


    //ActionMode CallBack for new Menu
    private ActionMode.Callback mActionModeCallback2 = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            actionMode.getMenuInflater().inflate(R.menu.menu_operation_selected, menu);
            actionMode.setTitle("Style");
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {

            int id = menuItem.getItemId();
            preferences.setAccountConnected(true);

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_delete) {
                //Toast.makeText(this, "Good news", Toast.LENGTH_LONG).show();
                Log.d(TAG, "action delete");
                actionMode.finish();
                return true;
            }
            if (id == R.id.action_about) {
                //Toast.makeText(this, "Bad news", Toast.LENGTH_LONG).show();
                actionMode.finish();
                return true;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            mActionMode = null;
        }
    };





}
