package com.katikatech.james.pilot_project.UI;

import android.content.Intent;
import android.os.Build;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;

import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.R;
import com.katikatech.james.pilot_project.SliderAdapter;

//Classe qui représente les pages de slide
public class slidePage extends AppCompatActivity {

    public static String COMPLETE_PRESENTATION;
    private ViewPager mySlideViewPager;
    private KatikaSharedPreferences preferences;
    private LinearLayout myLinearLayout;
    private SliderAdapter sliderAdapter;
    private TextView [] nDots;
    private Toolbar toolbarSlide;

    private Button nextButton;
    private Button prevButton;
    private int nCurrentPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide_page);

        preferences = new KatikaSharedPreferences(this);

        //Toolbar
        //toolbarSlide = (Toolbar) findViewById(R.id.toolbar_slide);
        //setSupportActionBar(toolbarSlide);

        /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbarSlide = (Toolbar) findViewById(R.id.toolbar_slide);
        }
        setSupportActionBar(toolbarSlide);
*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color
            window.setStatusBarColor(ContextCompat.getColor(getApplication(),R.color.colorWhite));

            //toolbarSlide.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        }

        //On initialise le visualiseur de page slide et une layout linéaire pour les points
        //On les retrouve par leur ID donné dans les layout
        mySlideViewPager = (ViewPager) findViewById(R.id.slideViewPager);
        myLinearLayout = (LinearLayout) findViewById(R.id.dotsLayout);

        nextButton = (Button) findViewById(R.id.next);
        prevButton = (Button) findViewById(R.id.previous);

        //on instancie le side adapter avec le contexte de cette classe
        sliderAdapter = new SliderAdapter(this);

        mySlideViewPager.setAdapter(sliderAdapter);

        //On rajoute les points dans leur layout
        addDotsIndicator(0);

        //On ajoute le listener qui va agir sur les points inclus precedemment
        mySlideViewPager.addOnPageChangeListener(viewListener);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Cas de figure où on est déjà au dernier élément des slides
                if (nCurrentPage == nDots.length - 1){
                    if(!preferences.getFirstAccountCreation()) {
                        Intent homeIntent = new Intent(slidePage.this, LoginOrSignUpActivity.class);
                        startActivity(homeIntent);
                        finish();
                    } else {
                        preferences.setAccountConnected(true);
                        Intent homeIntent = new Intent(slidePage.this, UserBoard.class);
                        startActivity(homeIntent);
                        finish();
                    }
                }
                //cas nominal, on incrémente l'indice de la page à présenter
                mySlideViewPager.setCurrentItem(nCurrentPage + 1);
            }
        });

        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mySlideViewPager.setCurrentItem(nCurrentPage - 1);
            }
        });
    }

    //Il s'agit d'une activité qui va être vu uniquement au premier lancement de
    //l'application, donc on ne doit plus le voir après.
    @Override
    protected void onStop(){
        super.onStop();
        preferences.setCompletePresentation(true);
    }

    //we press the back button
    public void onBackPressed() {

        preferences.setAccountConnected(true);
        if(!preferences.getFirstAccountCreation()) {
            Intent userBoardIntent = new Intent(this, LoginOrSignUpActivity.class);
            startActivity(userBoardIntent);
            finish();
        } else {
            Intent userBoardIntent = new Intent(this, UserBoard.class);
            startActivity(userBoardIntent);
            finish();
        }

    }


    //fonction qui permet de créer les boutons pour les slides
    public void addDotsIndicator(int position){

        nDots = new TextView[3]; // Tableau de textview dans la layout, chacun représentera un point
        myLinearLayout.removeAllViews();

        for (int i = 0; i < nDots.length; i ++){
            nDots[i] = new TextView(this); // nouveau TextView
            //nDots[i].setText(Html.fromHtml("&#8226;")); //caractère ascii
            nDots[i].setText(Html.fromHtml("&#9679;")); //caractère ascii
            nDots[i].setTextSize(10);
            nDots[i].setTextColor(getResources().getColor(R.color.colorBanner)); // couleur du bouton

            myLinearLayout.addView(nDots[i]); // ajouter la vue au Layout
        }

        if (nDots.length > 0){
            nDots[position].setText(Html.fromHtml("&#9675;"));
            nDots[position].setTextSize(10);
            nDots[position].setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }


    //On implémente le listener qui va jouer sur l'effet des points et des boutons quand on change de page du
    //SlidePager
    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

            addDotsIndicator(position);

            nCurrentPage = position;

            if(position == 0){
                nextButton.setEnabled(true);
                prevButton.setEnabled(false);
                prevButton.setVisibility(View.INVISIBLE);

                nextButton.setText("NEXT");
                prevButton.setText("");

            } else if (position == nDots.length - 1){
                nextButton.setEnabled(true);
                prevButton.setEnabled(true);
                prevButton.setVisibility(View.VISIBLE);

                nextButton.setText("FINISH");
                prevButton.setText("PREV");
            } else {
                nextButton.setEnabled(true);
                prevButton.setEnabled(true);
                prevButton.setVisibility(View.VISIBLE);

                nextButton.setText("NEXT");
                prevButton.setText("PREV");
            }

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
}
