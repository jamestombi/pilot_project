package com.katikatech.james.pilot_project.broadcastReceivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

public class BootCompleteReceiver extends BroadcastReceiver {
    AlarmManager alarmManager;
    PendingIntent pendingIntent;

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            //NotificationService.startActionTimeOut(context);
            //LoadBalanceIntentService.startActionNotify(context);
            scheduleNotification(context, 18000000);
        }
    }

    public void scheduleNotification(Context context, int delay) {

        Intent boardIntent = new Intent(context, NotificationPublisher.class);
        boardIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, 101);
        //boardIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        pendingIntent = PendingIntent.getBroadcast(context, 101, boardIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        long futureInMillis = SystemClock.elapsedRealtime() + delay;
        alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, 18000000, pendingIntent);
        Log.d("toto", "scheduleNotification: alarm set ok for "+ futureInMillis + " milliseconds!!!");
    }
}
