package com.katikatech.james.pilot_project.broadcastReceivers;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import android.util.Log;

import com.katikatech.james.pilot_project.R;
import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.UI.MainActivity;

import static android.content.Context.NOTIFICATION_SERVICE;

public class NotificationPublisher extends BroadcastReceiver {

    public static String NOTIFICATION_ID = "101";
    private final String TAG = "NotificationPublisher";
    private final String CHANNEL_ID = "less_used";
    KatikaSharedPreferences preferences;


    /*Create a channel for notifications in ANDROID version above 8.0.0 */
    private void createNotificationChannel(Context context) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Project Base";
            String description = "A test of notifications";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationChannel.setDescription(description);
            notificationChannel.enableLights(true);
            //notificationChannel.setLightColor(context.getResources().getColor(R.color.colorGold, context.getTheme()));
            notificationChannel.setLightColor(Color.rgb(0xFF,0xD7,0x00));
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

            notificationManager.createNotificationChannel(notificationChannel);
        }

    }

    private void sendReconnectNotification(Context context, int notif_id){

        Intent boardIntent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, boardIntent, 0);
        /*Create a notification channel for Android 8.0 and above*/
        createNotificationChannel(context);

        /*Notifications built*/
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_logo_notification)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(preferences.getUserName() + context.getString(R.string.notification_comeback_message))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setStyle(new NotificationCompat.BigTextStyle())
                .setColor(context.getResources().getColor(R.color.colorGold))
                .setColorized(true)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        /*Notifications sent*/
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        notificationManagerCompat.notify(notif_id, builder.build());
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        //throw new UnsupportedOperationException("Not yet implemented");
        preferences = new KatikaSharedPreferences(context);

        /*NotificationManager notificationManager = (NotificationManager)context.getSystemService(NOTIFICATION_SERVICE);


        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Project Base";
            String description = "A test of notifications";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_ID, name, importance);
            notificationChannel.setDescription(description);
            notificationChannel.enableLights(true);

            notificationManager.createNotificationChannel(notificationChannel);
        }

        Log.d(TAG, "onReceive: try to send notifcation");
        Notification notification = intent.getParcelableExtra(NOTIFICATION); */
        Log.d(TAG, "onReceive: try to send notifcation");
        int id = intent.getIntExtra(NOTIFICATION_ID, 0);
        //notificationManager.notify(id, notification);
        sendReconnectNotification(context, id);

    }
}
