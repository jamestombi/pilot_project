package com.katikatech.james.pilot_project.contactMessages;

public class AssistantMessage extends Message {
    private String text; // message body

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    public AssistantMessage(String text) {
        this.text = text;
    }

    public AssistantMessage(String name, String text) {
        this.name = name;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public int getMessageOwner() {
        return ASSISTANT_MESSAGE;
    }
}
