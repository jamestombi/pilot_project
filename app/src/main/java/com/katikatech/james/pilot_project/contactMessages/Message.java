package com.katikatech.james.pilot_project.contactMessages;

public abstract class Message {
    public static final int USER_MESSAGE = 0;
    public static final int ASSISTANT_MESSAGE = 1;
    public static final int DATE_INFORMATION = 2;

    abstract public int getMessageOwner();
}
