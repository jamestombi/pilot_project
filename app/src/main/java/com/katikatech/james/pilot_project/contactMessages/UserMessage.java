package com.katikatech.james.pilot_project.contactMessages;

public class UserMessage extends Message {
    private String text; // message body

    public UserMessage(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    @Override
    public int getMessageOwner() {
        return USER_MESSAGE;
    }
}

