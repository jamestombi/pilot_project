package com.katikatech.james.pilot_project.obsolete;

import android.content.Context;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.katikatech.james.pilot_project.Operation;
import com.katikatech.james.pilot_project.R;

import java.util.List;

public class ListViewAdapter extends ArrayAdapter<Operation> {
    public ListViewAdapter(@NonNull Context context, int resource, @NonNull List<Operation> objects) {
        super(context, resource, objects);
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;

        if(null == view) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_layout, null);
        }

        Operation operation = getItem(position);
        ImageView img = view.findViewById(R.id.imageView);
        TextView txtTitle = view.findViewById(R.id.txtTitle);
        TextView txtDescription = view.findViewById(R.id.txtDescription);
        TextView txtAmount = view.findViewById(R.id.txtAmount);

        img.setImageResource(R.drawable.dollar_sign);

        //Truncate in description printing too long descriptions
        String myDescription;
        String myAmount;
        if (operation.getDescription().length() > 15) {
            myDescription = operation.getDescription().substring(0, 15)+"...";
        } else {
            myDescription = operation.getDescription();
        }

        if (operation.getAmount() > 0) {
            txtAmount.setTextColor(Color.rgb(24, 240, 50));
        } else if (operation.getAmount() <= 0) {
            txtAmount.setTextColor(Color.rgb(0, 0, 0));
        }

        if (operation.getAmount() > 1000000.0 || operation.getAmount() < -1000000.0) {
            myAmount = (""+(operation.getAmount()/1000000.0)).substring(0,5)+"M";
        } else {
            myAmount = ""+operation.getAmount();
        }

        if (position%2 == 1) {
            view.setBackgroundColor(Color.rgb(240, 240, 240));
        }
        else if (position%2 == 0){
            view.setBackgroundColor(Color.rgb(255, 255, 255));
        }
        txtTitle.setText(myDescription);
        txtDescription.setText(operation.getInsertedAt());
        txtAmount.setText(myAmount+"€");

        return view;
    }


    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }


}
