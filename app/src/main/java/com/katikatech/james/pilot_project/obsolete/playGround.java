package com.katikatech.james.pilot_project.obsolete;

import android.content.ClipData;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.katikatech.james.pilot_project.R;
import com.katikatech.james.pilot_project.UI.InsertOperationActivity;

public class playGround extends AppCompatActivity {

    ImageView image1;
    ImageView image2;
    ImageView image3;

    LinearLayout tableSplash;
    LinearLayout tablePlayer1;
    LinearLayout tablePlayer2;
    TextView splashTex;

    Button firstButton;

    int windowwidth;
    int windowheight;
    int limite;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_ground);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        image1 = (ImageView) findViewById(R.id.card1);
        image2 = (ImageView) findViewById(R.id.card2);
        image3 = (ImageView) findViewById(R.id.card1_2);
        splashTex = (TextView) findViewById(R.id.DropText);
        firstButton = (Button) findViewById(R.id.firstButton);

        windowwidth = getWindowManager().getDefaultDisplay().getWidth();
        windowheight = getWindowManager().getDefaultDisplay().getHeight();

        tableSplash = (LinearLayout) findViewById(R.id.splash_table);
        tablePlayer1 = (LinearLayout) findViewById(R.id.player_1);

        image1.setOnLongClickListener(longClickListenerEvent);
        image2.setOnLongClickListener(longClickListenerEvent);
        image3.setOnLongClickListener(longClickListenerEvent);

        firstButton.setOnClickListener(Clicker);

        tableSplash.setOnDragListener(DragListenerEvent);
        tablePlayer1.setOnDragListener(DragListenerEvent);
    }


    //Listener of Long Click event
    View.OnLongClickListener longClickListenerEvent = new View.OnLongClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public boolean onLongClick(View view) {
            ClipData myClipData = ClipData.newPlainText("","");
            View.DragShadowBuilder myShadowBuilder = new View.DragShadowBuilder(view);

            //call for method which makes drag and drop
            view.startDrag(myClipData, myShadowBuilder, view, 0);
            return true;
        }
    };


    //Listener of Drag and drop event
    View.OnDragListener DragListenerEvent = new View.OnDragListener() {

        @Override
        public boolean onDrag(View view, DragEvent dragEvent) {

            int myDragEvent = dragEvent.getAction();

            switch (myDragEvent){

                case DragEvent.ACTION_DRAG_ENTERED:
                    final View view1 = (View) dragEvent.getLocalState();

                    if (view1.getId() == R.id.card1){

                        splashTex.setText("Card 1 is entering!");
                    }
                    else if (view1.getId() == R.id.card2){

                        splashTex.setText("Card 2 is entering!");

                    }
                    else if (view1.getId() == R.id.card1_2){

                        splashTex.setText("Card 3 is entering!");

                    }
                    break;

                case DragEvent.ACTION_DRAG_EXITED:
                    splashTex.setText("");
                    break;

                case DragEvent.ACTION_DROP:
                    final View view2 = (View) dragEvent.getLocalState();


                    if (view2.getId() == R.id.card1){

                        //traitement du cas de la carte
                        if (limite < 10) {
                            splashTex.setText("Card 1 is dropped!");

                            //get the old parent of the view being dragged
                            LinearLayout oldParent = (LinearLayout) view2.getParent();
                            //remove the dragged view from its old parent
                            oldParent.removeView(view2);
                            //create a new parent based on the view where to drop
                            LinearLayout newParent = (LinearLayout) view;
                            //add the dragged view in the new parent
                            newParent.addView(view2);
                        } else {
                            splashTex.setText("Limite is attended!");
                        }
                    }
                    else if (view2.getId() == R.id.card2){

                        limite ++;
                        splashTex.setText("Card 2 is dropped!");

                    }
                    else if (view2.getId() == R.id.card1_2){

                        //traitement du cas de la carte
                        if (limite < 10) {
                            splashTex.setText("Card 3 is dropped!");

                            //get the old parent of the view being dragged
                            LinearLayout oldParent = (LinearLayout) view2.getParent();
                            //remove the dragged view from its old parent
                            oldParent.removeView(view2);
                            //create a new parent based on the view where to drop
                            LinearLayout newParent = (LinearLayout) view;
                            //add the dragged view in the new parent
                            newParent.addView(view2);
                        } else {
                            splashTex.setText("Limite is attended!");
                        }
                    }
                    break;

            }
            return true;
        }
    };

    Button.OnClickListener Clicker = new Button.OnClickListener(){


        @Override
        public void onClick(View view) {
            Intent homeIntent = new Intent(playGround.this, InsertOperationActivity.class);
            startActivity(homeIntent);
            finish();
        }
    };

}
