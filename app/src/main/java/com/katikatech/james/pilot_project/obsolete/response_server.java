package com.katikatech.james.pilot_project.obsolete;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.AsyncTask;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.katikatech.james.pilot_project.Operation;
import com.katikatech.james.pilot_project.OperationRepository;
import com.katikatech.james.pilot_project.R;
import com.katikatech.james.pilot_project.UI.InsertOperationActivity;
import com.katikatech.james.pilot_project.UI.detailedTransactions;

import java.util.List;

public class response_server extends AppCompatActivity {

    LinearLayout resultLayout;
    TextView myResponseView;
    TextView myResponseView2;
    Button slideButton;
    Button closeMenu;
    ImageView userImage;
    RelativeLayout myRelativeLayout;
    FloatingActionButton myFloatingButtonAdd;
    ProgressDialog dialog;
    ListView myListView;
    ListViewAdapter myListViewAdapter;
    ScrollView myScrollView;
    ViewStub myViewStub;
    NavigationView myNavigator;
    ListViewAdapter listViewAdapter;
    List<Operation> externalOperationList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_response_server);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        String user;
        Integer ageGamer;


        myResponseView = (TextView) findViewById(R.id.myResponse);
        myResponseView2 = (TextView) findViewById(R.id.myResponse2);
        userImage = (ImageView) findViewById(R.id.UserImage);
        myRelativeLayout = (RelativeLayout) findViewById(R.id.MyRelativeLayout);
        myFloatingButtonAdd = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        myViewStub = (ViewStub) findViewById(R.id.StubList);
        dialog = new ProgressDialog(this);
        slideButton = (Button) findViewById(R.id.menu);

        //inflate the subView before getting the listView
        myViewStub.inflate();

        //Get the ListView
        myListView = (ListView) findViewById(R.id.myListView);

        //Call List of operation from database and prepare action like click action
        listOperation();

        try {
            user = getIntent().getExtras().getString("gamer");
            ageGamer = getIntent().getExtras().getInt("age_gamer");
        } catch (NullPointerException excp) {
            user = "Basic";
            ageGamer = 1;
        }

        //Preparer le clic sur le bouton
        slideButton.setOnClickListener(Apropos);
        myFloatingButtonAdd.setOnClickListener(SubmissionListener2);

    }

    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(response_server.this);
        builder.setMessage("Voulez vous sortir?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        dialog.dismiss();
                        Log.d("JTO", "sortie");
                    }
                }).setNegativeButton("Annuler", null);

        AlertDialog alert = builder.create();
        alert.show();
    }

    Button.OnClickListener SubmissionListener2 = new Button.OnClickListener() {


        @Override
        public void onClick(View view) {
            Intent homeIntent = new Intent(response_server.this, InsertOperationActivity.class);
            startActivity(homeIntent);
            finish();

        }

    };


    Button.OnClickListener Apropos = new Button.OnClickListener() {


        @Override
        public void onClick(View view) {
            //Intent homeIntent = new Intent(response_server.this, slidePage.class);
            //myNavigator.setVisibility(View.VISIBLE);
            Intent homeIntent = new Intent(response_server.this, detailedTransactions.class);
            startActivity(homeIntent);
            finish();

        }

    };



    public void listOperation() {

        new AsyncTask<Void, Void, List<Operation>>() {

            @Override
            protected void onPostExecute(final List<Operation> operationList){
                dialog.dismiss();
                myResponseView.setGravity(View.TEXT_ALIGNMENT_CENTER);
                myResponseView.setText("You have a total of");
                try {
                    if (operationList.size() > 0) {
                        //Define the new ListViewAdapter with a defined layout
                        myListViewAdapter = new ListViewAdapter(response_server.this, R.layout.list_layout, operationList);
                        //adapt all the outputs of the list to the design of the new adapter
                        myListView.setAdapter(myListViewAdapter);
                        //set a click listener on every item of the list
                        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                //A toast to show values of item clicked
                                Toast.makeText(getApplicationContext(), "decription: "+operationList.get(position).getDescription()+" amount : "+ operationList.get(position).getAmount(), Toast.LENGTH_SHORT).show();
                            }
                        });

                        float sumCredit = 0;
                        // A list of TextView
                        for (int item = 0; item < operationList.size(); item++) {
                            sumCredit += operationList.get(item).getAmount();
                        }
                        if (sumCredit >= 0) {
                            myResponseView.setTextColor(Color.rgb(0, 255, 0)); //GREEN
                            myResponseView2.setTextColor(Color.rgb(0, 255, 0)); //GREEN
                        } else {
                            myResponseView.setTextColor(Color.rgb(200,50,50)); //RED
                            myResponseView2.setTextColor(Color.rgb(200,50,50)); //RED
                        }
                        myResponseView2.setGravity(View.TEXT_ALIGNMENT_CENTER);
                        myResponseView2.setText(""+ sumCredit+"€");

                    }
                } catch (NullPointerException excp) {
                    Log.d("JTO Log: ", excp.toString());
                }
                return;
            }

            @Override
            protected List<Operation> doInBackground(Void... voids) {

                //access to database in asyncTask (SELECT operation)
                OperationRepository operationRepository = new
                        OperationRepository(getApplication());
                List <Operation> myOperationList = operationRepository.listOperations();
                return myOperationList;

            }

            @Override
            protected void onPreExecute(){
                // a progress dialog
                dialog = ProgressDialog.show(response_server.this, "",
                        "Loading Data. Please wait...", true);
                return;
            }
        }.execute();
    }

}
