package com.katikatech.james.pilot_project.recyclerview;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.katikatech.james.pilot_project.Category;
import com.katikatech.james.pilot_project.SharedPreferences.CurrentOperationSharedPreferences;
import com.katikatech.james.pilot_project.UI.DetailedOperationActivity;
import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.R;

import java.util.List;

import static com.katikatech.james.pilot_project.UI.CategoryActivity.TAG;

public class CategorySelectionRecyclerView extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Category> categories;
    private KatikaSharedPreferences preferences;
    private CurrentOperationSharedPreferences operationSharedPreferences;

    public CategorySelectionRecyclerView(List<Category> myCategories){

        categories = myCategories;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, int i) {
        final View view;
        LayoutInflater inflater;
        preferences = new KatikaSharedPreferences(viewGroup.getContext());
        operationSharedPreferences = new CurrentOperationSharedPreferences(viewGroup.getContext());

        final CategoryViewHolder categoryViewHolder;
        inflater = LayoutInflater.from(viewGroup.getContext());
        view = inflater.inflate(R.layout.category_layout, viewGroup, false);
        categoryViewHolder = new CategoryViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = categoryViewHolder.getAdapterPosition();
                //A toast to show values of item clicked
                Category aCategory =  categories.get(position);
                Toast.makeText(viewGroup.getContext(), "description: " +
                        aCategory.getTitle() + " id : " +
                        aCategory.getId(), Toast.LENGTH_SHORT).show();

                Log.d(TAG, "description: " +
                        aCategory.getTitle() + " id : " +
                        aCategory.getId() );

                operationSharedPreferences.setOperationCategory(aCategory.getId());
                preferences.setAccountConnected(true);
                Intent detailsOperationIntent = new Intent(viewGroup.getContext(), DetailedOperationActivity.class);
                detailsOperationIntent.putExtra("categorieTitle", aCategory.getTitle());
                detailsOperationIntent.putExtra("categoryID", aCategory.getId());
                viewGroup.getContext().startActivity(detailsOperationIntent);
                ((Activity)viewGroup.getContext()).finish();
            }
        });

        return categoryViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        int type = getItemViewType(i);

        Category aCategory =  categories.get(i);

        int imageResource;

        switch (i){
            case 1:
                imageResource = R.drawable.revenus_divers;
                break;
            case 2:
                imageResource = R.drawable.depenses_divers;
                break;
            case 3:
                imageResource = R.drawable.alimentation;
                break;
            case 4:
                imageResource = R.drawable.transport;
                break;
            case 5:
                imageResource = R.drawable.abonnement;
                break;
            case 6:
                imageResource = R.drawable.loyer;
                break;
            case 7:
                imageResource = R.drawable.allocations;
                break;
            case 8:
                imageResource = R.drawable.impots_taxes;
                break;
            default:
                imageResource = R.drawable.dollar_sign;
                break;
        }

        CategoryViewHolder myViewHolder = (CategoryViewHolder) viewHolder;
        myViewHolder.img.setImageResource(imageResource);
        myViewHolder.txtDescription.setText(aCategory.getTitle());


    }

    @Override
    public int getItemCount() {
        return categories.size();
    }


    public class CategoryViewHolder extends RecyclerView.ViewHolder {


        public View layout;

        //think about private attribute and encapsulation here
        public ImageView img;
        public TextView txtDescription;

        public CategoryViewHolder (View v){
            super(v);
            layout = v;
            img = layout.findViewById(R.id.categoryImageLayout);
            txtDescription = layout.findViewById(R.id.categoryTitleLayout);

        }
    }

}
