package com.katikatech.james.pilot_project.recyclerview;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.katikatech.james.pilot_project.transactionElements.DateItem;
import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.Operation;
import com.katikatech.james.pilot_project.transactionElements.OperationAccountItem;
import com.katikatech.james.pilot_project.R;
import com.katikatech.james.pilot_project.transactionElements.TransactionItem;
import com.katikatech.james.pilot_project.UI.DetailedOperationActivity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import static com.katikatech.james.pilot_project.UI.detailedTransactions.TAG;

public class DetailedAccountRecyclerView extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public List<TransactionItem> transactionItemList;
    public List<Operation> operations;
    private String currentDevise;
    private KatikaSharedPreferences preferences;


    public DetailedAccountRecyclerView(List<Operation> myOperations){
        transactionItemList = new ArrayList<TransactionItem>();
        LinkedHashMap<String, List<Operation>> mDateOperationMapping =
                new LinkedHashMap<String, List<Operation>>();

        operations = myOperations;
        transactionItemList.add(new ReferenceItem("Description",
                "P.U",
                "Qté",
                "Montant"));

        /* When you get a new operation, you check if the its insertion date is already
        * in the retrieved dates, if not, you add it in the hashMap with the corresponding operation
        * */
        for (int i = 0; i < operations.size(); i ++){
            List<Operation> currentListInDate =
                    mDateOperationMapping.get(operations.get(i).getInsertedAt());
            if(currentListInDate == null) {
                List<Operation> newOperationList = new ArrayList<Operation>();
                newOperationList.add(operations.get(i));
                mDateOperationMapping.put(operations.get(i).getInsertedAt(), newOperationList);
            } else {
                currentListInDate.add(operations.get(i));
                mDateOperationMapping.put(operations.get(i).getInsertedAt(), currentListInDate);
            }
        }

        /* for every key (date insertion) add the date in the transaction list
        * For every values of keys (operation) add in order in the List*/
        for (String dateItem : mDateOperationMapping.keySet()) {
            DateItem mDateItem = new DateItem(dateItem);
            transactionItemList.add(mDateItem);

            for (Operation mOperation : mDateOperationMapping.get(dateItem)){
                OperationAccountItem mOperationItem = new OperationAccountItem(mOperation);
                transactionItemList.add(mOperationItem);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {

        return transactionItemList.get(position).getItemType();
    }


    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        final View view;
        LayoutInflater inflater;
        preferences = new KatikaSharedPreferences(parent.getContext());
        //currentDevise = parent.getContext().getApplicationContext().getString(R.string.devise);
        currentDevise = new KatikaSharedPreferences(parent.getContext()).getUserCurrencySymbol();

        //Create a new view
        if(viewType == TransactionItem.ACCOUNT_ITEM) {
            final OperationAccountViewHolder operationAccountViewHolder;
            inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.account_view_list_layout, parent, false);
            //we can choose at will here the ViewHolder we want to inflate
            operationAccountViewHolder = new OperationAccountViewHolder(view);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = operationAccountViewHolder.getAdapterPosition();
                    //int newPosition = position - positionCorrection;
                    //String item = .get(itemPosition);
                    //A toast to show values of item clicked
                    OperationAccountItem insideOperation = (OperationAccountItem) transactionItemList.get(position);
                    //Toast.makeText(parent.getContext(), "description: " +
                    //        insideOperation.operation.getDescription() + " amount : " +
                    //        insideOperation.operation.getAmount(), Toast.LENGTH_SHORT).show();

                    preferences.setAccountConnected(true);
                    Intent respo_serv = new Intent(parent.getContext(), DetailedOperationActivity.class);
                    respo_serv.putExtra("description", insideOperation.operation.getDescription());
                    respo_serv.putExtra("amount", insideOperation.operation.getAmount());
                    respo_serv.putExtra("insertion", insideOperation.operation.getInsertedAt());
                    respo_serv.putExtra("remote_key", insideOperation.operation.getRemoteKey());
                    respo_serv.putExtra("categorieTitle", "Catégorie");
                    parent.getContext().startActivity(respo_serv);
                    ((Activity)parent.getContext()).finish();
                }
            });

            return operationAccountViewHolder;

        } else if (viewType == TransactionItem.DATE_ITEM){

            //positionCorrection ++;
            final DateViewHolder dateViewHolder;
            inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.date_group_layout, parent, false);
            dateViewHolder = new DateViewHolder(view);

            return dateViewHolder;

        } else if (viewType == TransactionItem.REFERENCE_ITEM){

            final OperationAccountViewHolder operationAccountViewHolder;
            inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.account_view_list_layout, parent, false);
            //we can choose at will here the ViewHolder we want to inflate
            operationAccountViewHolder = new OperationAccountViewHolder(view);
            return operationAccountViewHolder;

    }
        return null;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        int type = getItemViewType(position);

        if(type == TransactionItem.ACCOUNT_ITEM) {

            OperationAccountItem operationItem = (OperationAccountItem) transactionItemList.get(position);
            Operation operation = operationItem.operation;


            OperationAccountViewHolder myViewHolder = (OperationAccountViewHolder) holder;


            //Truncate if description printing too long
            String myDescription;
            String myAmount;
            if (operation.getDescription().length() > 15) {
                myDescription = operation.getDescription().substring(0, 15) + "...";
            } else {
                myDescription = operation.getDescription();
            }

            myViewHolder.txtDescription.setText(myDescription);

            //Define the color of the operation
            if (operation.getAmount() > 0) {
                myViewHolder.txtAmount.setTextColor(Color.rgb(50, 200, 50)); //Income

            } else if (operation.getAmount() <= 0) { //expenses
                myViewHolder.txtAmount.setTextColor(Color.rgb(0, 0, 0));
            }

            //Truncate the amount if it is above 1 million
            if (operation.getAmount() > 1000000.0 || operation.getAmount() < -1000000.0) {
                myAmount = ("" + (operation.getAmount() / 1000000.0)).substring(0, 3) + "M";
            } else {
                myAmount = "" + operation.getAmount();
            }

            if(operation.getNumberItems() >= 1000.0) {
               //String myQuantity = ("" + (operation.getNumberItems() / 1000.0)).substring(0,3) + "K";
                String myQuantity = (""+(int)(operation.getNumberItems() / 1000.0)) +
                        "." + (""+Math.abs(operation.getNumberItems() % 1000)).substring(0,2) + "K";
                myViewHolder.txtQuantity.setText(myQuantity);
            } else {
                myViewHolder.txtQuantity.setText("" + operation.getNumberItems());
            }

            //Define the unit price
            float unitPrice = operation.getAmount()/operation.getNumberItems();
            String myUnitPrice ="";
            if (Math.abs(unitPrice) >= 1000.0){
                //myUnitPrice = ("" + (unitPrice / 1000.0)) + "K";
                myUnitPrice = (""+(int)(unitPrice / 1000.0)) +
                        "." + (""+Math.abs(unitPrice % 1000)).substring(0,2) + "K";
            } else if (Math.abs(unitPrice) >= 1000000.0){
                //myUnitPrice = ("" + (unitPrice / 1000.0)) + "K";
                myUnitPrice = (""+(int)(unitPrice / 1000000.0)) +
                        "." + (""+Math.abs(unitPrice % 1000000)).substring(0,2) + "M";
            } else {
                myUnitPrice = ""+(double) Math.round(unitPrice * 100) / 100;
            }
            myViewHolder.txtUnitPrice.setText(myUnitPrice);

            myViewHolder.txtAmount.setText(myAmount);


        } else if (type == TransactionItem.DATE_ITEM){
            DateItem dateItem = (DateItem) transactionItemList.get(position);

            DateViewHolder dateViewHolder = (DateViewHolder) holder;

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.FRANCE);
            DateFormat formattedDate =
                    new SimpleDateFormat("EEEE, dd MMMM yyyy", Locale.FRANCE);
            try {
                //Only to take in today three main informations
                Date today = dateFormat.parse(dateFormat.format(new Date()));
                Date currentDate = dateFormat.parse(dateItem.date);
                if(currentDate.compareTo(today) == 0 ){
                    //dateViewHolder.dateGroup.setText("Aujourd'hui");
                    dateViewHolder.dateGroup.setText(
                            dateViewHolder.layout
                                    .getContext()
                                    .getString(R.string.today_string)
                    );
                } else {
                    dateViewHolder.dateGroup.setText(formattedDate.format(currentDate));
                }
            } catch (ParseException e) {
                Log.d(TAG,e.getMessage());
                dateViewHolder.dateGroup.setText(dateItem.date);
            }
        } else if (type == TransactionItem.REFERENCE_ITEM){
            ReferenceItem referenceItem = (ReferenceItem) transactionItemList.get(position);
            OperationAccountViewHolder myViewHolder = (OperationAccountViewHolder) holder;
            myViewHolder.txtDescription.setText(referenceItem.txtDescription);
            myViewHolder.txtQuantity.setText(referenceItem.txtQuantity);
            myViewHolder.txtUnitPrice.setText(referenceItem.txtUnitPrice);
            myViewHolder.txtAmount.setText(referenceItem.txtAmount);
            myViewHolder.layout.setBackgroundColor(0xFFCCFF00);
        }
    }

    @Override
    public int getItemCount() {
        return transactionItemList.size();
    }


    //Class holding the view of one item of the recycler view
    //Can we create multiple viewholder classes in order to present different
    //representations of item in e single activity or fragment
    public class OperationAccountViewHolder extends RecyclerView.ViewHolder {


        public View layout;

        //think about private attribute and encapsulation here
        public TextView txtDescription;
        public TextView txtQuantity;
        public TextView txtUnitPrice;
        public TextView txtAmount;

        public OperationAccountViewHolder (View v){
            super(v);
            layout = v;
            txtDescription = layout.findViewById(R.id.description_operation);
            txtQuantity = layout.findViewById(R.id.quantity_operation);
            txtUnitPrice = layout.findViewById(R.id.unit_amount_operation_in);
            txtAmount = layout.findViewById(R.id.amount_operation_in);

        }
    }

    public class DateViewHolder extends RecyclerView.ViewHolder {


        public View layout;

        //think about private attribute and encapsulation here
        public TextView dateGroup;

        public DateViewHolder (View v){
            super(v);
            layout = v;
            dateGroup = layout.findViewById(R.id.date_group);
        }
    }

    public class ReferenceItem extends TransactionItem {

        public String txtDescription;
        public String txtQuantity;
        public String txtUnitPrice;
        public String txtAmount;

        public ReferenceItem(String mDescription, String mUnitPrice, String mQuantity, String mAmount){
            txtDescription = mDescription;
            txtQuantity = mQuantity;
            txtUnitPrice = mUnitPrice;
            txtAmount = mAmount;
        }

        @Override
        public int getItemType() {
            return REFERENCE_ITEM;
        }
    }

}
