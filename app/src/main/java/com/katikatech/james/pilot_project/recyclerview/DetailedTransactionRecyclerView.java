package com.katikatech.james.pilot_project.recyclerview;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.katikatech.james.pilot_project.OperationRepository;
import com.katikatech.james.pilot_project.UI.UserBoard;
import com.katikatech.james.pilot_project.transactionElements.DateItem;
import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.Operation;
import com.katikatech.james.pilot_project.transactionElements.OperationItem;
import com.katikatech.james.pilot_project.R;
import com.katikatech.james.pilot_project.transactionElements.TransactionItem;
import com.katikatech.james.pilot_project.UI.DetailedOperationActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import static com.katikatech.james.pilot_project.UI.detailedTransactions.TAG;

public class DetailedTransactionRecyclerView extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public List<TransactionItem> transactionItemList;
    public List<Operation> operations;
    private String currentDevise;
    private KatikaSharedPreferences preferences;
    int elementSelected;
    List<Integer> selected;
    private ActionMode mActionMode;
    private SparseBooleanArray mSelectedItemsIds;
    private Context context;
    private Application application;
    DatabaseReference operationDatabase;


    public DetailedTransactionRecyclerView(Context inContext, Application mApplication, List<Operation> myOperations)
    {
        transactionItemList = new ArrayList<TransactionItem>();
        LinkedHashMap<String, List<Operation>> mDateOperationMapping =
                new LinkedHashMap<String, List<Operation>>();

        operations = myOperations;
        mSelectedItemsIds = new SparseBooleanArray();
        context = inContext;
        application = mApplication;
        elementSelected = 0;
        selected = new ArrayList<Integer>();

        // FIREBASE INITIALISATION
        operationDatabase = FirebaseDatabase.getInstance().getReference("Operations");

        /* When you get a new operation, you check if the its insertion date is already
        * in the retrieved dates, if not, you add it in the hashMap with the corresponding operation
        * */
        for (int i = 0; i < operations.size(); i ++){
            List<Operation> currentListInDate =
                    mDateOperationMapping.get(operations.get(i).getInsertedAt());
            if(currentListInDate == null) {
                List<Operation> newOperationList = new ArrayList<Operation>();
                newOperationList.add(operations.get(i));
                mDateOperationMapping.put(operations.get(i).getInsertedAt(), newOperationList);
            } else {
                currentListInDate.add(operations.get(i));
                mDateOperationMapping.put(operations.get(i).getInsertedAt(), currentListInDate);
            }
        }

        /* for every key (date insertion) add the date in the transaction list
        * For every values of keys (operation) add in order in the List*/
        for (String dateItem : mDateOperationMapping.keySet()) {
            DateItem mDateItem = new DateItem(dateItem);
            transactionItemList.add(mDateItem);

            for (Operation mOperation : mDateOperationMapping.get(dateItem)){
                OperationItem mOperationItem = new OperationItem(mOperation);
                transactionItemList.add(mOperationItem);
            }
        }
    }

    @Override
    public int getItemViewType(int position)
    {
        return transactionItemList.get(position).getItemType();
    }


    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType)
    {
        final View view;
        LayoutInflater inflater;
        preferences = new KatikaSharedPreferences(parent.getContext());
        currentDevise = new KatikaSharedPreferences(parent.getContext()).getUserCurrencySymbol();

        //Create a new view
        if(viewType == TransactionItem.OPERATION_ITEM) {
            final OperationViewHolder operationViewHolder;
            inflater = LayoutInflater.from(parent.getContext());
            //we can choose at will here the ViewHolder we want to inflate
            view = inflater.inflate(R.layout.list_layout, parent, false);

            operationViewHolder = new OperationViewHolder(view);

            //Click event:
            //If just a click go to details
            // if click after a previous long click,
            // if operation not selected, select it, if not unselect
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = operationViewHolder.getAdapterPosition();

                    //itemSelected == 0 ==> Just transiting to details on operations
                    //Log.d(TAG, "Click event, element selected  = " + elementSelected + ", size = " + selected.size());
                    //possible future optimisation here, instead of working with integers,
                    // work with boolean, to see if the element is already in the toggle selection
                    if(elementSelected == 0) {
                        //A toast to show values of item clicked
                        OperationItem insideOperation = (OperationItem) transactionItemList.get(position);

                        preferences.setAccountConnected(true);
                        Intent detailedOperationsIntent = new Intent(parent.getContext(), DetailedOperationActivity.class);
                        detailedOperationsIntent.putExtra("description", insideOperation.operation.getDescription());
                        detailedOperationsIntent.putExtra("amount", insideOperation.operation.getAmount());
                        detailedOperationsIntent.putExtra("insertion", insideOperation.operation.getInsertedAt());
                        detailedOperationsIntent.putExtra("remote_key", insideOperation.operation.getRemoteKey());
                        detailedOperationsIntent.putExtra("categorieTitle", "Catégorie");
                        parent.getContext().startActivity(detailedOperationsIntent);
                        ((Activity) parent.getContext()).finish();
                    } else {
                        //Mode multiSelect
                        toggleSelection(position);

                        if (!selected.contains((Integer) position)) {

                            /* Add the element in the list of selected object */
                            selected.add(position);
                            /* Increment the number of selected element */
                            elementSelected++;
                            mActionMode.setTitle(elementSelected + " Selected");
                        } else {

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                view.setBackground(parent.getContext().getDrawable(R.drawable.custom_ripple_click));
                            }

                            /* Remove the current selected element from the selected list */
                            selected.remove(Integer.valueOf(position));

                            Toast.makeText(parent.getContext(),
                                    "position: " + position + ", taille: " + selected.size(),
                                    Toast.LENGTH_SHORT).show();

                            /* Decrement the list of of selected element */
                            elementSelected--;
                            //Log.d(TAG, "element selected  = " + elementSelected + ", size = " + selected.size());
                            mActionMode.setTitle(elementSelected + " Selected");
                            //Clear the Contextual Menu if no more items selected
                            if (elementSelected == 0){
                                if (mActionMode != null) {
                                    mActionMode.finish();
                                }
                            }
                        }
                    }
                }
            });

            //Long click: Work only if no long click made before
            //if long click ok, select the operation
            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if(mActionMode != null) {
                        return false;
                    }
                    //No element have been selected in long click yet
                    if(elementSelected < 1) {
                        mActionMode = ((AppCompatActivity)view.getContext())
                                .startSupportActionMode(new ToolBarActionCallback(view.getContext()));
                        int position = operationViewHolder.getAdapterPosition();
                        toggleSelection(position);
                        elementSelected = 1;
                        selected.add(position);
                        //Log.d(TAG, "element selected  = " + elementSelected + ", size = " + selected.size());
                        mActionMode.setTitle(elementSelected + " Selected");
                        return true;
                    }
                    return false;
                }
            });

            return operationViewHolder;

        } else if (viewType == TransactionItem.DATE_ITEM){

            final DateViewHolder dateViewHolder;
            inflater = LayoutInflater.from(parent.getContext());
            //inflate the date layout
            view = inflater.inflate(R.layout.date_group_layout, parent, false);
            dateViewHolder = new DateViewHolder(view);

            return dateViewHolder;

        }
        return null;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {

        int type = getItemViewType(position);

        if(type == TransactionItem.OPERATION_ITEM) {

            OperationItem operationItem = (OperationItem) transactionItemList.get(position);
            Operation operation = operationItem.operation;

            int imageResource;

            switch (operation.getIdOperationCategory()){
                case 1:
                    imageResource = R.drawable.revenus_divers;
                    break;
                case 2:
                    imageResource = R.drawable.depenses_divers;
                    break;
                case 3:
                    imageResource = R.drawable.alimentation;
                    break;
                case 4:
                    imageResource = R.drawable.transport;
                    break;
                case 5:
                    imageResource = R.drawable.abonnement;
                    break;
                case 6:
                    imageResource = R.drawable.loyer;
                    break;
                case 7:
                    imageResource = R.drawable.allocations;
                    break;
                case 8:
                    imageResource = R.drawable.impots_taxes;
                    break;
                default:
                    imageResource = R.drawable.dollar_sign;
                    break;
            }

            OperationViewHolder myViewHolder = (OperationViewHolder) holder;
            myViewHolder.img.setImageResource(imageResource);

            //Truncate if description printing too long
            String myDescription;
            String myAmount;
            if (operation.getDescription().length() > 15) {
                myDescription = operation.getDescription().substring(0, 15) + "...";
            } else {
                myDescription = operation.getDescription();
            }

            //Define the color of the operation
            if (operation.getAmount() > 0) {
                myViewHolder.txtAmount.setTextColor(Color.rgb(50, 200, 50)); //Income

            } else if (operation.getAmount() <= 0) { //expenses
                myViewHolder.txtAmount.setTextColor(Color.rgb(0, 0, 0));
            }

            //Define the category
            myViewHolder.txtDescription.setText(
                    KatikaSharedPreferences.KatikaOPerationCategories
                            .get(operation.getIdOperationCategory() - 1)
            );

            //Truncate the amount if it is above 1 million
            if (operation.getAmount() > 1000000.0 || operation.getAmount() < -1000000.0) {
                myAmount = ("" + (operation.getAmount() / 1000000.0)).substring(0, 3) + "M";
            } else {
                myAmount = "" + operation.getAmount();
            }

            myViewHolder.txtTitle.setText(myDescription);

            String completeAmount = myAmount + " " +currentDevise;

            myViewHolder.txtAmount.setText(completeAmount);

            //Set Background color
            if(mSelectedItemsIds.get(position)){
                myViewHolder.itemView.setBackgroundColor(0X44A0A0A0);
            } else {
                myViewHolder.itemView.setBackgroundColor(0);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    myViewHolder.itemView.setBackground(context.getDrawable(R.drawable.custom_ripple_click));
                }
            }

        } else if (type == TransactionItem.DATE_ITEM){
            DateItem dateItem = (DateItem) transactionItemList.get(position);

            DateViewHolder dateViewHolder = (DateViewHolder) holder;

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.FRANCE);
            DateFormat formattedDate =
                    new SimpleDateFormat("EEEE, dd MMMM yyyy", Locale.FRANCE);
            try {
                //Only to take in today three main informations
                Date today = dateFormat.parse(dateFormat.format(new Date()));
                Date currentDate = dateFormat.parse(dateItem.date);
                if(currentDate.compareTo(today) == 0 ){
                    dateViewHolder.dateGroup.setText(
                            dateViewHolder.layout
                                    .getContext()
                                    .getString(R.string.today_string)
                    );
                } else {
                    dateViewHolder.dateGroup.setText(formattedDate.format(currentDate));
                }
            } catch (ParseException e) {
                Log.d(TAG,e.getMessage());
                dateViewHolder.dateGroup.setText(dateItem.date);
            }
        }
    }

    @Override
    public int getItemCount()
    {
        return transactionItemList.size();
    }


    //Class holding the view of one operation item of the recycler view
    //Can we create multiple viewholder classes in order to present different
    //representations of item in e single activity or fragment
    public class OperationViewHolder extends RecyclerView.ViewHolder
    {

        public View layout;

        //think about private attribute and encapsulation here
        public ImageView img;
        public TextView txtTitle;
        public TextView txtDescription;
        public TextView txtAmount;

        public OperationViewHolder (View v){
            super(v);
            layout = v;
            img = layout.findViewById(R.id.imageView);
            txtTitle = layout.findViewById(R.id.txtTitle);
            txtDescription = layout.findViewById(R.id.txtDescription);
            txtAmount = layout.findViewById(R.id.txtAmount);

        }
    }

    //Class holding the view of one date grouper item of the recycler view
    //When multiple operations are done the same day, the date view is the delimiter
    public class DateViewHolder extends RecyclerView.ViewHolder
    {
        public View layout;

        //think about private attribute and encapsulation here
        public TextView dateGroup;

        public DateViewHolder (View v){
            super(v);
            layout = v;
            dateGroup = layout.findViewById(R.id.date_group);
        }
    }

    //ActionMode CallBack for new Menu
    public class ToolBarActionCallback implements ActionMode.Callback {

        private Context context;

        public ToolBarActionCallback(Context mContext){
            this.context = mContext;
        }
        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            actionMode.getMenuInflater().inflate(R.menu.menu_operation_selected, menu);
            //actionMode.setTitle(mSelectedItemsIds.size() + " Selected");

            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            int id = menuItem.getItemId();
            preferences.setAccountConnected(true);

            //If Statement on Item of Contextual Action Bar
            if (id == R.id.action_delete) {
                //Log.d(TAG, "action delete");
                OperationItem insideOperation;
                for(int i = selected.size() - 1; i >= 0; i--) {
                    //Toast.makeText(context, "to delete " + selected.get(i), Toast.LENGTH_LONG).show();
                    //Log.d(TAG, "selected ===> " + selected.get(i));
                    insideOperation = (OperationItem) transactionItemList.get(selected.get(i));
                    //Toast.makeText(context,
                    //        "delete this: description= " + insideOperation.operation.getDescription() +
                    //                ", amount = " + insideOperation.operation.getAmount() +
                    //        ", id = " + insideOperation.operation.getId(),
                    //        Toast.LENGTH_SHORT).show();
                    new OperationRepository(application).hideOperation(context, insideOperation.operation);
                    transactionItemList.remove(selected.get(i));

                    //distant modification
                    Operation myCurrentOperation = insideOperation.operation;
                    String remoteKey = myCurrentOperation.getRemoteKey();
                    myCurrentOperation.setOperationVisible(false);

                    // Update data in remote database
                    if(remoteKey.length() != 0) {
                        operationDatabase.child(remoteKey).setValue(myCurrentOperation)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.d(TAG, "remote update success");
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {

                            }
                        });
                    } else {
                        remoteKey = preferences.getUserId()+"__"+insideOperation.operation.getId();
                        operationDatabase.child(remoteKey).setValue(myCurrentOperation)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.d(TAG, "remote update success");
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {

                            }
                        });
                    }
                }

                if(elementSelected > 1) {
                    Toast.makeText(context, elementSelected + " " + context.getResources().getString(R.string.elements_deleted), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, elementSelected + " " + context.getResources().getString(R.string.element_deleted), Toast.LENGTH_SHORT).show();
                }

                //We clear the selected List and finish contextual menu
                selected.clear();
                elementSelected = 0;
                notifyDataSetChanged();
                actionMode.finish();

                //After deletion, go back to userboard activity
                Intent userboardIntent = new Intent(context, UserBoard.class);
                preferences.setAccountConnected(true);
                context.startActivity(userboardIntent);
                ((Activity)context).finish();
                return true;


            }
            if (id == R.id.action_about) {
                actionMode.finish();
                return true;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            //Remove the selected elements in the list
            removeSelection();
            selected.clear();
            elementSelected = 0;
            actionMode.finish();
            mActionMode = null;
        }
    }

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }


    //Remove selected selections
    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }


    //Put or delete selected position into SparseBooleanArray
    public void selectView(int position, boolean value) {
        if (value) {
            mSelectedItemsIds.put(position, value);
        }
        else {
            mSelectedItemsIds.delete(position);
        }
        //Update data and view
        notifyDataSetChanged();
    }

    //Return all selected ids
    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

}
