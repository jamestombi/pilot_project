package com.katikatech.james.pilot_project.recyclerview;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.katikatech.james.pilot_project.contactMessages.AssistantMessage;
import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.contactMessages.Message;
import com.katikatech.james.pilot_project.R;
import com.katikatech.james.pilot_project.contactMessages.UserMessage;

import java.util.List;

public class MessageRecyclerView extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public List<Message> messages;
    private KatikaSharedPreferences preferences;


    public MessageRecyclerView(List<Message> myMessages){
        messages = myMessages;

    }

    public void add(Message message) {
        this.messages.add(message);
        notifyDataSetChanged(); // to render the list we need to notify
    }


    @Override
    public int getItemViewType(int position) {

        return messages.get(position).getMessageOwner();
    }



    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        final View view;
        LayoutInflater inflater;
        preferences = new KatikaSharedPreferences(parent.getContext());

        //Create a new view
        if(viewType == Message.USER_MESSAGE) {
            final UserMessageViewHolder myMessageViewHolder;
            inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.my_message, parent, false);
            //we can choose at will here the ViewHolder we want to inflate
            myMessageViewHolder = new UserMessageViewHolder(view);

            return myMessageViewHolder;

        } else if (viewType == Message.ASSISTANT_MESSAGE){
            final AssistantMessageViewHolder assistantMessageViewHolder;
            inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.assistant_message, parent, false);
            //we can choose at will here the ViewHolder we want to inflate
            assistantMessageViewHolder = new AssistantMessageViewHolder(view);

            return assistantMessageViewHolder;
        }
        return null;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        int type = getItemViewType(position);

        if(type == Message.USER_MESSAGE) {

            UserMessage myMessage = (UserMessage) messages.get(position);

            UserMessageViewHolder myViewHolder = (UserMessageViewHolder) holder;

            myViewHolder.messageBody.setText(myMessage.getText());


        } else if (type == Message.ASSISTANT_MESSAGE){
            AssistantMessage assistantMessage = (AssistantMessage) messages.get(position);

            AssistantMessageViewHolder assistantViewHolder = (AssistantMessageViewHolder) holder;

            assistantViewHolder.messageBody.setText(assistantMessage.getText());
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }


    //Class holding the view of one item of the recycler view
    //Can we create multiple viewholder classes in order to present different
    //representations of item in e single activity or fragment
    public class UserMessageViewHolder extends RecyclerView.ViewHolder {

        public TextView messageBody;

        public View layout;

        public UserMessageViewHolder (View v){
            super(v);
            layout = v;
            messageBody = layout.findViewById(R.id.message_body);
        }
    }

    public class AssistantMessageViewHolder extends RecyclerView.ViewHolder {

        public View avatar;
        public TextView name;
        public TextView messageBody;


        public View layout;

        public AssistantMessageViewHolder (View v){
            super(v);
            layout = v;
            messageBody = layout.findViewById(R.id.assistant_message_body);
            name = layout.findViewById(R.id.name_assistant);

        }
    }


}
