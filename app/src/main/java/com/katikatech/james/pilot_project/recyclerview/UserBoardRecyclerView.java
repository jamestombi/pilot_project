package com.katikatech.james.pilot_project.recyclerview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.katikatech.james.pilot_project.userCardItem.AccountCard;
import com.katikatech.james.pilot_project.userCardItem.BalanceCard;
import com.katikatech.james.pilot_project.userCardItem.CardItem;
import com.katikatech.james.pilot_project.userCardItem.GraphCard;
import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.userCardItem.LastOperationsCard;
import com.katikatech.james.pilot_project.R;
import com.katikatech.james.pilot_project.UI.DetailedOperationActivity;
import com.katikatech.james.pilot_project.UI.detailedTransactions;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class UserBoardRecyclerView extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<CardItem> cardItems;
    private String currentDevise;
    private KatikaSharedPreferences preferences;
    private Context mContext;
    BalanceCard balanceCard;
    private static final String TAG = "recyclerview.USER_BOARD";

    public UserBoardRecyclerView(List<CardItem> cardItemList, Context context) {

        cardItems = cardItemList;
        mContext = context;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        final View view;
        LayoutInflater inflater;
        preferences = new KatikaSharedPreferences(parent.getContext());
        //initialise the current devise
        //currentDevise = parent.getContext().getApplicationContext().getString(R.string.devise);
        currentDevise = new KatikaSharedPreferences(parent.getContext()).getUserCurrencySymbol();

        if (viewType == CardItem.BALANCE_LAYOUT) {
            inflater = LayoutInflater.from(parent.getContext());
            //Change list_layout layout to inflate
            view = inflater.inflate(R.layout.balance_card_layout, parent, false);



            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    preferences.setAccountConnected(true);
                    Intent detailsIntent = new Intent(parent.getContext(), detailedTransactions.class);
                    view.getContext().startActivity(detailsIntent);
                    ((Activity)parent.getContext()).finish();
                }
            });

            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    balanceCard.setMyBalance(getDistBalance(parent.getContext()));
                    //balanceCard.setMyBalance(100);
                    notifyDataSetChanged();
                    return true;
                }
            });
            //we can choose at will here the ViewHolder we want to inflate
            return new UserBalanceViewHolder(view);

        } else if (viewType == CardItem.GRAPH_LAYOUT){
            inflater = LayoutInflater.from(parent.getContext());
            //Change list_layout layout to inflate
            view = inflater.inflate(R.layout.graph_card_layout, parent, false);
            //we can choose at will here the ViewHolder we want to inflate
            return new UserGraphViewHolder(view);

        } else if (viewType == CardItem.ACCOUNT_LAYOUT){
            inflater = LayoutInflater.from(parent.getContext());
            //Change list_layout layout to inflate
            view = inflater.inflate(R.layout.user_info_layout, parent, false);
            //we can choose at will here the ViewHolder we want to inflate
            return new UserAccountViewHolder(view);

        } else if (viewType == CardItem.LAST_OPERATION_LAYOUT){
            inflater = LayoutInflater.from(parent.getContext());
            final LastOperationsViewHolder lastOperationsViewHolder;

            //Change list_layout layout to inflate
            view = inflater.inflate(R.layout.userboard_list_layout, parent, false);

            lastOperationsViewHolder = new LastOperationsViewHolder(view);

            //Set click event to detail operation Activity knowing the data
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = lastOperationsViewHolder.getAdapterPosition();
                    //A toast to show values of item clicked
                    LastOperationsCard insideOperation = (LastOperationsCard) cardItems.get(position);
                    //Toast.makeText(parent.getContext(), "description: " +
                    //        insideOperation.myDescription + " amount : " +
                    //        insideOperation.myAmount, Toast.LENGTH_SHORT).show();

                    preferences.setAccountConnected(true);
                    Intent detailsOperationIntent = new Intent(parent.getContext(), DetailedOperationActivity.class);
                    detailsOperationIntent.putExtra("description", insideOperation.myDescription);
                    detailsOperationIntent.putExtra("amount", insideOperation.myAmount);
                    detailsOperationIntent.putExtra("insertion", insideOperation.myDate);
                    detailsOperationIntent.putExtra("remote_key", insideOperation.remoteKey);
                    detailsOperationIntent.putExtra("categorieTitle", "Catégorie");
                    parent.getContext().startActivity(detailsOperationIntent);
                    ((Activity)parent.getContext()).finish();
                }
            });

            //we can choose at will here the ViewHolder we want to inflate
            return lastOperationsViewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        int type = getItemViewType(position);

        if (type == CardItem.BALANCE_LAYOUT) {
            balanceCard = (BalanceCard) cardItems.get(position);
            DecimalFormat df2 = new DecimalFormat("#.##");
            UserBalanceViewHolder balanceHolder = (UserBalanceViewHolder) holder;

            balanceHolder.balance.setText(""+df2.format(balanceCard.getMyBalance())+" "+currentDevise);

            balanceHolder.diffBalancePercentage.setText("Voir les détails >");


            /*
            balanceCard.setMyLastUpdate("12 septembre 2019");

            if(!balanceCard.getMyLastUpdate().isEmpty()){
                try {
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.FRANCE);
                    DateFormat humanDateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.FRANCE);
                    Date enteredDate = humanDateFormat.parse(balanceCard.getMyLastUpdate());
                    Date today = dateFormat.parse(dateFormat.format(new Date()));
                    long diff = TimeUnit.MILLISECONDS.toDays(today.getTime() - enteredDate.getTime());
                    if (diff > 0) {
                        balanceCard.setMyBalance(getDistBalance(mContext));
                        Log.d(TAG, "UUUUUUPDAAAAATE");
                        //balanceCard.setMyBalance(100);
                        notifyDataSetChanged();
                    }
                } catch (ParseException excp) {
                    Log.e(TAG, excp.getMessage());
                }
            }
            */

            if (!balanceCard.getMyLastUpdate().isEmpty()){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    balanceHolder.innerToolbar.setSubtitle("Mis à jour le "+ balanceCard.getMyLastUpdate());
                }
            }


            // your logic here
        } else if (type == CardItem.GRAPH_LAYOUT){
            GraphCard graphCard = (GraphCard) cardItems.get(position);
            UserGraphViewHolder graphHolder = (UserGraphViewHolder) holder;
            graphHolder.diffComment.setText(graphCard.myDiffComment);

            /**
             * Prepare and load bar chart
             */

            ArrayList<BarEntry> incomesBarEntries = new ArrayList<>();

            incomesBarEntries.add(new BarEntry(1, graphCard.myBalanceGraph.get("Incomes").get(0)));
            incomesBarEntries.add(new BarEntry(2, graphCard.myBalanceGraph.get("Incomes").get(1)));
            incomesBarEntries.add(new BarEntry(3, graphCard.myBalanceGraph.get("Incomes").get(2)));
            incomesBarEntries.add(new BarEntry(4, graphCard.myBalanceGraph.get("Incomes").get(3)));

            BarDataSet incomesDataSet = new BarDataSet(incomesBarEntries, "Revenus");
            incomesDataSet.setColors(ColorTemplate.rgb("33dd11"));

            ArrayList<BarEntry> expensesBarEntries = new ArrayList<>();

            expensesBarEntries.add(new BarEntry(1, graphCard.myBalanceGraph.get("Expenses").get(0)));
            expensesBarEntries.add(new BarEntry(2, graphCard.myBalanceGraph.get("Expenses").get(1)));
            expensesBarEntries.add(new BarEntry(3, graphCard.myBalanceGraph.get("Expenses").get(2)));
            expensesBarEntries.add(new BarEntry(4, graphCard.myBalanceGraph.get("Expenses").get(3)));

            BarDataSet ExpensesDataSet = new BarDataSet(expensesBarEntries, "Dépenses");
            ExpensesDataSet.setColors(ColorTemplate.rgb("dd3311"));

            BarData data = new BarData(incomesDataSet, ExpensesDataSet);

            graphHolder.barChart.setData(data);

            XAxis xAxis = graphHolder.barChart.getXAxis();
            xAxis.setValueFormatter(new IndexAxisValueFormatter(new String[]{"first", "second", "third", "fourth"}));
            xAxis.setCenterAxisLabels(true);
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setGranularity(1);
            xAxis.setGranularityEnabled(true);

            graphHolder.barChart.setDragEnabled(true);
            graphHolder.barChart.setVisibleXRangeMaximum(4);
            Description mDesc = new Description();
            mDesc.setText("");
            graphHolder.barChart.setDescription(mDesc);

            float barSpace = 0.10f;
            float groupSpace = 0.5f;
            data.setBarWidth(0.15f);

            graphHolder.barChart.getXAxis().setAxisMinimum(0);
            graphHolder.barChart.getXAxis().setAxisMaximum(0+graphHolder.barChart.getBarData().getGroupWidth(groupSpace, barSpace)*4);
            graphHolder.barChart.getAxisLeft().setAxisMinimum(0);

            graphHolder.barChart.groupBars(0,groupSpace, barSpace);

            graphHolder.barChart.invalidate();

            // your logic here
        } else if (type == CardItem.ACCOUNT_LAYOUT){

            AccountCard accountCard = (AccountCard) cardItems.get(position);
            UserAccountViewHolder accountHolder = (UserAccountViewHolder) holder;
            accountHolder.userName.setText(accountCard.userName);
            accountHolder.clientPhoneNumber.setText("Tél: "+accountCard.userPhoneNumber);
            accountHolder.clientAccountNumber.setText(""+accountCard.userAccountNumber);
        } else if (type == CardItem.LAST_OPERATION_LAYOUT){

            LastOperationsCard lastOperationsCard = (LastOperationsCard) cardItems.get(position);
            LastOperationsViewHolder lastOperationsViewHolder = (LastOperationsViewHolder) holder;

            /* Set Date in human readable format */
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.FRANCE);
            DateFormat formattedDate =
                    new SimpleDateFormat("dd MMM yyyy", Locale.FRANCE);
            try {
                Date currentDate = dateFormat.parse(lastOperationsCard.myDate);
                lastOperationsViewHolder.date_operation.setText(formattedDate.format(currentDate));
            } catch (ParseException e) {
                Log.d("JTOXXXXX",e.getMessage());
                lastOperationsViewHolder.date_operation.setText(lastOperationsCard.myDate);
            }

            /* Set the operation title */
            String myDescription;
            if (lastOperationsCard.myDescription.length() > 15) {
                myDescription = lastOperationsCard.myDescription.substring(0, 10) + "...";
            } else {
                myDescription = lastOperationsCard.myDescription;
            }
            lastOperationsViewHolder.title_operation.setText(myDescription);

            lastOperationsViewHolder.amount_operation.setText(lastOperationsCard.myAmount +" "+preferences.getUserCurrencySymbol());
        }

    }


    @Override
    public int getItemViewType(int position) {

        return cardItems.get(position).getCardType();
    }

    @Override
    public int getItemCount() {

        return cardItems.size();
    }

    public class UserBalanceViewHolder extends RecyclerView.ViewHolder{
        public View balanceLayout;
        public TextView balance;
        public TextView diffBalancePercentage;
        public Toolbar innerToolbar;

        public UserBalanceViewHolder(View v) {
            super(v);
            balanceLayout = v;
            balance = balanceLayout.findViewById(R.id.balance_amount);
            diffBalancePercentage = balanceLayout.findViewById(R.id.diff_balance_percent);
            innerToolbar =  balanceLayout.findViewById(R.id.toolbar_balance);

        }
    }

    public class UserGraphViewHolder extends RecyclerView.ViewHolder{
        public View graphLayout;
        public TextView diffComment;
        public BarChart barChart;

        public UserGraphViewHolder(View v) {
            super(v);
            graphLayout = v;
            //graphImage = graphLayout.findViewById(R.id.graphAmount);
            diffComment = graphLayout.findViewById(R.id.diff_comment);
            barChart = graphLayout.findViewById(R.id.expenses_barchart);

            barChart.setDrawBarShadow(false);
            barChart.setDrawValueAboveBar(true);
            barChart.setMaxVisibleValueCount(60);
            barChart.setPinchZoom(false);
            barChart.setDrawGridBackground(true);

        }
    }

    public class UserAccountViewHolder extends RecyclerView.ViewHolder{
        public View userPresentationLayout;

        public TextView userName;
        public TextView clientPhoneNumber;
        public TextView clientAccountNumber;

        public UserAccountViewHolder(View v) {
            super(v);
            userPresentationLayout = v;
            userName = userPresentationLayout.findViewById(R.id.User_name);
            clientPhoneNumber = userPresentationLayout.findViewById(R.id.User_phoneNumber);
            clientAccountNumber = userPresentationLayout.findViewById(R.id.User_ClientNumber);

        }
    }

    public class LastOperationsViewHolder extends RecyclerView.ViewHolder{
        public View userPresentationLayout;

        public TextView date_operation;
        public TextView title_operation;
        public TextView amount_operation;

        public LastOperationsViewHolder(View v) {
            super(v);
            userPresentationLayout = v;
            date_operation = userPresentationLayout.findViewById(R.id.date_operation);
            title_operation = userPresentationLayout.findViewById(R.id.title_operation);
            amount_operation = userPresentationLayout.findViewById(R.id.amount_operation);

        }
    }

    /**
     * Get Balance from the SERVER */
    public float getDistBalance(final Context context)
    {
        float myBalance = 0.f;

        //Thread used to call asyncTask asking for the SERVER data
        Thread mainThread = new Thread(){
            @Override
            public void run() {
                AsyncThread asyncThread = new AsyncThread(context);
                JSONObject jsonObject = null;

                try {
                    jsonObject = asyncThread.execute((context.getString(R.string.url_request_balance)),
                            preferences.getUserId())
                            .get(10, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    Log.e(TAG, e.getMessage());
                } catch (ExecutionException e) {
                    Log.e(TAG, e.getMessage());
                } catch (TimeoutException e) {
                    Log.e(TAG, e.getMessage());
                }

                final JSONObject receivedJSONObject = jsonObject;
                try {
                    Log.d(TAG, ""+receivedJSONObject.get("balance"));
                    preferences.setAccountBalance(BigDecimal.valueOf(receivedJSONObject.getDouble("balance")).floatValue());
                } catch (JSONException e) {
                    Log.d(TAG, e.getMessage());
                }

            }
        };
        mainThread.start();

        return preferences.getAccountBalance();
    }


    private static class AsyncThread extends AsyncTask<String, Void, JSONObject>{

        private Context mContext;

        public AsyncThread(Context context){
            mContext = context;
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            String url = strings[0];
            String userId = strings[1];
            JSONObject args = new JSONObject();
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            JSONObject response = new JSONObject();

            try {
                //Put Arguments
                args.put("userid", userId);
                final RequestFuture<JSONObject> future = RequestFuture.newFuture();
                //Create request
                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                        url, args, future, future);
                //Add request to request Queue
                requestQueue.add(request);
                Log.d("TAG OUT --> ", response.toString());
                try {
                    //Retrieve response
                    Log.d("TAG ON --> ", response.toString());
                    response = future.get(10, TimeUnit.SECONDS);
                    Log.d("TAG --> ", response.toString());

                    return response;
                    //return BigDecimal.valueOf(response.getDouble("balance")).floatValue();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    Log.e("toto", "TIME OUT: "+e.getMessage());
                }
            } catch (JSONException excp){
                Log.e("toto --> ", excp.getMessage());
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //ConstraintLayout mConstraintLayout = findViewById(R.id.loading_layout_user_board);
            //mConstraintLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            //viewBadPassword.setVisibility(View.VISIBLE);
            //ConstraintLayout mConstraintLayout = findViewById(R.id.loading_layout_user_board);
            //mConstraintLayout.setVisibility(View.INVISIBLE);
            /* clear the text zone */
            //editPassword.getText().clear()
        }
    }


}
