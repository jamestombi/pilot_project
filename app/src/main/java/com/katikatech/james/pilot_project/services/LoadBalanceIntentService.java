package com.katikatech.james.pilot_project.services;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.katikatech.james.pilot_project.Operation;
import com.katikatech.james.pilot_project.OperationRepository;
import com.katikatech.james.pilot_project.R;
import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.UI.MainActivity;
import com.katikatech.james.pilot_project.UI.UserBoard;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class LoadBalanceIntentService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_FOO = "com.example.james.pilot_project.services.action.FOO";
    private static final String ACTION_BAZ = "com.example.james.pilot_project.services.action.BAZ";
    private static final String ACTION_NOTIFY = "com.example.james.pilot_project.services.action.NOTIFY";
    private final int NOTIFICATION_ID = 101;
    private final String CHANNEL_ID = "less_used";
    private final String TAG = "less_used";

    // TODO: Rename parameters
    private static final String EXTRA_PARAM1 = "com.example.james.pilot_project.services.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "com.example.james.pilot_project.services.extra.PARAM2";
    public static final String SOURCE_URL = "balance";
    Context mContext;
    KatikaSharedPreferences preferences;

    public LoadBalanceIntentService() {
        super("LoadBalanceIntentService");

    }

    /*Create a channel for notifications in ANDROID version above 8.0.0 */
    private void createNotificationChannel() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Project Base";
            String description = "A test of notifications";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationChannel.setDescription(description);
            notificationChannel.enableLights(true);
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            notificationManager.createNotificationChannel(notificationChannel);
        }

    }

    private void sendReconnectNotification(){

        Intent boardIntent = new Intent(LoadBalanceIntentService.this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(LoadBalanceIntentService.this, 0, boardIntent, 0);
        /*Create a notification channel for Android 8.0 and above*/
        createNotificationChannel();

        /*Notifications built*/
        NotificationCompat.Builder builder = new NotificationCompat.Builder(LoadBalanceIntentService.this, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_logo_notification)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(preferences.getUserFirstName() + ",15 minutes sans mettre à jour votre profil, Vous nous manquez!!")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setStyle(new NotificationCompat.BigTextStyle())
                .setColor(getResources().getColor(R.color.colorGold))
                .setColorized(true)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        /*Notifications sent*/
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(LoadBalanceIntentService.this);
        notificationManagerCompat.notify(NOTIFICATION_ID, builder.build());
    }


    private class NewTimeThread extends Thread {
        int test;

        public NewTimeThread(int mTest) {
            super();
            test = mTest;
        }

        @Override
        public void run() {
            super.run();
            Log.d(TAG, "intent start notification process");
            AtomicBoolean OK = new AtomicBoolean(preferences.getOnlineMode());
            //preferences.setTimeoutLogoff(10);
            try {
                while (OK.get()) {
                    Thread.sleep(test);
                    sendReconnectNotification();

                    OK = new AtomicBoolean(preferences.getOnlineMode());
                    Log.d(TAG, "intent service SEND Notification OK is  " + OK);
                }
            } catch (InterruptedException excp){
                Log.d(TAG, "new time thread terminated");//OK = false;
            }
        }
    }

    private Thread NotifThread;


    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
        public static void startActionFoo(Context context, String param1, String param2) {
        Intent intent = new Intent(context, LoadBalanceIntentService.class);
        intent.setAction(ACTION_FOO);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    public static void startActionNotify(Context context) {
        Intent intent = new Intent(context, LoadBalanceIntentService.class);
        intent.setAction(ACTION_NOTIFY);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionBaz(Context context, String param1, String param2) {
        Intent intent = new Intent(context, LoadBalanceIntentService.class);
        intent.setAction(ACTION_BAZ);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            preferences = new KatikaSharedPreferences(this);
            if (ACTION_FOO.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                handleActionFoo(param1, param2);
            } else if (ACTION_BAZ.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                handleActionBaz(param1, param2);
            } else if (ACTION_NOTIFY.equals(action)) {
                handleActionNotify();
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFoo(String param1, String param2) {
        // TODO: Handle action Foo
        //throw new UnsupportedOperationException("Not yet implemented");

        String url = param1;
        Float balance = 0.f;
        JSONObject args = new JSONObject();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JSONObject response = new JSONObject();
        preferences = new KatikaSharedPreferences(this);
        try {
            args.put("userid", preferences.getUserId());
            final RequestFuture<JSONObject> future = RequestFuture.newFuture();
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                    url, args, future, future);
            requestQueue.add(request);
            Log.d("TAG OUT --> ", response.toString());
            try {
                Log.d("TAG ON --> ", response.toString());
                response = future.get(10, TimeUnit.SECONDS);
                Log.d("TAG OFFICIAL --> ", response.toString());
                balance = BigDecimal.valueOf(response.getDouble("balance")).floatValue();
                //balance = 10.f;

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                Log.e("toto", "TIME OUT: "+e.getMessage());
            }
        } catch (JSONException excp){
            Log.d("toto --> ", excp.getMessage());
        }

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(UserBoard.MyReceiver.ACTION_RESP);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        //broadcastIntent.putExtra(SOURCE_URL, balance);
        broadcastIntent.putExtra(SOURCE_URL, balance);
        sendBroadcast(broadcastIntent);
    }

    private void handleActionNotify() {
        // TODO: Handle action Foo
        //throw new UnsupportedOperationException("Not yet implemented");

        NotifThread = new NewTimeThread(3600000);
        //NotifThread.setPriority(Thread.MAX_PRIORITY);
        NotifThread.start();

        /*Handler h = new Handler();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                //preferences.setTimeoutLogoff(10);
                sendReconnectNotification();
            }
        };
        Log.d(TAG, "intent start notification process");
        AtomicBoolean OK = new AtomicBoolean(preferences.getOnlineMode());
        while (OK.get()) {
            h.postDelayed(r, 150000);
            OK = new AtomicBoolean(preferences.getOnlineMode());
            Log.d(TAG, "intent service SEND Notification OK is  " + OK);
        }*/



    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionBaz(String param1, String param2) {
        // TODO: Handle action Baz
        //throw new UnsupportedOperationException("Not yet implemented");
        String url = param1;
        Float balance = 0.f;
        JSONObject args = new JSONObject();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JSONObject response = new JSONObject();
        preferences = new KatikaSharedPreferences(this);
        try {
            args.put("userid", preferences.getUserId());
            final RequestFuture<JSONObject> future = RequestFuture.newFuture();
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                    url, args, future, future);
            requestQueue.add(request);
            Log.d("SEND OUT --> ", response.toString());
            try {
                Log.d("RECV ON --> ", response.toString());
                response = future.get(10, TimeUnit.SECONDS);
                Log.d("RESP OFFICIAL --> ", response.toString());
                JSONArray syncOperations = response.getJSONArray("syncOperation");
                for (int i = 0; i < syncOperations.length(); i ++){
                    Log.d("value "+i, syncOperations.getJSONObject(i).toString());
                    Operation currentOperation = getOperation(syncOperations.getJSONObject(i).toString());
                    Log.d("MAG  ---> ", " amount : "+currentOperation.getAmount() +  " description: " + currentOperation.getDescription());
                    OperationRepository operationRepository = new OperationRepository(getApplication());

                                    operationRepository.insertOperation(getApplication().
                                            getApplicationContext(),
                                            currentOperation.getDescription(),
                                            currentOperation.getAmount(),
                                            currentOperation.getInsertedAt(),
                                            currentOperation.getRemoteKey());
                }
                if(syncOperations.length()>= 1) {
                    balance = 10.f;
                } else {
                    balance = 0.f;
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                Log.e("toto", "TIME OUT: "+e.getMessage());
            }
        } catch (JSONException excp){
            Log.d("toto --> ", excp.getMessage());
        }

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(UserBoard.MyReceiver.ACTION_RESP);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        //broadcastIntent.putExtra(SOURCE_URL, balance);
        broadcastIntent.putExtra(SOURCE_URL, balance);
        sendBroadcast(broadcastIntent);
    }

    public Operation getOperation(String operationJson){
        Gson myGson = new Gson();
        return myGson.fromJson(operationJson, Operation.class);
    }
}
