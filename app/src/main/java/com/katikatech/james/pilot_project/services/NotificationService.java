package com.katikatech.james.pilot_project.services;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.katikatech.james.pilot_project.R;
import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.broadcastReceivers.NotificationPublisher;

import java.util.concurrent.atomic.AtomicBoolean;

public class NotificationService extends Service {

    private static final String TAG = "NotificationService";
    private final String CHANNEL_ID = "less_used";
    public final int DELAY = 30000;
    public final int MAX_DELAY = 60000;
    public final int INTERMEDIATE_DELAY = 24000000;
    public final int SUPER_MAX_DELAY = 86400000; /* 24 hours */
    //public final int SUPER_MAX_DELAY = 60000; /* 1 minutes */
    private static KatikaSharedPreferences preferences;
    AlarmManager alarmManager;
    PendingIntent pendingIntent;

    private static final String ACTION_SET_TIMEOUT = "com.example.james.pilot_project.action.SET_TIMEOUT";
    private static final String ACTION_CANCEL_TIMEOUT = "com.example.james.pilot_project.action.CANCEL_TIMEOUT";

    private class TimeThread extends Thread {
        int test;

        public TimeThread(int mTest) {
            super();
            test = mTest;
        }

        @Override
        public void run() {
            super.run();
            AtomicBoolean OK = new AtomicBoolean(preferences.getOnlineMode());
            int value;
            int lastDelay;
            preferences.setTimeoutLogoff(10);
            try {
                while (OK.get() && preferences.getTimeOutLogoff() != 0) {
                    value = test;
                    Thread.sleep(DELAY);
                    if(preferences.getTimeOutLogoff() == 0){
                        break;
                    }
                    //OK = preferences.getOnlineMode();
                    Log.d(TAG, "BEFORE SEND Notification OK is  " + OK);
                    if(OK.get() && !preferences.getAccountConnected()) {
                        synchronized (this) {
                            lastDelay = preferences.getTimeOutLogoff() + DELAY;
                            Log.d(TAG, "DELAY Notification OK is  " + lastDelay);
                            preferences.setTimeoutLogoff(lastDelay);
                            if(lastDelay > MAX_DELAY) {
                                preferences.setTimeoutLogoff(0);
                                Log.d(TAG, "SEND Notification " + value);
                                handleActionNotify();
                            }
                        }
                    }

                    OK = new AtomicBoolean(preferences.getOnlineMode());
                    Log.d(TAG, "AFTER SEND Notification OK is  " + OK);
                }
            } catch (InterruptedException excp){
                //OK = false;
            }
        }
    }

    //private Thread timeOutThread;


    private void handleActionNotify() {
        scheduleNotification(/*getNotification(),*/ SUPER_MAX_DELAY);
    }



    public void setTimeout(final int test) {
        preferences = new KatikaSharedPreferences(getApplicationContext());
        Log.d(TAG, "START ACTION MODE " + preferences.getOnlineMode());
        //timeOutThread.start();
    }

    public void cancelTimeout() {
        preferences = new KatikaSharedPreferences(getApplicationContext());
        preferences.setOnlineMode(false);

        //timeOutThread.interrupt();


        alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent boardIntent = new Intent(NotificationService.this, NotificationPublisher.class);
        pendingIntent = PendingIntent.getBroadcast(NotificationService.this, 101, boardIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.cancel(pendingIntent);

        Log.d(TAG, "STOP ACTION MODE " + preferences.getOnlineMode());
    }

    public static void startActionTimeOut(Context context) {
        preferences = new KatikaSharedPreferences(context);
        Intent intent = new Intent(context, NotificationService.class);
        //intent.setAction(ACTION_SET_TIMEOUT);
        intent.setAction(preferences.getActionStartTimeout());
        context.startService(intent);
    }

    public static void stopActionTimeOut(Context context) {
        preferences = new KatikaSharedPreferences(context);
        Intent intent = new Intent(context, NotificationService.class);
        //intent.setAction(ACTION_CANCEL_TIMEOUT);
        intent.setAction(preferences.getActionStopTimeout());
        context.startService(intent);
    }


    public NotificationService() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        String action;
        try {
         action = intent.getAction();
        } catch (NullPointerException excp ){
            action = ACTION_SET_TIMEOUT;
        }
        //timeOutThread = new TimeThread(DELAY);

        if (ACTION_SET_TIMEOUT.equals(action)) {
            preferences = new KatikaSharedPreferences(getApplicationContext());
            preferences.setOnlineMode(true);
            //setTimeout(DELAY);
            handleActionNotify();

        } else if (ACTION_CANCEL_TIMEOUT.equals(action)){
            cancelTimeout();

        } else {
            stopSelf();
        }

        return START_STICKY;

    }



    public void scheduleNotification(/*Notification notification,*/ int delay) {

        Intent boardIntent = new Intent(NotificationService.this, NotificationPublisher.class);
        boardIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, 101);
        //boardIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        pendingIntent = PendingIntent.getBroadcast(NotificationService.this, 101, boardIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        long futureInMillis = SystemClock.elapsedRealtime() + delay;
        alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, INTERMEDIATE_DELAY, pendingIntent);
        Log.d(TAG, "scheduleNotification: alarm set ok for "+ futureInMillis + " milliseconds!!!");
    }

    private Notification getNotification() {

        /*Notifications built*/
        NotificationCompat.Builder builder = new NotificationCompat.Builder(NotificationService.this, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_logo_notification)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(preferences.getUserFirstName() + ", Beaucoup de temps sans mettre à jour votre profil, Vous nous manquez!!")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setStyle(new NotificationCompat.BigTextStyle())
                .setColor(getResources().getColor(R.color.colorGold))
                .setColorized(true)
                .setAutoCancel(true);

        Log.d(TAG, "getNotification: built notification");

        return builder.build();
    }


}
