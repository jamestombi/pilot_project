package com.katikatech.james.pilot_project.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.katikatech.james.pilot_project.ApiConnectionHelper;
import com.katikatech.james.pilot_project.KatikaSMSReceiver;
import com.katikatech.james.pilot_project.SharedPreferences.KatikaSharedPreferences;
import com.katikatech.james.pilot_project.R;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;


public class PhoneNumberVerifierService extends Service {

    public static final int STATUS_STARTED = 10;
    public static final int STATUS_REQUESTING = 20;
    public static final int STATUS_REQUEST_SENT = 30;
    public static final int STATUS_RESPONSE_RECEIVED = 50;
    public static final int STATUS_RESPONSE_VERIFYING = 70;
    public static final int STATUS_RESPONSE_VERIFIED = 90;
    public static final int STATUS_RESPONSE_FAILED = 95;
    public static final int STATUS_MAX = 100;

    public static final int MAX_TIMEOUT = 1800000; // 30 mins in millis

    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS

    private static final String ACTION_VERIFY_MESSAGE = "com.example.james.pilot_project.action.message";
    public static final String ACTION_VERIFICATION_STATUS_CHANGE = "com.example.james.pilot.action.verification_status_change";
    private static final String EXTRA_PHONE_NUMBER = "com.example.james.pilot_project.extra.PHONE_NUMBER";
    private static final String EXTRA_USER_ID = "com.example.james.pilot_project.extra.USER_ID";
    private static final String EXTRA_USER_FIRSTNAME = "com.example.james.pilot_project.extra.USER_FIRSTNAME";
    private static final String EXTRA_USER_NAME = "com.example.james.pilot_project.extra.USER_NAME";
    private static final String EXTRA_USER_PHONE = "com.example.james.pilot_project.extra.USER_PHONE";
    private static final String EXTRA_MESSAGE = "com.example.james.pilot_project.extra.MESSAGE";
    private static final String TAG = "phonenumber_service";

    private static final String ACTION_VERIFY = "com.example.james.pilot_project.action.VERIFY";
    private static final String ACTION_SMS_VERIFY = "com.example.james.pilot_project.action.SMS_VERIFY";
    private static final String ACTION_USERID = "com.example.james.pilot_project.action.USER_ID";
    private static final String ACTION_USER_UPDATE = "com.example.james.pilot_project.action.USER_UPDATE";
    private static final String ACTION_CANCEL = "com.example.james.pilot_project.action.CANCEL";
    private final String CHANNEL_ID = "number_verifier";
    private static final int NOTIFICATION_ID = 1001;

    boolean isVerifying;
    boolean isRunning;

    private NotificationCompat.Builder notification;
    private ApiConnectionHelper api;
    private KatikaSMSReceiver smsReceiver;
    private SmsRetrieverClient smsRetrieverClient;


    public PhoneNumberVerifierService() {
    }

    /*Create a channel for notifications in ANDROID version above 8.0.0 */
    private void createServerNotificationChannel() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "serverResponse";
            String description = "A notification sent after server response";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationChannel.setDescription(description);
            notificationChannel.enableLights(true);
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            notificationManager.createNotificationChannel(notificationChannel);
        }

    }

    public static void startActionVerify(Context context, String phoneNo) {
        Intent intent = new Intent(context, PhoneNumberVerifierService.class);
        intent.setAction(ACTION_VERIFY);
        intent.putExtra(EXTRA_PHONE_NUMBER, phoneNo);
        context.startService(intent);
    }

    public static void startActionSMSVerification(Context context, String SMSMessage) {
        Intent intent = new Intent(context, PhoneNumberVerifierService.class);
        intent.setAction(ACTION_SMS_VERIFY);
        intent.putExtra(EXTRA_MESSAGE, SMSMessage);
        context.startService(intent);
    }

    public static void startActionRequestId(Context context, String userIdInit) {
        Intent intent = new Intent(context, PhoneNumberVerifierService.class);
        intent.setAction(ACTION_USERID);
        intent.putExtra(EXTRA_USER_ID, userIdInit);
        context.startService(intent);
    }

    public static void startActionUpdateUser(Context context, String userId, String userFirstName, String userName, String userPhone) {
        Intent intent = new Intent(context, PhoneNumberVerifierService.class);
        intent.setAction(ACTION_USER_UPDATE);
        intent.putExtra(EXTRA_USER_ID, userId);
        intent.putExtra(EXTRA_USER_FIRSTNAME, userFirstName);
        intent.putExtra(EXTRA_USER_NAME, userName);
        intent.putExtra(EXTRA_USER_PHONE, userPhone);
        context.startService(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        api = new ApiConnectionHelper(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        final String action = intent.getAction();

        if (ACTION_VERIFY.equals(action)) {
            final String phoneNo = intent.getStringExtra(EXTRA_PHONE_NUMBER);
            startVerify(phoneNo);
        } else if (ACTION_SMS_VERIFY.equals(action)) {
            final String SMSMessage = intent.getStringExtra(EXTRA_MESSAGE);
            verifyMessage(SMSMessage);
        }  else if (ACTION_USERID.equals(action)) {
            final String UserIdInit = intent.getStringExtra(EXTRA_USER_ID);
            requestUserId(UserIdInit);
        } else if(ACTION_USER_UPDATE.equals(action)) {
            final String UserId = intent.getStringExtra(EXTRA_USER_ID);
            final String UserFirstName = intent.getStringExtra(EXTRA_USER_FIRSTNAME);
            final String UserName = intent.getStringExtra(EXTRA_USER_NAME);
            final String UserPhone = intent.getStringExtra(EXTRA_USER_PHONE);
            updateUser(UserId, UserFirstName, UserName, UserPhone);
        } else if (ACTION_CANCEL.equals(action)) {
            stopSelf();
        }

        // Redelivering intent because the server APIs are designed to be called multiple times
        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
        isVerifying = false;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void notifyStatus(int status, @Nullable String phoneNo) {
        if (status < STATUS_STARTED || status >= STATUS_RESPONSE_VERIFIED) {
            isVerifying = false;
        } else {
            isVerifying = true;
        }

        try {
            //Notification n = getNotification(this, status, phoneNo);
            //startForeground(NOTIFICATION_ID, n);
            Log.d(TAG, "Status: "+ status + " phone: "+phoneNo);
            isRunning = true;
        } catch (IllegalStateException e) {
            Log.d(TAG, "Notification progress unable to be configured.");
        }
    }

    private void setVerificationState(@Nullable String phoneNumber) {
        KatikaSharedPreferences prefs = new KatikaSharedPreferences(this);
        prefs.setUserPhone(phoneNumber);
        Intent i = new Intent(ACTION_VERIFICATION_STATUS_CHANGE);
        LocalBroadcastManager.getInstance(PhoneNumberVerifierService.this)
                .sendBroadcast(i);
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void startVerify(String phoneNo) {
        // Make this a foreground service
        notifyStatus(STATUS_STARTED, phoneNo);


       // Start SMS receiver code
        smsRetrieverClient = SmsRetriever.getClient(this);
        Task<Void> task = smsRetrieverClient.startSmsRetriever();
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //smsReceiver.setTimeout();
                notifyStatus(STATUS_REQUEST_SENT, null);
                Log.d(TAG, "SmsRetrievalResult status: Success");
                //Toast.makeText(PhoneNumberVerifierService.this, getString(R.string.verifier_registered),
                //        Toast.LENGTH_SHORT).show();
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "SmsRetrievalResult start failed.", e);
                stopSelf();
            }
        });


        // Communicate to background servers to send SMS and get the expect OTP
        notifyStatus(STATUS_REQUESTING, phoneNo);
        api.request(phoneNo,
                new ApiConnectionHelper.RequestResponse() {
                    @Override
                    public void onResponse(int success) {
                        if (success != 0) {
                            Toast succesToast = Toast.makeText(PhoneNumberVerifierService.this,
                                    getString(R.string.verifier_sms_server_ok),
                                    Toast.LENGTH_LONG);
                            succesToast.setGravity(Gravity.CENTER, 0, 0);
                            succesToast.show();
                        } else {
                            Log.e(TAG, "Unsuccessful request call.");
                            Toast.makeText(PhoneNumberVerifierService.this,
                                    getString(R.string.toast_unverified), Toast.LENGTH_LONG).show();
                            stopSelf();
                        }
                    }
                }, new ApiConnectionHelper.ApiError() {
                    @Override
                    public void onError(VolleyError error) {
                        // Do something else.
                        Log.d(TAG, "Error getting response");
                        Toast.makeText(PhoneNumberVerifierService.this,
                                getString(R.string.toast_request_error), Toast.LENGTH_LONG).show();
                        stopSelf();
                    }
                });
    }

    void requestUserId(String userIdInit){
        api.request_userid(userIdInit,
                new ApiConnectionHelper.RequestUserIdResponse() {
                    @Override
                    public void onResponse(String userId, boolean success) {
                        if (!userId.isEmpty()) {
                            Toast.makeText(PhoneNumberVerifierService.this,
                                    getString(R.string.verifier_server_response) + userId,
                                    Toast.LENGTH_SHORT).show();
                            KatikaSharedPreferences prefs = new KatikaSharedPreferences(PhoneNumberVerifierService.this);
                            prefs.setUserId(userId);
                        } else {
                            Log.e(TAG, "Unsuccessful request call.");
                            Toast.makeText(PhoneNumberVerifierService.this,
                                    getString(R.string.toast_unverified), Toast.LENGTH_LONG).show();
                            stopSelf();
                        }
                    }
                }, new ApiConnectionHelper.ApiError() {
                    @Override
                    public void onError(VolleyError error) {
                        // Do something else.
                        Log.d(TAG, "Error getting response");
                        Toast.makeText(PhoneNumberVerifierService.this,
                                getString(R.string.toast_request_error), Toast.LENGTH_LONG).show();
                        stopSelf();
                    }
                });

    }

    void updateUser(String userId, String userFirstName, String userName, String userPhone){
        api.request_update_user(userId, userFirstName, userName, userPhone,
                new ApiConnectionHelper.UpdateResponse() {
                    @Override
                    public void onResponse(boolean success) {
                        if (success) {
                            Toast.makeText(PhoneNumberVerifierService.this,
                                    getString(R.string.verifier_server_response),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e(TAG, "Unsuccessful request call.");
                            Toast.makeText(PhoneNumberVerifierService.this,
                                    getString(R.string.toast_unverified), Toast.LENGTH_LONG).show();
                            stopSelf();
                        }
                    }
                }, new ApiConnectionHelper.ApiError() {
                    @Override
                    public void onError(VolleyError error) {
                        // Do something else.
                        Log.d(TAG, "Error getting response");
                        Toast.makeText(PhoneNumberVerifierService.this,
                                getString(R.string.toast_request_error), Toast.LENGTH_LONG).show();
                        stopSelf();
                    }
                });

    }

    void verifyMessage(String smsMessage) {
        KatikaSharedPreferences prefs = new KatikaSharedPreferences(this);
        String requestPhone = prefs.getTempPhone();
        notifyStatus(STATUS_RESPONSE_VERIFYING, null);
        api.verify(requestPhone, smsMessage,
                new ApiConnectionHelper.VerifyResponse() {
                    @Override
                    public void onResponse(boolean success, String phoneNumber) {
                        KatikaSharedPreferences preferences = new KatikaSharedPreferences(getApplicationContext());
                        String requestPhone = preferences.getTempPhone();
                        Log.d(TAG, "inside: " + phoneNumber + " request: " + requestPhone + " state: " + success);
                        if (success && phoneNumber != null && phoneNumber.equals(requestPhone)) {
                            Log.d(TAG, "Successfully verified phone number: " + phoneNumber);
                            notifyStatus(STATUS_RESPONSE_VERIFIED, null);
                            Toast.makeText(PhoneNumberVerifierService.this,
                                    getString(R.string.toast_verified), Toast.LENGTH_LONG).show();
                            setVerificationState(phoneNumber);
                        } else {
                            Log.d(TAG, "Unable to verify response.");
                            notifyStatus(STATUS_RESPONSE_FAILED, null);
                            Toast.makeText(PhoneNumberVerifierService.this,
                                    getString(R.string.toast_unverified), Toast.LENGTH_LONG
                            ).show();
                        }
                        stopSelf();
                    }
                }, new ApiConnectionHelper.ApiError() {
                    @Override
                    public void onError(VolleyError error) {
                        Log.d(TAG, "Communication error with server.");
                        Toast.makeText(PhoneNumberVerifierService.this,
                                getString(R.string.toast_unverified), Toast.LENGTH_LONG
                        ).show();
                        stopSelf();
                    }
                });
    }


    private Notification getNotification(Context context, int status, @Nullable String phoneNo) {
        if (phoneNo != null) {
            Intent cancelIntent = new Intent(this, PhoneNumberVerifierService.class);
            cancelIntent.setAction(ACTION_CANCEL);

            PendingIntent cancelPendingIntent = PendingIntent.getService(this, 0, cancelIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Action cancelAction = new NotificationCompat.Action.Builder(
                    R.drawable.baseline_perm_identity_black_18dp,
                    getString(R.string.cancel_verification), cancelPendingIntent
            ).build();

            createServerNotificationChannel();

            notification = new NotificationCompat.Builder(context, CHANNEL_ID)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(context.getString(R.string.phone_verify_notify_title))
                    .setContentText(context.getString(R.string.phone_verify_notify_message, phoneNo))
                    .addAction(cancelAction);

        } else if (notification == null) {
            throw new IllegalStateException("A Phone number needs to be provided initially");
        }

        notification.setProgress(STATUS_MAX, status, false);
        return notification.build();
    }

}
