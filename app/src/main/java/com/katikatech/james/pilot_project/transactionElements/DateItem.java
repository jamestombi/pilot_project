package com.katikatech.james.pilot_project.transactionElements;

public class DateItem extends TransactionItem {
    public String date;

    public DateItem(String mDate){
        date = mDate;
    }

    @Override
    public int getItemType() {
        return DATE_ITEM;
    }
}
