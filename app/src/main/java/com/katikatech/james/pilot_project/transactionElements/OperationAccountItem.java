package com.katikatech.james.pilot_project.transactionElements;

import com.katikatech.james.pilot_project.Operation;

public class OperationAccountItem extends TransactionItem {
    public Operation operation;

    public OperationAccountItem(Operation mOperation){
        operation = mOperation;
    }

    @Override
    public int getItemType() {
        return ACCOUNT_ITEM;
    }
}
