package com.katikatech.james.pilot_project.transactionElements;

import com.katikatech.james.pilot_project.Operation;

public class OperationItem extends TransactionItem {
    public Operation operation;

    public OperationItem(Operation mOperation){
        operation = mOperation;
    }

    @Override
    public int getItemType() {
        return OPERATION_ITEM;
    }
}
