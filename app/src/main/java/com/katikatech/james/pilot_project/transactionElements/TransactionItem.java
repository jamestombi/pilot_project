package com.katikatech.james.pilot_project.transactionElements;

public abstract class TransactionItem {
    public static final int DATE_ITEM = 0;
    public static final int OPERATION_ITEM = 1;
    public static final int ACCOUNT_ITEM = 2;
    public static final int REFERENCE_ITEM = 3;

    public abstract int getItemType();
}
