package com.katikatech.james.pilot_project.userCardItem;

/**
 * BalanceCard to post the balance of all accounts of the user
 */
public class AccountCard extends CardItem {

    public String userName;
    public String userPhoneNumber;
    public String userAccountNumber;

    public AccountCard(){

    }

    public AccountCard(String aUserName, String aPhoneNumber, String aUserAccount){
        userName = aUserName;
        userPhoneNumber = aPhoneNumber;
        userAccountNumber = aUserAccount;
    }

    @Override
    public int getCardType() {
            return ACCOUNT_LAYOUT;
        }

}
