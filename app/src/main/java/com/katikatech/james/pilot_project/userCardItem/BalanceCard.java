package com.katikatech.james.pilot_project.userCardItem;

/**
 * BalanceCard to post the balance of all accounts of the user
 */
public class BalanceCard extends CardItem {
    private float myBalance;
    public int myDiffBalance;
    private String myLastUpdate;

    public BalanceCard(){

    }

    public BalanceCard(float balanceAmount, int currentDiffBalance, String lastUpdate){
        myBalance = balanceAmount;
        myDiffBalance = currentDiffBalance;
        myLastUpdate = lastUpdate;
    }

    public float getMyBalance() {
        return myBalance;
    }

    public void setMyBalance(float myBalance) {
        this.myBalance = myBalance;
    }

    public String getMyLastUpdate() {
        return myLastUpdate;
    }

    public void setMyLastUpdate(String myLastUpdate) {
        this.myLastUpdate = myLastUpdate;
    }

    @Override
    public int getCardType() {
        return BALANCE_LAYOUT;
    }
}
