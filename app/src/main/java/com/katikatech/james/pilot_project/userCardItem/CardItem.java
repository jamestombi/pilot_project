package com.katikatech.james.pilot_project.userCardItem;


/******
 * The class CardItem will be an abstract class from where all cardViews of the user board will
 * inherit
 */
public abstract class CardItem {
    public static final int BALANCE_LAYOUT = 0;
    public static final int ACCOUNT_LAYOUT = 1;
    public static final int GRAPH_LAYOUT = 2;
    public static final int LAST_OPERATION_LAYOUT = 3;

    abstract public int getCardType();

}
