package com.katikatech.james.pilot_project.userCardItem;

import java.util.List;
import java.util.TreeMap;

public class GraphCard extends CardItem {
    public TreeMap<String, List<Float>> myBalanceGraph;
    public String myDiffComment;

    public GraphCard(){

    }

    public GraphCard(TreeMap<String, List<Float>> myBalance, String currentDiffComment){
        myBalanceGraph = myBalance ;
        myDiffComment = currentDiffComment;
    }

    @Override
    public int getCardType() {

        return GRAPH_LAYOUT;
    }

}
