package com.katikatech.james.pilot_project.userCardItem;

public class LastOperationsCard extends CardItem {
    public String myDate;
    public String myDescription;
    public String myAmount;
    public String remoteKey;

    public LastOperationsCard(String myDate, String myDescription, String myAmount, String myRemoteKey) {
        this.myDate = myDate;
        this.myDescription = myDescription;
        this.myAmount = myAmount;
        this.remoteKey = myRemoteKey;
    }

    @Override
    public int getCardType() {

        return LAST_OPERATION_LAYOUT;
    }
}
